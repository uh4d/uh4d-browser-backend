Documentation of graph database schema using CIDOC CRM as base ontology.

`.graphml` files can be viewed and edited with, for example, [yEd Graph Editor](https://www.yworks.com/products/yed).
