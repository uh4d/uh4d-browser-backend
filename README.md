# UH4D Browser Backend

API Endpoint Documentation: https://4dbrowser.urbanhistory4d.org/api

## Getting started

Follow [detailed instructions](https://gitlab.com/uh4d/uh4d-browser-app/-/blob/dev/server-setup.md) to set up backend application for production scenario.

For development purposes, you may omit web server configuration and use [Neo4j Desktop](https://neo4j.com/developer/neo4j-desktop/) for easier handling of Neo4j database.

Copy `config-template.js` to `config.js` and set properties accordingly.

Install packages:

    npm install

Start server:

    node server.js

If using process manager:

    pm2 start server.js --name uh4d


## Refactor

Executing a script:

    node refactor/<script> [arguments]

Executing a cypher file:

    cypher-shell -u neo4j -p <password> -d uh4d -f <cypher file>

Consider performing a backup beforehand.

## Database Backup

Before refactoring any database entries, make a backup of Neo4j database and application data.

### Backup Neo4j

    neo4j-admin backup --backup-dir=<path> --database=uh4d

You can restore this backup using following commands (Neo4j has to be stopped):

    systemctl stop neo4j
    neo4j-admin restore --from=<path> --database=uh4d --force
    systemctl start neo4j

See [official docs](https://neo4j.com/docs/operations-manual/current/backup/) for more information.

#### Cypher file

Alternatively, database entries can be stored in a .cypher file:

    cypher-shell -u neo4j -p <password> -d uh4d

    CALL apoc.export.cypher.all('uh4d-all.cypher', { cypherFormat: 'updateAll' });

    :exit

The generated .cypher file is located at `/var/lib/neo4j/import/uh4d-all.cypher` (`%NEO4J_HOME%/import` on Windows).

Ensure that Neo4j instance is configured with

    apoc.export.file.enabled=true

### Backup data

    cd uh4d-data
    7z a uh4d-data.zip images objects terrain
