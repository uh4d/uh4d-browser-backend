import * as neo4j from 'neo4j-request';
import errPkg from 'http-errors';
import * as shortId from 'shortid';
import { checkRequiredProperties, replace } from '../../modules/utils.js';
import { updateOSMIntersections } from '../../modules/osm-intersection.js';

const { BadRequest, InternalServerError, NotFound } = errPkg;

/**
 * @param req {e.Request<{id: string}, any, {name: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export default async function duplicate(req, res, next) {

  if (!checkRequiredProperties(req.body, ['name'])) {
    return next(new BadRequest('Missing values: name'));
  }

  // language=Cypher
  const q = `
    MATCH (original:E22:UH4D {id: $id})<-[:P67]-(object:D1)-[:P106]->(file:D9),
          (original)<-[:P157]-(place:E53)-[:P168]->(geo:E94)
    OPTIONAL MATCH (place)-[:P89]->(scene:E53)
    CREATE (e22:E22:UH4D {id: $e22id})-[:P1]->(name:E41:UH4D $e41),
           (e22)<-[:P157]-(placeNew:E53:UH4D {id: $e53id})-[:P89]->(scene),
           (placeNew)-[:P168]->(geoNew:E94 {id: $e94id}),
           (e22)<-[:P67]-(object)
    SET geoNew += apoc.map.removeKey(geo, 'id')
    CALL apoc.do.when(scene IS NOT NULL, 'CREATE (place)-[:P89]->(scene)', '', {place: placeNew, scene: scene}) YIELD value
    RETURN e22.id AS id,
           name.value AS name,
           {from: NULL, to: NULL} AS date,
           apoc.map.removeKeys(object, ['id', 'vrcity_forceTextures']) AS origin,
           apoc.map.removeKey(file, 'id') AS file,
           apoc.map.removeKey(geoNew, 'id') AS location,
           object.vrcity_forceTextures AS vrcity_forceTextures,
           NULL AS misc,
           NULL as address,
           [] AS formerAddresses,
           [] AS tags
  `;

  const id = shortId.generate() + '_' + replace(req.body.name) + '_duplicate';

  const params = {
    id: req.params.id,
    e22id: 'e22_' + id,
    e41: {
      id: 'e41_' + id,
      value: req.body.name + ' Duplicate'
    },
    e53id: 'e53_' + id,
    e94id: 'e94_' + id
  };

  try {

    const results = await neo4j.writeTransaction(q, params);

    if (results[0]) {

      results[0].osm_overlap = await updateOSMIntersections(results[0].id);

      res.json(results[0]);

    } else {
      next(new NotFound());
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}
