import * as neo4j from 'neo4j-request';
import fs from 'fs-extra';
import * as path from 'path';
import errPkg from 'http-errors';
import moment from 'moment';
import { v4 as uuid } from 'uuid';
import * as shortId from 'shortid';
import { Euler, Matrix4 } from 'three';
import { checkRequiredProperties } from '../../modules/utils.js';
import logger from '../../modules/logger.js';
import Coordinates from '../../modules/coordinates.js';
import { computeModelCenter } from '../../modules/model-center.js';
import { updateOSMIntersections } from '../../modules/osm-intersection.js';
import { cleanOSMPlaces } from '../../modules/cleanup.js';
import * as config from '../../config.js';

const { BadRequest, Conflict, InternalServerError, NotFound } = errPkg;

/**
 * Get object by ID.
 * @param req {e.Request<{id: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export async function get(req, res, next) {

  // language=Cypher
  const q = `
    MATCH (e22:E22:UH4D {id: $id})-[:P1]->(name:E41),
          (e22)<-[:P67]-(object:D1)-[:P106]->(file:D9),
          (e22)<-[:P157]-(place:E53)-[:P168]->(geo:E94)
    OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(from:E61)
    OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(to:E61)
    OPTIONAL MATCH (e22)-[:P3]->(misc:E62)
    OPTIONAL MATCH (place)-[:P87]->(addr:E45)
    OPTIONAL MATCH (addr)-[:P139]-(fAddr:E45)
    WITH e22, name, misc, from, to, object, file, geo, addr, place, collect(fAddr.value) AS formerAddresses
    OPTIONAL MATCH (place)-[:P121]->(osmPlace:E53)
    WITH e22, name, misc, from, to, object, file, geo, addr, formerAddresses, collect(osmPlace.osm) AS osm_overlap
    OPTIONAL MATCH (e22)-[:has_tag]->(tag:TAG)
    RETURN e22.id AS id,
           name.value AS name,
           {from: from.value, to: to.value} AS date,
           apoc.map.removeKeys(object, ['id', 'vrcity_forceTextures']) AS origin,
           apoc.map.removeKey(file, 'id') AS file,
           apoc.map.removeKey(geo, 'id') AS location,
           object.vrcity_forceTextures AS vrcity_forceTextures,
           misc.value AS misc,
           addr.value as address,
           formerAddresses,
           osm_overlap,
           collect(tag.id) AS tags
  `;

  const params = {
    id: req.params.id
  };

  try {

    const results = await neo4j.readTransaction(q, params);

    if (results[0]) {
      res.json(results[0]);
    } else {
      next(new NotFound());
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * Update property specified by `prop`.
 * @param req {e.Request<{id: string}, any, any, {prop: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export async function update(req, res, next) {

  let q = `MATCH (e22:E22:UH4D {id: $id})<-[:P157]-(place:E53) `;

  const params = {
    id: req.params.id
  };

  const id = shortId.generate();

  switch (req.query.prop) {

    case 'name':
      q += `MATCH (e22)-[:P1]->(name:E41) SET name.value = $name`;
      params.name = req.body.name;
      break;

    case 'from':
      if (req.body.date.from) {
        // set from date
        const fromDate = moment(req.body.date.from);
        if (!fromDate.isValid()) {
          next(new BadRequest('Wrong date format'));
          return;
        }

        q += `
          MERGE (e22)<-[:P108]-(e12:E12:UH4D)-[:P4]->(e52:E52:UH4D)-[:P82]->(date:E61:UH4D)
          ON CREATE SET e12.id = $e12id, e52.id = $e52id, date.id = $e61id
          SET date.value = date($date)`;
        params.e12id = 'e12_' + id;
        params.e52id = 'e52_e12_' + id;
        params.e61id = 'e61_e52_e12_' + id;
        params.date = fromDate.format('YYYY-MM-DD');
      } else {
        // remove date
        q += `
          OPTIONAL MATCH (e22)<-[:P108]-(e12:E12)-[:P4]->(e52:E52)-[:P82]->(from:E61)
          DETACH DELETE e12, e52, from`;
      }
      break;

    case 'to':
      if (req.body.date.to) {
        // set to date
        const toDate = moment(req.body.date.to);
        if (!toDate.isValid()) {
          next(new BadRequest('Wrong date format'));
          return;
        }
        q += `
          MERGE (e22)<-[:P13]-(e6:E6:UH4D)-[:P4]->(e52:E52:UH4D)-[:P82]->(date:E61:UH4D)
          ON CREATE SET e6.id = $e6id, e52.id = $e52id, date.id = $e61id
          SET date.value = date($date)`;
        params.e6id = 'e6_' + id;
        params.e52id = 'e52_e6_' + id;
        params.e61id = 'e61_e52_e6_' + id;
        params.date = toDate.format('YYYY-MM-DD');
      } else {
        // remove date
        q += `
          OPTIONAL MATCH (e22)<-[:P13]-(e6:E6)-[:P4]->(e52:E52)-[:P82]->(to:E61)
          DETACH DELETE e6, e52, to`;
      }
      break;

    case 'misc':
      if (req.body.misc) {
        // set misc
        q += `
          MERGE (e22)-[:P3]->(misc:E62)
          ON CREATE SET misc.id = $misc.id
          SET misc.value = $misc.value`;
        params.misc = { id: 'e62_misc_' + id, value: req.body.misc };
      } else {
        // remove misc
        q += `OPTIONAL MATCH (e22)-[:P3]->(misc:E62) DETACH DELETE misc`
      }
      break;

    case 'address':
      q += `
        OPTIONAL MATCH (place)-[r:P87]->(:E45)
        DELETE r
        MERGE (addr:E45:UH4D {value: $address.value})
        ON CREATE SET addr.id = $address.id
        MERGE (place)-[:P87]->(addr)`;
      params.address = { id: 'e45_' + id, value: req.body.address };
      break;

    case 'tags':
      q += `
        OPTIONAL MATCH (e22)-[r:has_tag]->(:TAG)
        DELETE r
        WITH e22
        FOREACH (tag IN $tags |
          MERGE (t:TAG:UH4D {id: tag})
          MERGE (e22)-[:has_tag]->(t)
        )`;
      params.tags = req.body.tags || [];
      break;

    case 'origin': {
      // compute new center
      const rotation = new Euler(req.body.origin.omega, req.body.origin.phi, req.body.origin.kappa, 'YXZ');
      const matrix = new Matrix4().makeRotationFromEuler(rotation);
      const filePath = path.join(config.path.data, req.body.file.path, req.body.file.file);
      const center = await computeModelCenter(filePath, matrix);
      req.body.location = new Coordinates(req.body.origin.latitude, req.body.origin.longitude, req.body.origin.altitude)
        .add(center).toJSON();

      q += `
        MATCH (e22)<-[:P67]-(object:D1), (place)-[:P168]->(geo:E94)
        OPTIONAL MATCH (object)-[:P67]->(:E22)<-[:P157]-(:E53)-[:P168]->(geoDuplicate:E94)
        SET object += $origin,
            geo += $location,
            geoDuplicate += $location
      `;
      params.origin = req.body.origin;
      params.location = req.body.location;
      break;
    }

    case 'vrcity_forceTextures':
      q += `
        MATCH (e22)<-[:P67]-(object:D1)
        SET object.vrcity_forceTextures = $forceTextures
      `;
      params.forceTextures = !!req.body.vrcity_forceTextures;
      break;

    default:
      return next(new BadRequest('No supported update routine!'));

  }

  q += ' RETURN e22';

  try {

    const result = await neo4j.writeTransaction(q, params);

    if (result[0]) {

      // execute tasks after database update
      switch (req.query.prop) {
        case 'origin':
          req.body.osm_overlap = await updateOSMIntersections(req.params.id);
          await cleanOSMPlaces();
      }

      res.json(req.body);

    } else {
      next(new NotFound());
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * Object save request body
 * @typedef {Object} ObjectSaveRequest
 * @property {string} id
 * @property {string} name
 * @property {string} path
 * @property {string} file
 * @property {string} original
 * @property {string} type
 * @property {number[]} matrix
 * @property {number} latitude
 * @property {number} longitude
 * @property {number} altitude
 * @property {number} omega
 * @property {number} phi
 * @property {number} kappa
 * @property {string=} scene
 */

/**
 * Move uploaded files from temp folder to final destination and store in database.
 * @param req {e.Request<{}, any, ObjectSaveRequest>}
 * @param res {e.Response}
 * @param next {Function}
 * @return {Promise<void>}
 */
export async function save(req, res, next) {

  // check essential data
  const requiredProps = ['id', 'name', 'path', 'file', 'original', 'type', 'matrix', 'latitude', 'longitude', 'altitude', 'omega', 'phi', 'kappa'];
  if (!checkRequiredProperties(req.body, requiredProps)) {
    return next(new BadRequest('Missing values: ' + requiredProps.join('|')));
  }

  // check if tmp path exists
  const tmpPath = path.join(config.path.data, 'tmp', req.body.id);

  const exists = await fs.pathExists(tmpPath);
  if (!exists) {
    return next(new BadRequest(`Temporary folder ${req.body.id} does not exist!`));
  }

  // compute center
  const location = new Coordinates(req.body.latitude, req.body.longitude, req.body.altitude)

  try {

  const rotation = new Euler(req.body.omega, req.body.phi, req.body.kappa, 'YXZ');
  const matrix = new Matrix4().makeRotationFromEuler(rotation);
  const center = await computeModelCenter(path.join(tmpPath, req.body.file), matrix);
  location.add(center);

  } catch (e) {

    return next(new InternalServerError(e));

  }

  // parameters
  const id = shortId.generate();
  const relPath = `objects/${uuid()}/`;
  const absPath = path.join(config.path.data, relPath);

  const params = {
    scene: req.body.scene,
    e22id: 'e22_' + id,
    placeId: 'e53_' + id,
    e41: {
      id: 'e41_' + id,
      value: req.body.name
    },
    dobj: {
      id: 'd1_' + id,
      matrix: req.body.matrix,
      latitude: req.body.latitude,
      longitude: req.body.longitude,
      altitude: req.body.altitude,
      omega: req.body.omega,
      phi: req.body.phi,
      kappa: req.body.kappa,
    },
    file: {
      id: 'd9_' + id,
      path: relPath,
      file: req.body.file,
      type: req.body.type,
      original: req.body.original
    },
    geo: Object.assign({
      id: 'e94_' + id
    }, location.toJSON())
  };

  // language=Cypher
  const q = `
    ${req.body.scene ? 'MATCH (city:E53:UH4D {id: $scene})' : ''}
    CREATE (e22:E22:UH4D {id: $e22id})-[:P1]->(name:E41:UH4D $e41),
           (e22)<-[:P67]-(object:D1:UH4D $dobj)-[:P106]->(file:D9:UH4D $file),
           (e22)<-[:P157]-(place:E53:UH4D {id: $placeId})${req.body.scene ? '-[:P89]->(city)' : ''},
           (place)-[:P168]->(geo:E94:UH4D $geo)
    RETURN e22.id AS id,
           name.value AS name,
           {from: NULL, to: NULL} AS date,
           apoc.map.removeKey(object, 'id') AS origin,
           apoc.map.removeKey(file, 'id') AS file,
           apoc.map.removeKey(geo, 'id') AS location,
           NULL AS misc,
           NULL as address,
           [] AS formerAddresses,
           [] AS tags
  `;

  try {

    // move to final location
    logger.info('Move:', tmpPath, absPath);
    await fs.move(tmpPath, absPath);

    const results = await neo4j.writeTransaction(q, params);

    if (results[0]) {

      // update OSM intersections
      results[0].osm_overlap = await updateOSMIntersections(results[0].id);

      res.json(results[0]);

    } else {
      throw new Error('No nodes added for ' + req.body.file);
    }

  } catch (e) {

    // cleanup paths
    await fs.remove(tmpPath);
    await fs.remove(absPath);

    next(new InternalServerError(e));

  }

}

/**
 * Remove object from database and delete files.
 * However, files won't be deleted if there is a duplicate, i.e. other objects referencing the files.
 * @param req {e.Request<{id: string}>}
 * @param res {e.Response}
 * @param next {Function}
 * @return {Promise<void>}
 */
export async function remove(req, res, next) {

  // detach POI from object
  // language=Cypher
  const qPoi = `
    MATCH (e22:E22:UH4D {id: $id})<-[:P67]-(poi:E73)-[:P2]-(:E55 {id: "poi"}),
          (poi)-[:P67]->(e92:E92)
    OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(from:E61)
    OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(to:E61)
    MERGE (e92)-[:P160]->(e52:E52:UH4D)-[:P82]->(date:E61:UH4D)
      ON CREATE SET e52.id = replace(poi.id, "e73_", "e52_"), date.id = replace(poi.id, "e73_", "e61_")
    SET date.from = CASE WHEN from IS NULL THEN NULL ELSE from.value END,
    date.to = CASE WHEN to IS NULL THEN NULL ELSE to.value END
    RETURN poi
  `;

  // language=Cypher
  const qRemove = `
    MATCH (e22:E22:UH4D {id: $id})-[:P1]->(name:E41),
          (e22)<-[:P67]-(object:D1)-[:P106]->(file:D9),
          (e22)<-[:P157]-(place)-[:P168]->(geo:E94)
    OPTIONAL MATCH (object)-[:P67]->(duplicate:E22) WHERE NOT e22 = duplicate
    WITH e22, place, name, object, file, geo, duplicate, file.path AS path
    CALL apoc.do.when(duplicate IS NULL, 'DETACH DELETE object, file RETURN 1 AS check', '', { object: object, file: file }) YIELD value
    OPTIONAL MATCH (e22)<-[:P108]-(e12:E12)-[:P4]->(e12e52:E52)-[:P82]->(from:E61)
    OPTIONAL MATCH (e22)<-[:P13]-(e6:E6)-[:P4]->(e6e52:E52)-[:P82]->(to:E61)
    OPTIONAL MATCH (e22)-[:P3]->(misc:E62)
    DETACH DELETE e22, place, geo, name, e12, e12e52, from, e6, e6e52, to, misc
    RETURN path, duplicate`;

  const params = {
    id: req.params.id
  };

  // wrap in custom transaction
  const session = neo4j.session();
  const txc = session.beginTransaction();

  try {

    // execute cypher queries
    await txc.run(qPoi, params);

    const result = await txc.run(qRemove, params);
    const records = neo4j.extractRecords(result.records);

    if (!records[0]) {
      await txc.rollback();
      return next(new NotFound());
    }

    // check for corrupted path to not remove the wrong directory
    const relPath = records[0].path;
    if (!/^objects\/\S+$/.test(relPath)) {
      await txc.rollback();
      return next(new Conflict(`Path to delete does not match pattern!`));
    }

    // remove if there is no duplicate
    if (!records[0].duplicate) {
      logger.info('Remove:', relPath);
      await fs.remove(path.join(config.path.data, relPath));
    }

    await txc.commit();

    res.send();

  } catch (e) {

    await txc.rollback();
    next(new InternalServerError(e));

  } finally {

    await session.close();

  }

}
