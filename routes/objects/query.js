import errPkg from 'http-errors';
import * as neo4j from 'neo4j-request';
import moment from 'moment';

const { InternalServerError } = errPkg;

/**
 * @param req {e.Request<{scene: string}, {}, {}, {scene?: string, date?: string, lat?: string, lon?: string, r?: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export default async function query(req, res, next) {

  // check query parameter
  let date = req.query.date ? moment(req.query.date) : null;
  if (date && !date.isValid()) {
    date = null;
  }

  const spatialParams = {
    latitude: parseFloat(req.query.lat),
    longitude: parseFloat(req.query.lon),
    radius: parseFloat(req.query.r)
  };
  const doSpatialQuery = !isNaN(spatialParams.latitude) && !isNaN(spatialParams.longitude) && !isNaN(spatialParams.radius);

  const params = {
    scene: req.query.scene,
    geo: spatialParams
  };

  let q = '';

  if (doSpatialQuery) {
    q += `WITH point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint `;
  }

  q += 'MATCH ' + (req.query.scene ? '(:E53:UH4D {id: $scene})<-[:P89]-' : '');
  q += `(place:E53)-[:P157]->(e22:E22:UH4D)-[:P1]->(name:E41),
        (e22)<-[:P67]-(object:D1)-[:P106]->(file:D9),
        (place)-[:P168]->(geo:E94) `;

  if (doSpatialQuery) {
    q += `WHERE distance(userPoint, point({latitude: geo.latitude, longitude: geo.longitude})) < $geo.radius`;
  }

  q += `
    OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(from:E61)
    OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(to:E61)`;

  if (date) {
    q += `
      WITH e22, name, object, file, from, to, geo, place
      WHERE (from IS NULL OR from.value < date($date)) AND (to IS NULL OR to.value > date($date))`;

    params.date = date.format('YYYY-MM-DD');
  }

  q += ` OPTIONAL MATCH (place)-[:P121]->(osmPlace:E53)`;

  q += `
    RETURN e22.id AS id,
           name.value AS name,
           {from: from.value, to: to.value} AS date,
           apoc.map.removeKeys(object, ['id', 'vrcity_forceTextures']) AS origin,
           apoc.map.removeKey(file, 'id') AS file,
           apoc.map.removeKey(geo, 'id') AS location,
           object.vrcity_forceTextures AS vrcity_forceTextures,
           collect(osmPlace.osm) AS osm_overlap`;

  try {

    const results = await neo4j.readTransaction(q, params);
    res.json(results);

  } catch (e) {

    next(new InternalServerError(e));

  }

}
