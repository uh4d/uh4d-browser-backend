import { Router } from 'express';
import { logFileUpload, mUpload } from '../middlewares.js';

const router = Router({ mergeParams: true });

import query from './query.js';
import * as object from './object.js';
router.get('/', query);
router.post('/', object.save);

import * as tmp from './tmp.js';
router.post('/tmp', mUpload.single('uploadModel'), logFileUpload, tmp.upload);
router.delete('/tmp/:id', tmp.remove);

router.get('/:id', object.get);
router.patch('/:id', object.update);
router.delete('/:id', object.remove);

import duplicate from './duplicate.js';
router.post('/:id/duplicate', duplicate);

export default router;
