import * as path from 'path';
import * as shortId from 'shortid';
import fs from 'fs-extra';
import * as mime from 'mime-types';
import * as JSZip from 'jszip';
import { execFile } from 'child-process-promise';
import LineByLineReader from 'line-by-line';
import errPkg from 'http-errors';
import * as config from '../../config.js';
import { escapeRegex, replace } from '../../modules/utils.js';
import { processModel } from '../../modules/process-model.js';

const { HttpError, InternalServerError, NotFound, UnsupportedMediaType } = errPkg;

/**
 * Upload file to temporary location.
 * @param req {e.Request}
 * @param res {e.Response}
 * @param next {Function}
 * @return {Promise<void>}
 */
export async function upload(req, res, next) {

  const id = shortId.generate();
  const relPath = `tmp/${id}/`;
  const absPath = path.join(config.path.data, relPath);
  const fileName = replace(req.file.originalname);

  try {

    // create tmp folder
    await fs.ensureDir(absPath);
    // copy uploaded file to tmp folder
    await fs.rename(req.file.path, path.join(absPath, fileName));

    const responseBody = {
      id,
      path: relPath,
      original: fileName
    };

    // lookup mime type
    switch (mime.lookup(fileName)) {
      case 'application/zip': {
        // process zip
        const meta = await processZip({path: absPath, name: fileName});
        Object.assign(responseBody, {
          name: meta.name,
          file: meta.gltf,
          type: meta.type
        });
        break;
      }
      case 'model/obj': {
        // process obj directly
        const meta = await processModel({ path: absPath, name: fileName });
        Object.assign(responseBody, {
          name: meta.name,
          file: meta.gltf,
          type: meta.type
        });
        break;
      }
      default:
        // cleanup path
        await fs.remove(absPath);

        next(new UnsupportedMediaType(fileName + ' is none of the following formats: obj'));
        return;
    }

    res.json(responseBody);

  } catch (e) {

    // cleanup paths
    await fs.remove(req.file.path);
    await fs.remove(absPath);

    if (e instanceof HttpError) {
      next(e);
    } else {
      next(new InternalServerError(e));
    }

  }

}

/**
 * Delete temporary files.
 * @param req {e.Request<{id: string}>}
 * @param res {e.Response}
 * @param next {Function}
 * @return {Promise<void>}
 */
export async function remove(req, res, next) {

  const absPath = path.join(config.path.data, 'tmp', req.params.id);

  try {

    const exists = await fs.pathExists(absPath);
    if (!exists) {
      next(new NotFound(`Temporary folder ${req.params.id} does not exist!`));
      return;
    }

    await fs.remove(absPath);

    res.send();

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 *  @param file {{path: string, name: string}}
 */
async function processZip(file) {

  const zipFile = path.join(file.path, file.name);
  const basename = path.basename(file.name, '.zip');
  const objTempFile = path.join(file.path, basename + '.obj');

  // read zip file
  const zipData = await fs.readFile(zipFile);
  const zip = await JSZip.loadAsync(zipData);

  // extract obj file
  const objResults = zip.file(/.+\.obj$/i);
  if (objResults[0]) {
    await extractFileFromZip(objResults[0], objTempFile);
  } else {
    throw new UnsupportedMediaType('Zip file does not contain one of the following formats: obj');
  }

  // find and extract mtl files and textures
  const mtlFiles = [];
  const imgFiles = [];

  // find (and replace) mtl files
  await findAndReplaceLine(objTempFile, /^(mtllib)\s+(?:.*[\\/])?(.+)$/i, matches => {
    mtlFiles.push(matches[2]);
    return `${matches[1]} ${matches[2]}`;
  });

  // iterate over mtl files
  for (const mtl of mtlFiles) {

    const mtlResults = zip.file(new RegExp(escapeRegex(mtl) + '$'));

    if (mtlResults[0]) {

      // extract mtl file
      const mtlFile = path.join(file.path, mtl);
      await extractFileFromZip(mtlResults[0], mtlFile);

      // find textures
      await findAndReplaceLine(mtlFile, /^(\s*map.+)\s+(?:.*[\\/])?(.+)$/, matches => {
        const tFile = matches[2];
        let tFileMeta = imgFiles.find(m => m.originalName === tFile);

        if (!tFileMeta) {
          tFileMeta = {
            originalName: tFile,
            approvedName: tFile
          };
          // rename if not jpg or png
          if (!['.jpg', '.png'].includes(path.extname(tFile))) {
            tFileMeta.approvedName = tFile.replace(/\.\S+$/, '.jpg');
          }
          imgFiles.push(tFileMeta);
        }

        return `${matches[1]} ${tFileMeta.approvedName}`;
      });

    }

  }

  // iterate over found texture files
  for (const img of imgFiles) {

    const imgResults = zip.file(new RegExp(escapeRegex(img.originalName) + '$'));

    if (imgResults[0]) {

      // extract image file
      await extractFileFromZip(imgResults[0], path.join(file.path, img.originalName));

      // resize and convert image
      await execFile(config.exec.ImageMagick, [path.join(file.path, img.originalName), '-resize', '2048x2048>', path.join(file.path, img.approvedName)]);

      // resize for mobile usage (max 1024 pixels)
      await execFile(config.exec.ImageMagick, [path.join(file.path, img.originalName), '-resize', '1024x1024>', path.join(file.path, img.approvedName.replace(/\.[^.]+$/, '_mobile$&'))]);

      // if approved name differs from original name, remove the original file
      if (img.originalName !== img.approvedName) {
        await fs.remove(path.join(file.path, img.originalName));
      }

    }

  }

  // process obj
  const meta = await processModel({ path: file.path, name: path.basename(objTempFile) });

  // remove extracted obj and mtl
  await fs.remove(objTempFile);
  for (const mtl of mtlFiles) {
    await fs.remove(path.join(file.path, mtl));
  }

  return meta;

}

/**
 * Extract a file from a zip object.
 * @param zipObject {JSZipObject}
 * @param outFile {string}
 * @return {Promise<void>}
 */
function extractFileFromZip(zipObject, outFile) {

  return new Promise((resolve, reject) => {

    zipObject.nodeStream()
      .pipe(fs.createWriteStream(outFile))
      .on('finish', () => { resolve(); })
      .on('error', err => { reject(err); });

  });

}

/**
 * @callback RegExpCallback
 * @param matches {RegExpExecArray}
 * @return {string}
 */

/**
 * Read file line by line and look for patterns defined by a regular expression.
 * If a match is found, the callback is triggered, the string returned replaces
 * the original string in this line.
 * @param file {string}
 * @param regex {RegExp}
 * @param callback {RegExpCallback}
 * @return {Promise<void>}
 */
function findAndReplaceLine(file, regex, callback) {

  return new Promise((resolve, reject) => {

    const tempFile = file + '.tmp';
    const lr = new LineByLineReader(file, { skipEmptyLines: false });
    const writeStream = fs.createWriteStream(tempFile);

    lr.on('error', (err) => {
      writeStream.end(async () => {
        await fs.remove(tempFile);
        reject(err);
      });
    });

    writeStream.on('error', async (err) => {
      lr._readStream.destroy();
      await fs.remove(tempFile);
      reject(err);
    });

    lr.on('line', line => {

      const matches = regex.exec(line);
      if (matches) {
        const replaced = callback(matches);
        writeStream.write(`${replaced}\n`);
      } else {
        writeStream.write(`${line}\n`);
      }

    });

    lr.on('end', () => {

      writeStream.end(async () => {

        try {
          await fs.remove(file);
          await fs.rename(tempFile, file);
          resolve();
        } catch (e) {
          reject(e);
        }

      });

    });

  });

}

