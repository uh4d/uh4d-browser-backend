import { Router } from 'express';
import * as neo4j from 'neo4j-request';
import errPkg from 'http-errors';
import { escapeRegex } from '../../modules/utils.js';

const { InternalServerError, NotFound } = errPkg;

const router = Router({ mergeParams: true });

router.get('/', query);
router.get('/:id', get);

export default router;

/**
 * @param req {e.Request<{}, any, {}, {search?: string}>}
 * @param res
 * @param next
 * @return {Promise<void>}
 */
async function query(req, res, next) {

  // language=Cypher
  const q = `
    MATCH (e21:E21:UH4D)-[:P131]->(name:E82)
    WHERE name.value =~ $search
    RETURN e21.id as id, name.value AS value`;

  const params = {
    search: `(?i).*${req.query.search ? escapeRegex(req.query.search) : ''}.*`
  };

  try {

    const results = await neo4j.readTransaction(q, params);
    res.json(results);

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * @param req {e.Request<{id: string}>}
 * @param res
 * @param next
 * @return {Promise<void>}
 */
async function get(req, res, next) {

  // language=Cypher
  const q = `
    MATCH (e21:E21:UH4D {id: $id})-[:P131]->(name:E82)
    RETURN e21.id AS id, name.value AS value`;

  const params = {
    id: req.params.id
  };

  try {

    const results = await neo4j.readTransaction(q, params);

    if (results[0]) {
      res.json(results[0]);
    } else {
      next(new NotFound(`Person with ID ${req.params.id} not found!`));
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}
