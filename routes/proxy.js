import request from 'request';
import errPkg from 'http-errors';

const { BadRequest } = errPkg;

/**
 * Proxy any URL request.
 * @param req {e.Request<{}, any, null, {url: string}>}
 * @param res {e.Response}
 * @param next
 */
export function proxyRequest(req, res, next) {

  if (!req.query.url) {
    return next(new BadRequest('No `url` query parameter provided!'));
  }

  request.get(req.query.url)
    .on('response', response => {
      response.headers['access-control-allow-origin'] = '*';
    })
    .pipe(res);

}
