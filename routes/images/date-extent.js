import * as neo4j from 'neo4j-request';
import errPkg from 'http-errors';

const { InternalServerError } = errPkg;

/**
 * @param req {e.Request<null, any, any, {scene: string; lat: string, lon: string, r: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export async function get(req, res, next) {

  // language=Cypher
  const q = `
    MATCH (:E53:UH4D {id: $scene})<-[:P89]-(:E53)<-[:P7]-(e65:E65)-[:P94]->(image:E38)
    CALL {
      WITH e65
      MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
      WHERE exists(date.from)
      RETURN date.to AS d
      ORDER BY d
      LIMIT 1
      UNION
      WITH e65
      MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
      WHERE exists(date.to)
      RETURN date.to AS d
      ORDER BY d DESC
      LIMIT 1
    }
    RETURN d
  `;

  try {

    const results = await neo4j.readTransaction(q, { scene: req.query.scene });

    // if there are no images with dates yet, get standard dates
    if (!results[0]) {
      results.push(
        { d: '1800-01-01'},
        { d: '2020-01-01'}
      );
    }

    res.json({
      from: results[0].d,
      to: results[1].d
    });

  } catch (e) {

    next(new InternalServerError(e));

  }

}
