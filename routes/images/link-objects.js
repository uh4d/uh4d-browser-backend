import errPkg from 'http-errors';
import * as neo4j from 'neo4j-request';

const { InternalServerError, NotFound } = errPkg;

/**
 * @param req {e.Request<{id: string}, any, {weights: Object<string, number>}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export async function linkToObjects(req, res, next) {

  const weights = [];
  if (req.body.weights) {
    for (const [key, value] of Object.entries(req.body.weights)) {
      weights.push({
        id: key,
        weight: value
      });
    }
  }

  // language=Cypher
  let q = `
    MATCH (image:E38:UH4D {id: $id})
    OPTIONAL MATCH (image)-[r:P138]->(:E22)
    DELETE r`;

  if (weights.length) {
    // language=Cypher
    q += `
      WITH image
      UNWIND $weights AS value
      MATCH (e22:E22:UH4D {id: value.id})
      MERGE (image)-[r:P138]->(e22)
      SET r.weight = value.weight
      RETURN image.id AS image, collect({id: e22.id, weight: r.weight}) AS weights
  `;
  } else {
    q += `
      RETURN image.id AS image, [] AS weights
    `;
  }

  const params = {
    id: req.params.id,
    weights
  };

  try {

    const results = await neo4j.writeTransaction(q, params);

    if (results[0]) {
      res.json({
        image: results[0].image,
        objects: results[0].weights.reduce((map, curr) => {
          map[curr.id] = curr.weight;
          return map;
        }, {})
      });
    } else {
      next(new NotFound(`Image with ID ${req.params.id} not found!`));
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}
