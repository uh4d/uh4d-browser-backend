import * as neo4j from 'neo4j-request';
import errPkg from 'http-errors';
import { escapeRegex } from '../../modules/utils.js';

const { InternalServerError } = errPkg;

/**
 * Image query object.
 * @typedef {Object} ImageQuery
 * @property {string} scene
 * @property {string|string[]} q
 * @property {string} from
 * @property {string} to
 * @property {string} undated
 * @property {string} lat
 * @property {string} lon
 * @property {string} r
 * @property {string} weights
 */

/**
 * @param req {e.Request<{}, any, {}, ImageQuery>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export default async function query(req, res, next) {

  const qFilter = [];
  const objList = [];
  const regexPosList = [];
  const regexNegList = [];
  const regexAuthorList = [];
  const regexOwnerList = [];

  // first triage
  (req.query.q ? Array.isArray(req.query.q) ? req.query.q : [req.query.q] : []).forEach(value => {
    if (/^obj:/.test(value)) {
      objList.push(value.replace(/^obj:/, ''));
    } else {
      qFilter.push(value);
    }
  });

  const spatialParams = {
    latitude: parseFloat(req.query.lat),
    longitude: parseFloat(req.query.lon),
    radius: parseFloat(req.query.r)
  };
  const doSpatialQuery = !isNaN(spatialParams.latitude) && !isNaN(spatialParams.longitude) && !isNaN(spatialParams.radius);

  let q = '';

  if (doSpatialQuery) {
    q += 'WITH point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint ';
  }

  if (req.query.scene) {
    q += `MATCH (:E53:UH4D {id: $scene})<-[:P89]-`;
  } else {
    q += 'MATCH '
  }

  q += `(place:E53)<-[:P7]-(e65:E65)-[:P94]->(image:E38)`;

  if (objList.length) {
    q += `
      MATCH (image)-[:P138]->(e22:E22)
      WHERE e22.id IN $objList
    `;
  }

  if (doSpatialQuery) {
    q += `
      MATCH (place)-[:P168]->(geo:E94)
      WHERE distance(userPoint, point({latitude: geo.latitude, longitude: geo.longitude})) < $geo.radius
    `;
  }

  if (req.query.from && req.query.to && req.query.from !== 'null' && req.query.to !== 'null') {
    q += `
      OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
      WITH image, place, e65, date
      WHERE date ${req.query.undated === 'true' ? 'IS NULL OR': 'IS NOT NULL AND'} date.to > date($from) AND date.from < date($to)
    `;
  }

  q += `
    WITH image, place, e65
    MATCH (image)-[:P106]->(file:D9),
          (image)-[:P102]->(title:E35)
    OPTIONAL MATCH (e65)-[:P14]->(authorId:E21)-[:P131]->(author:E82)
    OPTIONAL MATCH (image)-[:P105]->(ownerId:E40)-[:P131]->(owner:E82)
    OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
    // OPTIONAL MATCH (image)-[:has_spatial]->(spatial:Spatial)
    OPTIONAL MATCH (e65)-[:P16]->(spatial:E22)
    OPTIONAL MATCH (place)-[:P168]->(geo:E94)
    OPTIONAL MATCH (image)-[:P2]->(tSnot:E55 {id: "image:spatial:not_possible"})
    OPTIONAL MATCH (image)-[:P2]->(tSyet:E55 {id: "image:spatial:not_yet_possible"})
    OPTIONAL MATCH (image)-[:has_tag]->(tag:TAG)
    WITH image, file, title, authorId, author, date, ownerId, owner, spatial, geo, tSnot, tSyet, collect(tag.id) AS tags
  `;

  qFilter.forEach((value, index) => {

    const qPrefix = index === 0 ? ' WHERE' : ' AND';

    if (value === 'spatial:set') {

      q += qPrefix + ' spatial IS NOT NULL';

    } else if (value === 'spatial:unset') {

      q += qPrefix + ' spatial IS NULL';

    } else if (/^author:/.test(value)) {

      if (regexAuthorList.length < 1) {
        q += qPrefix + ` any(e IN $regexAuthorList
          WHERE authorId.id = e
          OR author.value =~ "(?i).*" + e + ".*"
        )`;
      }
      regexAuthorList.push(escapeRegex(value.replace(/^author:/, '')));

    } else if (/^owner:/.test(value)) {

      if (regexOwnerList.length < 1) {
        q += qPrefix + ` any(e IN $regexOwnerList
          WHERE ownerId.id = e
          OR owner.value =~ "(?i).*" + e + ".*"
        )`;
      }
      regexOwnerList.push(escapeRegex(value.replace(/^owner:/, '')));

    } else if (/^-/.test(value)) {

      if (regexNegList.length < 1) {
        q += qPrefix + ` none(e IN $regexNegList
          WHERE title.value =~ e
          OR author.value =~ e
          OR owner.value =~ e
          OR any(tag IN tags WHERE tag =~ e)
        )`;
      }
      regexNegList.push(`(?i).*${escapeRegex(value.replace(/^-/, '').replace(/"/g, ''))}.*`);

    } else {

      if (regexPosList.length < 1) {
        q += qPrefix + ` all(e IN $regexPosList
          WHERE title.value =~ e
          OR author.value =~ e
          OR owner.value =~ e
          OR any(tag IN tags WHERE tag =~ e)
        )`;
      }
      regexPosList.push(`(?i).*${escapeRegex(value.replace(/"/g, ''))}.*`);

    }

  });

  if (req.query.weights === '1') {
    q += `
      WITH image, file, title, author, date, owner, spatial, geo, tSnot, tSyet, tags
      OPTIONAL MATCH (image)-[rw:P138]->(e22:E22)`;
  }

  q += ` RETURN
    image.id AS id,
    apoc.map.removeKey(file, 'id') AS file,
    title.value AS title,
    author.value AS author,
    owner.value AS owner,
    date,
    CASE
      WHEN spatial IS NULL THEN NULL
      ELSE apoc.map.removeKey(apoc.map.merge(geo, spatial), 'id')
    END AS camera,
    CASE
      WHEN spatial IS NOT NULL THEN CASE WHEN spatial.method = 'pipeline' THEN 4 ELSE 1 END
      WHEN tSnot IS NOT NULL THEN 2
      WHEN tSyet IS NOT NULL THEN 3
      ELSE 0
    END AS spatialStatus
  `;

  if (req.query.weights === '1') {
    q += `,
      CASE
        WHEN e22 IS NULL THEN []
        ELSE collect({id: e22.id, weight: rw.weight})
      END AS objects`;
  }

  const params = {
    scene: req.query.scene,
    from: req.query.from,
    to: req.query.to,
    regexPosList,
    regexNegList,
    regexAuthorList,
    regexOwnerList,
    objList,
    geo: spatialParams
  };

  try {

    const results = await neo4j.readTransaction(q, params);

    if (req.query.weights === '1') {
      results.forEach(img => {
        img.objects = img.objects.reduce((map, curr) => {
          map[curr.id] = curr.weight;
          return map;
        }, {});
      });
    }

    res.json(results);

  } catch (e) {

    next(new InternalServerError(e));

  }

}
