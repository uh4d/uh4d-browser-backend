import * as shortId from 'shortid';
import * as path from 'path';
import fs from 'fs-extra';
import errPkg from 'http-errors';
import * as config from '../../config.js';
import { replace } from '../../modules/utils.js';
import processImage from '../../modules/process-image.js';
import { writeImageToDatabase } from '../../import/modules/database.js';
import { getImage } from './image.js';

const { BadRequest, InternalServerError } = errPkg;

/**
 * @param req {e.Request}
 * @param res {e.Response}
 * @param next {Function}
 * @return {Promise<void>}
 */
export default async function upload(req, res, next) {

  // check required data
  if (!req.body.scene) {
    next(new BadRequest('Missing essential data scene'));
    return fs.remove(req.file.path);
  }

  const tmpDir = path.join(config.path.data, 'tmp', shortId.generate());
  const fileName = replace(req.file.originalname);
  const tmpFile = path.join(tmpDir, fileName);

  try {

    // create tmp folder
    await fs.ensureDir(tmpDir);
    // copy uploaded file to tmp folder
    await fs.rename(req.file.path, tmpFile);

    const meta = {
      scene: req.body.scene,
      title: path.parse(req.file.originalname).name,
      file: await processImage(tmpFile, tmpDir) // process image
    };

    // copy into image folder
    await fs.copy(path.join(tmpDir, meta.file.path), path.join(config.path.data, meta.file.path));

    try {

      const result = await writeImageToDatabase(meta);

      // retrieve image meta data
      const image = await getImage(result.image.id);
      res.json(image);

    } catch (e) {

      await fs.remove(path.join(config.path.data, meta.file.path))
      return next(new InternalServerError(e));

    }

  } catch (e) {

    next(new InternalServerError(e));

  } finally {

    // remove temporary folder
    await fs.remove(tmpDir);

  }

}
