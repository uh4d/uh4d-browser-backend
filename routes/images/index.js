import { Router } from 'express';
import { logFileUpload, mUpload } from '../middlewares.js';

const router = Router({ mergeParams: true });

import * as dateExtent from './date-extent.js';
router.get('/dateExtent', dateExtent.get);

import query from './query.js';
router.get('/', query);

import upload from './upload.js';
router.post('/', mUpload.single('uploadImage'), logFileUpload, upload);

import * as image from './image.js';
router.get('/:id', image.get);
router.patch('/:id', image.update);
// router.post('/:id/spatial', image.setSpatial);

import * as link from './link-objects.js';
router.post('/:id/link', link.linkToObjects);

import * as fileUpdate from './file-update.js';
router.get('/:id/file/check', fileUpdate.check);
router.get('/:id/file/update', fileUpdate.update);

export default router;
