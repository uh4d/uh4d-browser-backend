import * as neo4j from 'neo4j-request';
import errPkg from 'http-errors';
import { Euler, Matrix4, Vector3 } from 'three';
import * as utm from 'utm';
import * as shortId from 'shortid';
import parseDate from '../../modules/parse-date.js';
import * as utils from '../../modules/utils.js';
import { checkRequiredProperties } from '../../modules/utils.js';

const { BadRequest, InternalServerError, NotFound } = errPkg;

/**
 * Get image by ID. If `?regexp=1`, it will look for image where ID parameter is part of the image ID.
 * @param id {string}
 * @param queryParams {{regexp?: string}}
 * @return {Promise<Object|undefined>}
 */
export async function getImage(id, queryParams = {}) {

  let q = '';

  if (queryParams.regexp === '1') {
    q += `MATCH (image:E38:UH4D)
      WHERE image.id =~ ".*" + $id + ".*"
      MATCH `;
  } else {
    q += 'MATCH (image:E38:UH4D {id: $id}),';
  }

  // language=Cypher
  q += `  (image)<-[:P94]->(e65:E65),
          (image)-[:P106]->(file:D9),
          (image)-[:P102]->(title:E35),
          (image)-[:P48]->(identifier:E42)
    OPTIONAL MATCH (e65)-[:P14]->(:E21)-[:P131]->(author:E82)
    OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
    OPTIONAL MATCH (image)-[:P105]->(:E40)-[:P131]->(owner:E82)
    OPTIONAL MATCH (image)-[:P3]->(desc:E62)-[:P3_1]->(:E55 {id: "image_description"})
    OPTIONAL MATCH (image)-[:P3]->(misc:E62)-[:P3_1]->(:E55 {id: "image_miscellaneous"})
    OPTIONAL MATCH (e65)-[:P16]->(spatial:E22)
    OPTIONAL MATCH (e65)-[:P7]->(:E53)-[:P168]->(geo:E94)
    OPTIONAL MATCH (image)-[:P2]->(tSnot:E55 {id: "image:spatial:not_possible"})
    OPTIONAL MATCH (image)-[:P2]->(tSyet:E55 {id: "image:spatial:not_yet_possible"})
    OPTIONAL MATCH (image)-[:has_tag]->(tag:TAG)
    
    RETURN image.id AS id,
      apoc.map.removeKey(file, 'id') AS file,
      title.value AS title,
      identifier.permalink AS permalink,
      identifier.slub_cap_no AS captureNumber,
      author.value AS author,
      date {.*, from: toString(date.from), to: toString(date.to)} AS date,
      owner.value AS owner,
      desc.value AS description,
      misc.value AS misc,
      CASE
        WHEN spatial IS NULL THEN NULL
        ELSE apoc.map.removeKey(apoc.map.merge(geo, spatial), 'id')
      END AS camera,
      CASE
        WHEN spatial IS NOT NULL THEN CASE WHEN spatial.method = 'pipeline' THEN 4 ELSE 1 END
        WHEN tSnot IS NOT NULL THEN 2
        WHEN tSyet IS NOT NULL THEN 3
        ELSE 0
      END AS spatialStatus, 
      collect(tag.id) AS tags`;

  const params = { id };

  return (await neo4j.readTransaction(q, params))[0];

}

/**
 * Get image by ID. If `?regexp=1`, it will look for image where ID parameter is part of the image ID.
 * @param req {e.Request<{id: string}, any, {}, {regexp?: string}>}
 * @param res {e.Response}
 * @param next {Function}
 * @return {Promise<void>}
 */
export async function get(req, res, next) {

  try {

    const result = await getImage(req.params.id, req.query);

    if (result) {
      res.json(result);
    } else {
      next(new NotFound(`Image with ID ${req.params.id} not found!`));
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * Update property specified by `prop`.
 * @param req {e.Request<{id: string}, any, any, {prop: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export async function update(req, res, next) {

  const id = shortId.generate();

  let q = `MATCH (image:E38:UH4D {id: $id})<-[:P94]-(e65:E65)-[:P7]->(place:E53) `;

  const params = {
    id: req.params.id
  };

  switch (req.query.prop) {

    case 'title':
      if (!checkRequiredProperties(req.body, ['title'])) {
        return next(new BadRequest('Missing values: title'));
      }

      q += `MATCH (image)-[:P102]->(title:E35) SET title.value = $title`;
      params.title = req.body.title;
      break;

    case 'date': {
      const date = parseDate(req.body.date.value);
      q += `
        MERGE (e65)-[:P4]->(e52:E52:UH4D)-[:P82]->(date:E61:UH4D)
          ON CREATE SET e52.id = $e52id, date.id = $e61id 
        SET date.value = $date.value`;
      if (date) {
        req.body.date = date;
        q += `,
            date.from = date($date.from),
            date.to = date($date.to),
            date.display = $date.display`;
      }
      params.date = req.body.date;
      params.e52id = 'e52_' + id;
      params.e61id = 'e61_e52_' + id;
      break;
    }

    case 'description':
      q += `MATCH (tdesc:E55:UH4D {id: "image_description"}) `;
      if (req.body.description) {
        // set desc
        q += `
          MERGE (image)-[:P3]->(desc:E62:UH4D)-[:P3_1]->(tdesc)
            ON CREATE SET desc.id = $desc.id
          SET desc.value = $desc.value`;
        params.desc = { id: 'e62_desc_' + id, value: req.body.description };
      } else {
        // remove desc
        q += `OPTIONAL MATCH (image)-[:P3]->(desc:E62)-[:P3_1]->(tdesc) DETACH DELETE desc`;
      }
      break;

    case 'misc':
      q += `MATCH (tmisc:E55:UH4D {id: "image_miscellaneous"}) `;
      if (req.body.misc) {
        // set misc
        q += `
          MERGE (image)-[:P3]->(misc:E62:UH4D)-[:P3_1]->(tmisc)
            ON CREATE SET misc.id = $misc.id
          SET misc.value = $misc.value`;
        params.misc = { id: 'e62_misc_' + id, value: req.body.misc };
      } else {
        // remove misc
        q += `OPTIONAL MATCH (image)-[:P3]->(misc:E62)-[:P3_1]->(tmisc) DETACH DELETE misc`;
      }
      break;

    case 'author':
      q += `OPTIONAL MATCH (e65)-[r14:P14]->(:E21)-[:P131]->(:E82)`;
      if (req.body.author) {
        q += `
          MERGE (e21:E21:UH4D)-[:P131]->(e82:E82:UH4D {value: $name})
            ON CREATE SET e21.id = $e21id, e82.id = $e82id
          CREATE (e65)-[:P14]->(e21)`;
        params.name = req.body.author;
        params.e21id = `e21_${id}_${utils.replace(req.body.author)}`;
        params.e82id = `e82_${id}_${utils.replace(req.body.author)}`;
      }
      q += ` DELETE r14`;
      break;

    case 'owner':
      q += `OPTIONAL MATCH (image)-[r105:P105]->(:E40)-[:P131}->(:E82)`;
      if (req.body.owner) {
        q += `
          MERGE (e40:E40:UH4D)-[:P131]->(e82:E82:UH4D {value: $owner})
            ON CREATE SET e40.id = $e40id, e82.id = $e82id
          CREATE (image)-[:P105]->(e40)`;
        params.owner = req.body.owner;
        params.e40id = `e40_${id}_${utils.replace(req.body.owner)}`;
        params.e82id = `e82_${id}_${utils.replace(req.body.owner)}`;
      }
      q += ' DELETE r105';
      break;

    case 'tags':
      q += `
        OPTIONAL MATCH (image)-[rtag:has_tag]->(:TAG)
        DELETE rtag
        WITH image
        FOREACH (tag in $tags |
          MERGE (t:TAG:UH4D {id: tag})
          MERGE (image)-[:has_tag]->(t)
        )`;
      params.tags = req.body.tags || [];
      break;

    case 'camera': {
      const requiredProps = ['ck', 'offset', 'latitude', 'longitude', 'altitude', 'omega', 'phi', 'kappa'];
      if (!checkRequiredProperties(req.body.camera, requiredProps)) {
        return next(new BadRequest('Missing values: ' + requiredProps.join('|')));
      }

      q += `
        OPTIONAL MATCH (image)-[rSnot:P2]->(:E55 {id: "image:spatial:not_possible"})
        OPTIONAL MATCH (image)-[rSyet:P2]->(:E55 {id: "image:spatial:not_yet_possible"})
        MERGE (e65)-[:P16]->(spatial:E22:UH4D)
        MERGE (place)-[:P168]->(geo:E94:UH4D)
        SET spatial = $spatial,
            geo = $geo
        DELETE rSnot, rSyet
      `;
      params.spatial = {
        id: 'camera_' + req.body.id,
        matrix: req.body.camera.matrix,
        ck: req.body.camera.ck,
        offset: req.body.camera.offset,
        k: [0],
        omega: req.body.camera.omega,
        phi: req.body.camera.phi,
        kappa: req.body.camera.kappa,
        method: 'manual'
      };
      params.geo = {
        id: 'e94_' + req.body.id,
        latitude: req.body.camera.latitude,
        longitude: req.body.camera.longitude,
        altitude: req.body.camera.altitude
      };
      req.body.spatialStatus = 1;
      break;
    }

    case 'spatialStatus':
      // status can only be changed, if not yet spatialized
      try {
        const result = await neo4j.readTransaction(`MATCH (:E53:UH4D {id: $scene})<-[:P89]-(:E53)<-[:P7]-(e65:E65)-[:P94]->(image:E38:UH4D {id: $id}) OPTIONAL MATCH (image)-[:has_spatial]->(spatial:Spatial) RETURN spatial`, params);
        if (result[0].spatial) {
          return next(new BadRequest('Image already properly spatialized. Status cannot be updated!'));
        }
      } catch (e) {
        next(new InternalServerError(e));
      }
      // update status
      q += `
        OPTIONAL MATCH (image)-[rSnot:P2]->(:E55 {id: "image:spatial:not_possible"})
        OPTIONAL MATCH (image)-[rSyet:P2]->(:E55 {id: "image:spatial:not_yet_possible"})
      `;
      switch (req.body.spatialStatus) {
        case 0:
          break;
        case 1:
          return next(new BadRequest('spatialStatus: 1 can not be set manually! Use `camera` property instead.'));
        case 2:
          q += ' MERGE (t:E55:UH4D {id: $spatialType}) CREATE (image)-[:P2]->(t)';
          params.spatialType = 'image:spatial:not_possible';
          break;
        case 3:
          q += ' MERGE (t:E55:UH4D {id: $spatialType}) CREATE (image)-[:P2]->(t)';
          params.spatialType = 'image:spatial:not_yet_possible';
          break;
        case 4:
          return next(new BadRequest('spatialStatus: 4 can not be set manually! Use POST `pipeline/images` route instead.'));
        default:
          return next(new BadRequest('No valid spatialStatus!'));
      }
      q += ' DELETE rSnot, rSyet';

  }

  q += ' RETURN image';

  try {

    const result = await neo4j.writeTransaction(q, params);

    if (result[0]) {
      res.json(req.body);
    } else {
      next(new NotFound());
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * Object save request body
 * @typedef {Object} SpatialSaveRequest
 * @property {string} id
 * @property {number[]} matrix
 * @property {number} latitude
 * @property {number} longitude
 * @property {number} altitude
 * @property {number} omega
 * @property {number} phi
 * @property {number} kappa
 */

/**
 * @deprecated
 * @param req {e.Request}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export async function setSpatial(req, res, next) {

  if (!req.body.matrix || !req.body.offset || !req.body.ck) {
    next(new BadRequest('Missing values: ck|matrix|offset'));
    return;
  }

  // get coordinates of scene
  // language=Cypher
  const q0 = `
    MATCH (place:E53:UH4D {id: $scene})-[:P168]->(geo:E94)
    RETURN geo.latitude AS latitude,
           geo.longitude AS longitude,
           geo.altitude AS altitude`;

  const results = await neo4j.readTransaction(q0, {scene: req.params.scene});

  if (!results[0]) {
    next(new NotFound(`Scene with ID ${req.params.scene} not found!`));
    return;
  }

  // convert to utm
  const {easting, northing, zoneNum} = utm.fromLatLon(results[0].latitude, results[0].longitude);
  const utmOffset = new Vector3(easting, results[0].altitude, northing);
  const matrix = new Matrix4().fromArray(req.body.matrix);
  const translation = new Vector3().setFromMatrixPosition(matrix);
  const rotation = new Euler().setFromRotationMatrix(matrix, 'YXZ');
  const utmPosition = translation.multiply(new Vector3(1, 1, -1)).add(utmOffset);
  const {latitude, longitude} = utm.toLatLon(utmPosition.x, utmPosition.z, zoneNum, undefined, true);

  // write values
  // language=Cypher
  const q1 = `
    MATCH (:E53:UH4D {id: $scene})<-[:P89]-(place:E53)<-[:P7]-(:E65)-[:P94]->(image:E38:UH4D {id: $id})
    OPTIONAL MATCH (image)-[rSnot:P2]->(:E55 {id: "image:spatial:not_possible"})
    OPTIONAL MATCH (image)-[rSyet:P2]->(:E55 {id: "image:spatial:not_yet_possible"})
    MERGE (image)-[r:has_spatial]->(spatial:Spatial:UH4D)
    SET spatial = $spatial, r = $meta
    MERGE (place)-[:P168]->(geo:E94:UH4D)
    SET geo = $geo
    DELETE rSnot, rSyet
    RETURN apoc.map.merge(geo, spatial) AS spatial, 1 AS spatialStatus`;

  const params = {
    scene: req.params.scene,
    id: req.params.id,
    spatial: {
      id: 'spatial_' + req.params.id,
      matrix: req.body.matrix,
      offset: req.body.offset,
      ck: req.body.ck
    },
    meta: req.body.meta,
    geo: {
      id: 'e94_' + req.params.id,
      latitude,
      longitude,
      altitude: utmPosition.y,
      omega: rotation.x,
      phi: rotation.y,
      kappa: rotation.z
    }
  };

  try {

    const results = await neo4j.writeTransaction(q1, params);

    if (results[0]) {
      res.json(results[0]);
    } else {
      next(new NotFound(`Image with ID ${req.params.id} not found!`));
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}
