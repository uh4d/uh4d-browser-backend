import * as neo4j from 'neo4j-request';
import errPkg from 'http-errors';
import { promisify } from 'util';
import * as path from 'path';
import * as stream from 'stream';
import fs from 'fs-extra';
import got from 'got';
import probe from 'probe-image-size';
import * as shortId from 'shortid';
import { replace } from '../../modules/utils.js';
import { processImage } from '../../modules/process-image.js';
import * as config from '../../config.js';

const { InternalServerError, NotFound } = errPkg;

const pipeline = promisify(stream.pipeline);

/**
 * Get file metadata and identifiers of image.
 * @param params {Object}
 * @return {Promise<Object>}
 */
async function getFileMetaData(params) {

  // language=Cypher
  const q = `
    MATCH (image:E38:UH4D {id: $id}),
          (image)-[:P106]->(file:D9),
          (image)-[:P48]->(identifier:E42)
    RETURN file, identifier`;

  return (await neo4j.readTransaction(q, params))[0];

}

/**
 * Parse image url from DFD site.
 * @param permalink {string}
 * @return {Promise<string|null>}
 */
async function retrieveImageUrl(permalink) {

  const response = await got.get(permalink);

  const pattern = /<a class="download"[^<>]*href="([^"]*)"/;
  const matches = pattern.exec(response.body);

  return matches ? matches[1] : null;

}

/**
 * Download file to path.
 * @param url {string}
 * @param path {string}
 * @return {Promise<void>}
 */
async function downloadImage(url, path) {

  await pipeline(
    got.stream(url),
    fs.createWriteStream(path)
  );

}

/**
 * @param req {e.Request<{id: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export async function check(req, res, next) {

  try {

    // get image metadata
    const fileMeta = await getFileMetaData({ id: req.params.id });

    if (!fileMeta) {
      next(new NotFound(`Image with ID ${req.params.id} not found!`));
      return;
    }

    const file = fileMeta.file;
    const permalink = fileMeta.identifier.permalink;

    if (!permalink) {
      res.json({
        updateAvailable: false,
        reason: `No permalink available!`
      });
      return;
    }

    // parse image url
    const imageUrl = await retrieveImageUrl(permalink);

    if (!imageUrl) {
      res.json({
        updateAvailable: false,
        reason: `Couldn't retrieve image url from ${permalink}!`
      });
      return;
    }

    // get image size of image online
    /** @type {probe.ProbeResult} */
    const imageSize = await probe(imageUrl);

    res.json(Object.assign(imageSize, {
      updateAvailable: imageSize.width > file.width || imageSize.height > file.height
    }));

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * @param req {e.Request<{id: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export async function update(req, res, next) {

  const tmpDir = path.join(config.path.data, 'tmp', shortId.generate());

  try {

    // get image metadata
    const fileMeta = await getFileMetaData({ id: req.params.id });

    if (!fileMeta) {
      next(new NotFound(`Image with ID ${req.params.id} not found!`));
      return;
    }

    const oldFile = fileMeta.file;
    const permalink = fileMeta.identifier.permalink;

    // parse image url
    const imageUrl = await retrieveImageUrl(permalink);

    if (!imageUrl) {
      next(new NotFound(`Couldn't retrieve image url from ${permalink}`));
      return;
    }

    // create temporary directory
    await fs.ensureDir(tmpDir);

    const filename = replace(imageUrl.split('/').pop());
    const tmpFile = path.join(tmpDir, filename);

    // download image to tmp file
    await downloadImage(imageUrl, tmpFile);

    // process image
    const newFile = await processImage(tmpFile, tmpDir);

    // copy into image folder
    await fs.copy(path.join(tmpDir, newFile.path), path.join(config.path.data, newFile.path));

    // update database entry
    // language=Cypher
    const q = `
      MATCH (image:E38:UH4D {id: $id})-[:P106]->(file:D9)
      SET file += $file
      RETURN file`;

    const params = {
      id: req.params.id,
      file: newFile
    };

    try {

      const results = await neo4j.writeTransaction(q, params);
      res.json(results[0].file);

    } catch (e) {

      // remove new folder
      await fs.remove(path.join(config.path.data, newFile.path));

      next(new InternalServerError(e));
      return;

    }

    // remove old folder
    await fs.remove(oldFile.path);

  } catch (e) {

    next(new InternalServerError(e));

  } finally {

    // remove temporary folder
    await fs.remove(tmpDir);

  }

}
