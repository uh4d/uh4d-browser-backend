import { Router } from 'express';
import { logFileUpload, mUpload } from '../middlewares.js';

const router = Router({ mergeParams: true });

import * as images from './images.js';
router.get('/images', images.query);
router.post('/images', images.save);

import * as pkl from './pkl-file.js';
router.post('/images/:id/pkl', mUpload.single('file'), logFileUpload, pkl.savePkl);

export default router;
