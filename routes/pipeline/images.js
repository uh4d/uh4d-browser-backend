import * as neo4j from 'neo4j-request';
import errPkg from 'http-errors';
import { Euler, Quaternion, Vector2, Vector3 } from 'three';
import { escapeRegex } from '../../modules/utils.js';
import Coordinates from '../../modules/coordinates.js';
import * as config from '../../config.js';
import { getSceneData } from '../scenes/scene.js';

const { BadRequest, InternalServerError, NotFound } = errPkg;

/**
 * Pipeline image query object.
 * @typedef {Object} PipelineImageQuery
 * @property {string|string[]} q
 * @property {string} scene
 * @property {string} from
 * @property {string} to
 * @property {string} undated
 * @property {string} lat
 * @property {string} lon
 * @property {string} r
 * @property {string} forceSceneOrigin
 */

/**
 * Query images with Cartesian coordinates relative to origin.
 * The origin is the average position of all queried images with spatial coordinates.
 * @param req {e.Request<{}, any, {}, PipelineImageQuery>}
 * @param res {e.Response}
 * @param next {e.NextFunction}
 * @return {Promise<void>}
 */
export async function query(req, res, next) {

  const qFilter = [];
  const objList = [];
  const regexPosList = [];
  const regexNegList = [];

  // first triage
  (req.query.q ? Array.isArray(req.query.q) ? req.query.q : [req.query.q] : []).forEach(value => {
    if (/^obj:/.test(value)) {
      objList.push(value.replace(/^obj:/, ''));
    } else {
      qFilter.push(value);
    }
  });

  const spatialParams = {
    latitude: parseFloat(req.query.lat),
    longitude: parseFloat(req.query.lon),
    radius: parseFloat(req.query.r)
  };
  const doSpatialQuery = !isNaN(spatialParams.latitude) && !isNaN(spatialParams.longitude) && !isNaN(spatialParams.radius);

  let q = '';

  if (doSpatialQuery) {
    q += 'WITH point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint ';
  }

  if (req.query.scene) {
    q += `MATCH (:E53:UH4D {id: $scene})<-[:P89]-`;
  } else {
    q += 'MATCH '
  }

  q += `(place:E53)<-[:P7]-(e65:E65)-[:P94]->(image:E38)`;

  if (objList.length) {
    q += `
      MATCH (image)-[:P138]->(e22:E22)
      WHERE e22.id IN $objList
    `;
  }

  if (doSpatialQuery) {
    q += `
      MATCH (place)-[:P168]->(geo:E94)
      WHERE distance(userPoint, point({latitude: geo.latitude, longitude: geo.longitude})) < $geo.radius
    `;
  }

  if (req.query.from && req.query.to && req.query.from !== 'null' && req.query.to !== 'null') {
    q += `
      OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
      WITH image, place, e65, date
      WHERE date ${req.query.undated === 'true' ? 'IS NULL OR': 'IS NOT NULL AND'} date.to > date($from) AND date.from < date($to)
    `;
  }

  q += `
    WITH image, place, e65
    MATCH (image)-[:P106]->(file:D9),
          (image)-[:P102]->(title:E35)
    OPTIONAL MATCH (e65)-[:P14]->(authorId:E21)-[:P131]->(author:E82)
    OPTIONAL MATCH (image)-[:P105]->(ownerId:E40)-[:P131]->(owner:E82)
    OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
    // OPTIONAL MATCH (image)-[:has_spatial]->(spatial:Spatial)
    OPTIONAL MATCH (e65)-[:P16]->(spatial:E22)
    OPTIONAL MATCH (place)-[:P168]->(geo:E94)
    OPTIONAL MATCH (image)-[:P2]->(tSnot:E55 {id: "image:spatial:not_possible"})
    OPTIONAL MATCH (image)-[:P2]->(tSyet:E55 {id: "image:spatial:not_yet_possible"})
    OPTIONAL MATCH (image)-[:has_tag]->(tag:TAG)
    WITH image, file, title, authorId, author, date, ownerId, owner, spatial, geo, tSnot, tSyet, collect(tag.id) AS tags
  `;

  qFilter.forEach((value, index) => {

    const qPrefix = index === 0 ? ' WHERE' : ' AND';

    if (value === 'spatial:set') {

      q += qPrefix + ' spatial IS NOT NULL';

    } else if (value === 'spatial:unset') {

      q += qPrefix + ' spatial IS NULL';

    } else if (/^-/.test(value)) {

      if (regexNegList.length < 1) {
        q += qPrefix + ` none(e IN $regexNegList
          WHERE title.value =~ e
          OR author.value =~ e
          OR owner.value =~ e
          OR any(tag IN tags WHERE tag =~ e)
        )`;
      }
      regexNegList.push(`(?i).*${escapeRegex(value.replace(/^-/, '').replace(/"/g, ''))}.*`);

    } else {

      if (regexPosList.length < 1) {
        q += qPrefix + ` all(e IN $regexPosList
          WHERE title.value =~ e
          OR author.value =~ e
          OR owner.value =~ e
          OR any(tag IN tags WHERE tag =~ e)
        )`;
      }
      regexPosList.push(`(?i).*${escapeRegex(value.replace(/"/g, ''))}.*`);

    }

  });

  q += ` RETURN
    image.id AS id,
    apoc.map.removeKey(file, 'id') AS file,
    apoc.map.submap(date, ['from', 'to'], null, false) AS date,
    CASE
      WHEN spatial IS NULL THEN NULL
      ELSE apoc.map.removeKey(apoc.map.merge(geo, spatial), 'id')
    END AS camera,
    CASE
      WHEN spatial IS NOT NULL THEN CASE WHEN spatial.method = 'pipeline' THEN 4 ELSE 1 END
      WHEN tSnot IS NOT NULL THEN 2
      WHEN tSyet IS NOT NULL THEN 3
      ELSE 0
    END AS spatialStatus
  `;

  const params = {
    scene: req.query.scene,
    from: req.query.from,
    to: req.query.to,
    regexPosList,
    regexNegList,
    objList,
    geo: spatialParams
  };

  try {

    const results = await neo4j.readTransaction(q, params);

    let origin;
    if (req.query.scene && req.query.forceSceneOrigin === '1') {
      // set origin by scene
      const scene = await getSceneData(req.query.scene);
      origin = new Coordinates(scene.latitude, scene.longitude, scene.altitude);
    } else {
      // set origin by average position of all available positions
      origin = results
        .filter(img => img.camera)
        .map(img => ({ latitude: img.camera.latitude, longitude: img.camera.longitude }))
        .reduce((acc, curr, index, array) => {
          acc.latitude += curr.latitude / array.length;
          acc.longitude += curr.longitude / array.length;
          return acc;
        }, new Coordinates());
    }

    // map image properties
    const entries = results.map(img => {

      const map = {
        id: img.id,
        image: config.host + '/data/' + img.file.path + img.file.original,
        pkl: img.file.pkl ? config.host + '/data/' + img.file.path + img.file.pkl : null,
        date: img.date,
        width: img.file.width,
        height: img.file.height,
        pose: null,
        spatialStatus: img.spatialStatus
      };

      if (img.camera) {

        // set translation relative to origin of local coordinate system
        const t = new Coordinates(img.camera.latitude, img.camera.longitude, img.camera.altitude).toCartesian(origin);
        const euler = new Euler(img.camera.omega, img.camera.phi, img.camera.kappa, 'YXZ');
        const q = new Quaternion().setFromEuler(euler);

        const pp = new Vector2().fromArray(img.camera.offset)
          .multiplyScalar(map.height)
          .add(new Vector2(map.width / 2, map.height / 2));
        const k = Array.isArray(img.camera.k) ? img.camera.k : [0];

        map.pose = {
          tx: t.x,
          ty: t.y,
          tz: t.z,
          qw: q.w,
          qx: q.x,
          qy: q.y,
          qz: q.z,
          params: [img.camera.ck * map.height, ...pp.toArray(), ...k]
        };

      }

      return map;

    });

    // return data object
    res.json({
      origin: origin.toJSON(),
      images: entries
    });

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * @typedef {Object} IPose
 * @property {number} tx
 * @property {number} ty
 * @property {number} tz
 * @property {number} qw
 * @property {number} qx
 * @property {number} qy
 * @property {number} qz
 * @property {number[]} params
 */

/**
 * Pipeline image body object.
 * @typedef {Object} PipelineImageBody
 * @property {Object} origin
 * @property {number} origin.latitude
 * @property {number} origin.longitude
 * @property {number} origin.altitude
 * @property {{id: string, pose: IPose, width: number, height: number}[]} images
 */

/**
 * Save new poses of images.
 * @param req {e.Request<{}, any, PipelineImageBody>}
 * @param res {e.Response}
 * @param next {e.NextFunction}
 * @return {Promise<void>}
 */
export async function save(req, res, next) {

  // check request body
  const rOrigin = req.body.origin;
  if (!rOrigin ||
    typeof rOrigin.latitude !== 'number' ||
    typeof rOrigin.longitude !== 'number' ||
    typeof rOrigin.altitude !== 'number') {
    return next(new BadRequest('Request body: origin in wrong format!'));
  }

  if (!req.body.images || !Array.isArray(req.body.images)) {
    return next(new BadRequest('Request body: images must be an array of objects!'));
  }

  const statements = [];
  const origin = new Coordinates(rOrigin.latitude, rOrigin.longitude, rOrigin.altitude);

  for (const img of req.body.images) {

    if (!img.id) {
      return next(new BadRequest('No image ID provided!'));
    }

    try {

      // convert back to WGS-84 coordinates
      const position = origin.clone().add(new Vector3(img.pose.tx, img.pose.ty, img.pose.tz)).toJSON();
      const quaternion = new Quaternion(img.pose.qx, img.pose.qy, img.pose.qz, img.pose.qw);
      const euler = new Euler().setFromQuaternion(quaternion, 'YXZ');

      // convert intrinsic parameters
      const ck = img.pose.params[0] / img.height;
      const offset = new Vector2(img.width / 2, img.height / 2)
        .sub(new Vector2(img.pose.params[1], img.pose.params[2]))
        .divideScalar(img.height)
        .toArray();
      const k = img.pose.params.slice(3);

      const params = {
        id: img.id,
        geo: Object.assign({
          id: 'e94_' + img.id
        }, position),
        camera: {
          id: 'camera_' + img.id,
          ck, offset, k,
          omega: euler.x,
          phi: euler.y,
          kappa: euler.z,
          method: 'pipeline'
        }
      };

      // language=Cypher
      const q = `
        MATCH (img:E38:UH4D {id: $id})<-[:P94]-(e65:E65)-[:P7]->(place:E53)
        OPTIONAL MATCH (image)-[rSnot:P2]->(:E55 {id: "image:spatial:not_possible"})
        OPTIONAL MATCH (image)-[rSyet:P2]->(:E55 {id: "image:spatial:not_yet_possible"})
        MERGE (e65)-[:P16]->(camera:E22:UH4D)
        MERGE (place)-[:P168]->(geo:E94:UH4D)
        SET camera = $camera,
            geo = $geo
        DELETE rSnot, rSyet
        RETURN img
      `;

      statements.push({ statement: q, parameters: params });

      } catch (e) {

        return next(new BadRequest(`Some values are missing or have wrong format of image with ID ${img.id}`));

      }

  }

  try {

    // update database
    const txc = neo4j.session().beginTransaction();

    // check for each statement if it has been successful, otherwise rollback transaction
    for (const s of statements) {

      const result = await txc.run(s.statement, s.parameters);

      if (!result.records[0]) {
        await txc.rollback();
        return next(new NotFound(`Image not found with ID ${s.parameters.id}. No values updated (transaction rollback)!`));
      }

    }

    await txc.commit();

    res.send();

  } catch (e) {

    next(new InternalServerError(e));

  }

}
