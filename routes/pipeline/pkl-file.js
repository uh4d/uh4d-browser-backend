import errPkg from 'http-errors';
import * as neo4j from 'neo4j-request';
import * as path from 'path';
import fs from 'fs-extra';
import logger from '../../modules/logger.js';
import * as config from '../../config.js';

const { InternalServerError, NotFound } = errPkg;

/**
 * Upload `.pkl` file for associated image and update database entry.
 * @param req {e.Request<{id: string}, any, {}, {regexp?: string}>}
 * @param res {e.Response}
 * @param next {e.NextFunction}
 * @return {Promise<void>}
 */
export async function savePkl(req, res, next) {

  // query image by ID

  let q0 = '';

  if (req.query.regexp === '1') {
    q0 += 'MATCH (image:E38:UH4D) WHERE image.id =~ ".*" + $id + ".*"';
  } else {
    q0 += 'MATCH (image:E38:UH4D {id: $id})';
  }

  q0 += `
   MATCH (image)-[:P106]->(file:D9)
   RETURN image.id AS id, file`;

  let image = null;

  try {

    const results = await neo4j.readTransaction(q0, {id: req.params.id});

    if (results[0]) {
      image = results[0];
    } else {
      return next(new NotFound(`Image with ID ${req.params.id} not found!`));
    }

  } catch (e) {

    return next(new InternalServerError(e));

  }

  // copy uploaded pkl file
  const matches = /^(.+)\.[^.]+$/.exec(image.file.original);
  const fileName = matches[1] + '.pkl';
  const filePath = path.join(config.path.data, image.file.path, fileName);

  try {

    logger.info('Move:', req.file.path, filePath);
    await fs.rename(req.file.path, filePath);

  } catch (e) {

    return next(new InternalServerError(e));

  }

  // update database entry

  // language=Cypher
  const q1 = `
    MATCH (image:E38:UH4D {id: $id})-[:P106]->(file:D9)
    SET file.pkl = $pkl
    RETURN image.id AS id, apoc.map.removeKey(file, 'id') AS file
  `;

  const params = {
    id: image.id,
    pkl: fileName
  };

  try {

    const results = await neo4j.writeTransaction(q1, params);

    res.json(results[0]);

  } catch (e) {

    next(new InternalServerError(e));

    // delete file if database write failed
    await fs.remove(filePath);

  }

}
