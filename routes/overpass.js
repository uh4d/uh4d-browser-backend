import * as path from 'path';
import { fileURLToPath } from 'url';
import fs from 'fs-extra';
import errPkg from 'http-errors';
import { Subject } from 'rxjs';
import { rateLimit as rateLimitOperator } from '../modules/utils.js';
import logger from '../modules/logger.js';
import { queryOverpass } from '../modules/overpass.js';
import { checkRequiredProperties } from '../modules/utils.js';

const { BadRequest, InternalServerError } = errPkg;

const dirname = path.dirname(fileURLToPath(import.meta.url));

/**
 * Get cached Overpass data. Otherwise, pass request forward to overpass query (but rate-limited).
 * @param limit {number} Number of requests
 * @param window {number} Rolling window duration (in milliseconds)
 * @returns {(function(e.Request<null, any, any, {lat?: string, lon?: string, r?: string}>, e.Response, function))}
 */
export function getCache(limit, window) {

  const subject = new Subject();

  subject
    .pipe(rateLimitOperator(limit, window))
    .subscribe(next => {
      next();
    });

  return async (req, res, next) => {

    // check query parameters
    const requiredProps = ['lat', 'lon', 'r'];
    if (!checkRequiredProperties(req.query, requiredProps)) {
      return next(new BadRequest('Missing essiential query parameters: ' + requiredProps.join('|')));
    }

    // round position
    const lat = (Math.round(parseFloat(req.query.lat) * 200) / 200).toFixed(3);
    const lon = (Math.round(parseFloat(req.query.lon) * 200) / 200).toFixed(3);

    try {

      // check for cached data file
      const file = path.join(dirname, '..', '.cache/overpass', `${lat}-${lon}-${req.query.r}.json`);
      const exists = await fs.pathExists(file);

      if (exists) {

        // read file and return
        const data = await fs.readJson(file);

        res.json(data);

      } else {

        // no file, pass request forward
        subject.next(next);

      }

    } catch (e) {

      next(new InternalServerError(e));

    }

  };

}

/**
 * Query OSM Overpass API by lat, lon, and radius.
 * @param req {e.Request<null, any, any, {lat?: string, lon?: string, r?: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export async function query(req, res, next) {

  // check query parameters
  const requiredProps = ['lat', 'lon', 'r'];
  if (!checkRequiredProperties(req.query, requiredProps)) {
    return next(new BadRequest('Missing essiential query parameters: ' + requiredProps.join('|')));
  }

  // round position
  const lat = (Math.round(parseFloat(req.query.lat) * 200) / 200).toFixed(3);
  const lon = (Math.round(parseFloat(req.query.lon) * 200) / 200).toFixed(3);

  try {

    // query Overpass API
    const data = await queryOverpass(parseFloat(lat), parseFloat(lon), parseFloat(req.query.r));

    res.json(data);

    // cache data
    await fs.ensureDir(path.join(dirname, '..', '.cache/overpass'));
    const file = path.join(dirname, '..', '.cache/overpass', `${lat}-${lon}-${req.query.r}.json`);

    await fs.writeJson(file, data, { spaces: 2 });

  } catch (e) {

    if (!res.headersSent) {
      next(new InternalServerError(e));
    } else {
      logger.error(e);
    }

  }

}
