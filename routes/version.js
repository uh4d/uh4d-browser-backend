import * as path from 'path';
import { execFile } from 'child-process-promise';
import * as neo4j from 'neo4j-request';
import fs from 'fs-extra';
import * as config from '../config.js';
// import * as pkg from '../package.json';

/**
 * Print version and check available executables.
 * @param req {e.Request}
 * @param res {e.Response}
 * @return {Promise<void>}
 */
export default async function version(req, res) {

  const pkg = await fs.readJson(new URL('../package.json', import.meta.url));

  // neo4j
  const neo4jServerInfo = await neo4j.getDriver().verifyConnectivity();

  const response = {
    API: 'UH4D ' + pkg.version,
    Neo4j: neo4jServerInfo.version
  };

  // Assimp
  try {

    const result = await execFile(config.exec.Assimp, ['version']);
    const matches = result.stdout.match(/Version\s([^\s]+)\s/);
    response.Assimp = matches[1];

  } catch (e) {

    console.error(e);
    response.Assimp = 'FAILED!';

  }

  // Collada2Gltf
  try {

    // Collada2Gltf has no explicit command to get the version
    // check if executed without command options will output help message
    const version = await execFile(config.exec.Collada2Gltf, [])
      .then(result => result)
      .catch(reason => reason)
      .then(result => {
        const matches = result.stdout.match(/^(COLLADA2GLTF)/);
        if (matches && matches[1] === 'COLLADA2GLTF') {
          return matches[1];
        } else {
          return Promise.reject(result);
        }
      });
    response.Collada2Gltf = `${version} (unknown version)`;

  } catch (e) {

    console.error(e);
    response.Collada2Gltf = 'FAILED!';

  }

  // ImageMagick identify
  try {

    const result = await execFile(config.exec.ImageIdentify, ['-version']);
    const matches = result.stdout.match(/^Version:\s(.+)\shttp/);
    response.ImageIdentify = `${path.basename(config.exec.ImageIdentify)} ${matches[1]}`;

  } catch (e) {

    console.error(e);
    response.ImageIdentify = 'FAILED!';

  }

  // ImageMagick convert/magick
  try {

    const result = await execFile(config.exec.ImageMagick, ['-version']);
    const matches = result.stdout.match(/^Version:\s(.+)\shttp/);
    response.ImageMagick = `${path.basename(config.exec.ImageMagick)} ${matches[1]}`;

  } catch (e) {

    console.error(e);
    response.ImageMagick = 'FAILED!';

  }

  res.json(response);

}
