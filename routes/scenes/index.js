import { Router } from 'express';

const router = Router({ mergeParams: true });

import * as scene from './scene.js';
router.get('/:scene', scene.get);
router.post('/', scene.save);

export default router;
