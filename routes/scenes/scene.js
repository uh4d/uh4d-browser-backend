import * as neo4j from 'neo4j-request';
import errPkg from 'http-errors';
import { checkRequiredProperties, replace } from '../../modules/utils.js';

const { BadRequest, InternalServerError, NotFound } = errPkg;

/**
 * Get scene data.
 * @param sceneId {string}
 * @return {Promise<{id: string, name: string, latitude: number, longitude: number, altitude: number}>}
 */
export async function getSceneData(sceneId) {

  // language=Cypher
  const q = `
    MATCH (place:E53:UH4D {id: $scene})-[:P1]->(name:E41),
          (place)-[:P168]->(geo:E94)
    RETURN place.id AS id,
           name.value as name,
           geo.latitude AS latitude,
           geo.longitude AS longitude,
           geo.altitude AS altitude
  `;

  const params = {
    scene: sceneId
  };

  return (await neo4j.readTransaction(q, params))[0];

}

/**
 * Get scene.
 * @param req {e.Request<{scene: string}>}
 * @param res {e.Response}
 * @param next {e.NextFunction}
 * @return {Promise<void>}
 */
export async function get(req, res, next) {

  try {

    const result = await getSceneData(req.params.scene);

    if (result) {
      res.json(result);
    } else {
      next(new NotFound(`Scene with ID ${req.params.scene} not found!`));
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * Create new scene root nodes.
 * @param req {e.Request<any, any, {name: string, latitude: string, longitude: string, altitude: string}>}
 * @param res {e.Response}
 * @param next {e.NextFunction}
 * @return {Promise<void>}
 */
export async function save(req, res, next) {

  const requiredProps = ['name', 'latitude', 'longitude', 'altitude'];
  if (!checkRequiredProperties(req.body, requiredProps)) {
    return next(new BadRequest('Missing values:  ' + requiredProps.join('|')));
  }

  // check if numbers
  if (typeof req.body.latitude !== 'number' ||
    typeof req.body.longitude !== 'number' ||
    typeof req.body.altitude !== 'number') {
    return next(new BadRequest('latitude|longitude|altitude must be numbers!'));
  }

  const sceneId = replace(req.body.name).toLowerCase();

  // check if scene already exists
  const exists = await getSceneData(sceneId);
  if (exists) {
    return next(new BadRequest(`Scene with ID ${sceneId} already exists!`));
  }

  // language=Cypher
  const q = `
    CREATE (place:E53:UH4D {id: $scene})-[:P1]->(name:E41:UH4D $name),
          (place)-[:P168]->(geo:E94:UH4D $geo)
    RETURN place.id AS id,
           name.value as name,
           geo.latitude AS latitude,
           geo.longitude AS longitude,
           geo.altitude AS altitude
  `;

  const params = {
    scene: sceneId,
    name: {
      id: 'e41_' + sceneId,
      value: req.body.name
    },
    geo: {
      id: 'e94_' + sceneId,
      latitude: req.body.latitude,
      longitude: req.body.longitude,
      altitude: req.body.altitude
    }
  };

  try {

    const results = await neo4j.writeTransaction(q, params);
    res.json(results[0]);

  } catch (e) {

    next(new InternalServerError(e));

  }

}
