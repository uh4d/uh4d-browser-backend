import { Router } from 'express';
import errPkg from 'http-errors';
import { fileURLToPath } from 'url';
import * as path from 'path';
import fs from 'fs-extra';
import moment from 'moment';
import * as config from '../../config.js';

const { InternalServerError, NotFound } = errPkg;

const router = Router({ mergeParams: true });

router.get('/', query);
router.get('/:file', get);
router.delete('/:file', remove);

export default router;

/**
 * @param req {e.Request<{}, any, any, {format?: string, from?: string, to?: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
async function query(req, res, next) {

  try {

    if (req.query.format === 'html') {

      // send log-viewer html page
      const html = await fs.readFile(path.join(path.dirname(fileURLToPath(import.meta.url)), 'log-viewer.html'), { encoding: 'utf-8' });
      res.setHeader('Content-Type', 'text/html');
      res.send(html);

    } else {

      // query files and return list
      let files = await fs.readdir(config.path.logs);

      // filter list
      if (req.query.from && req.query.to) {
        const from = moment(req.query.from);
        const to = moment(req.query.to);
        files = files.filter(file => moment(file.slice(0, 15), 'YYYYMMDD-HHmmss').isBetween(from, to));
      }

      res.json(files);

    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * Read log file and send content.
 * @param req {e.Request<{file: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
async function get(req, res, next) {

  const filePath = path.join(config.path.logs, req.params.file);

  // check if file exists
  const exists = await fs.pathExists(filePath);
  if (!exists) {
    return next(new NotFound());
  }

  try {

    const data = await fs.readFile(filePath, { encoding: 'utf-8' });

    res.send(data);

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * Delete log file.
 * @param req {e.Request<{file: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
async function remove(req, res, next) {

  const filePath = path.join(config.path.logs, req.params.file);

  // check if file exists
  const exists = await fs.pathExists(filePath);
  if (!exists) {
    return next(new NotFound());
  }

  try {

    await fs.remove(filePath);

    res.send();

  } catch (e) {

    next(new InternalServerError(e));

  }

}
