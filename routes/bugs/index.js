import { Router } from 'express';
import errPkg from 'http-errors';
import got from 'got';
import * as config from '../../config.js';
import { checkRequiredProperties } from '../../modules/utils.js';

const { BadRequest, InternalServerError } = errPkg;

const router = Router({ mergeParams: true });

router.post('/', postBug);

export default router;


/**
 * Open new issue in GitLab repo utilizing GitLab API.
 * @param req {e.Request<{}, any, {context: string, scene?: string, title: string, description: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
async function postBug(req, res, next) {

  const requiredProps = ['context', 'title', 'description'];
  if (!checkRequiredProperties(req.body, requiredProps)) {
    return next(new BadRequest('Missing values: ' + requiredProps.join('|')));
  }

  try {

    const gitlab = config.gitlab[req.body.context] ? config.gitlab[req.body.context] : config.gitlab['uh4d-browser-app'];

    const response = await got.post(`${gitlab.apiEndpoint}/projects/${gitlab.projectId}/issues`, {
      json: {
        title: req.body.title,
        description: `_Scene:_ ${req.body.scene || 'any'}\n\n${req.body.description}`
      },
      headers: {
        'PRIVATE-TOKEN': gitlab.privateToken
      },
      responseType: 'json'
    });

    res.json(response.body);

  } catch (err) {

    next(new InternalServerError(err));

  }

}
