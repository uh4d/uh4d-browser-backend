import { Router } from 'express';
import * as neo4j from "neo4j-request";
import errPkg from 'http-errors';
import { escapeRegex } from '../../modules/utils.js';

const { InternalServerError } = errPkg;

const router = Router({ mergeParams: true });

router.get('/', query);

export default router;

async function query(req, res, next) {

  // language=Cypher
  const q = `
    MATCH (addr:E45:UH4D)
    WHERE addr.value =~ $search
    RETURN addr.id AS id, addr.value AS value`;

  const params = {
    search: `(?i).*${ escapeRegex(req.query.search) }.*`
  };

  try {

    const results = await neo4j.readTransaction(q, params);
    res.json(results);

  } catch (e) {

    next(new InternalServerError(e));

  }

}
