import { Router } from 'express';
import { rateLimit } from './middlewares.js';

import imagesRoutes from './images/index.js';
import objectsRoutes from './objects/index.js';
import poisRoutes from './pois/index.js';
import toursRoutes from './tours/index.js';
import terrainRoutes from './terrain/index.js';
import mapsRoutes from './maps/index.js';
import sceneRoutes from './scenes/index.js';
import personsRoutes from './actors/persons.js';
import legalBodiesRoutes from './actors/legal-bodies.js';
import addressesRoutes from './addresses/index.js';
import tagsRoutes from './tags/index.js';

// root router
const router = Router();

import version from './version.js';
router.get('/version', version);

router.use('/images', imagesRoutes);
router.use('/objects', objectsRoutes);
router.use('/pois', poisRoutes);
router.use('/tours', toursRoutes);

router.use('/terrain', terrainRoutes);
router.use('/maps', mapsRoutes);
router.use('/scenes', sceneRoutes);

router.use('/persons', personsRoutes);
router.use('/legalbodies', legalBodiesRoutes);
router.use('/addresses', addressesRoutes);
router.use('/tags', tagsRoutes);

import bugsRoutes from './bugs/index.js';
router.use('/bugs', bugsRoutes);

import logsRoutes from './logs/index.js';
router.use('/logs', logsRoutes);

// proxy url requests
import { proxyRequest } from './proxy.js';
router.get('/proxy', proxyRequest);

// proxy requests to Elevation API
import * as elevation from './elevation.js';
router.get('/elevation/gltf/:folder/:file', elevation.getTerrainGltf);
router.get('/elevation', rateLimit(1, 6000), elevation.getTerrainMeta);

// Overpass API
import * as overpass from './overpass.js';
router.get('/overpass', overpass.getCache(1, 1000), overpass.query);

import pipelineRoutes from './pipeline/index.js';
router.use('/pipeline', pipelineRoutes);

import * as scene from './scenes/scene.js';
router.get('/:scene', scene.get);

export default router;
