import errPkg from 'http-errors';
import * as neo4j from 'neo4j-request';
import { escapeRegex } from '../../modules/utils.js';

const { InternalServerError } = errPkg;

export default async function query(req, res, next) {

  // language=Cypher
  const q = `
    MATCH (tag:TAG:UH4D)
    WHERE tag.id =~ $search
    RETURN tag.id AS value`;

  const params = {
    search: `(?i).*${ escapeRegex(req.query.search) }.*`
  };

  try {

    const results = await neo4j.readTransaction(q, params);
    res.json(results);

  } catch (e) {

    next(new InternalServerError(e))

  }

}
