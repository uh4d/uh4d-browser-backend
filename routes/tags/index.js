import { Router } from 'express';

const router = Router({ mergeParams: true });

import query from './query.js';
router.get('/', query);

export default router;
