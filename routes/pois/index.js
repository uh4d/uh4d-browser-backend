import { Router } from 'express';

const router = Router({ mergeParams: true });

import query from './query/index.js';
router.get('/', query);

import * as poi from './pois.js';
router.post('/', poi.save);
router.get('/:id', poi.get);
router.patch('/:id', poi.update);
router.delete('/:id', poi.remove);

export default router;

/**
 * Point of interest interface
 * @typedef IPointOfInterest
 * @property {string} id
 * @property {string} title
 * @property {string} content
 * @property {object} location
 * @property {string} location.id
 * @property {number} location.latitude
 * @property {number} location.longitude
 * @property {number} location.altitude
 * @property {number[]} location.position
 */
