import * as path from 'path';
import fs from 'fs-extra';
import { fileURLToPath } from 'url';

// load available modules in this directory
const modules = {};
loadModules();

async function loadModules() {

  //
  const dirname = path.dirname(fileURLToPath(import.meta.url));
  const files = fs.readdirSync(dirname);

  for (const file of files) {

    if (file === 'index.js') continue;

    const basename = path.basename(file, '.js');

    modules[basename] = await import('./' + file);

  }

}

/**
 * @param req {e.Request<{}, {}, {}, {type?: string[]}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export default async function query(req, res, next) {

  // parse types
  const types = req.query.type
    ? Array.isArray(req.query.type)
      ? req.query.type
      : [req.query.type]
    : ['uh4d'];

  try {

    // iterate over types and execute query at respective module
    const promises = types.map(async (type) => {
      if (modules[type]) {
        const pois = await modules[type].default(req);
        pois.forEach(poi => {
          poi.type = type;
        });
        return pois;
      } else {
        return Promise.resolve([]);
      }
    });

    // concatenate lists of POIs
    const results = await Promise.all(promises);
    const concatResults = results.reduce((previousValue, currentValue) => previousValue.concat(currentValue));

    res.json(concatResults);

  } catch (e) {

    next(e);

  }

}
