import errPkg from 'http-errors';
import got from 'got';

const { BadRequest } = errPkg;

/**
 * @param req {e.Request<{}, {}, {}, {lat: string, lon: string, r: string}>}
 * @return {Promise<IPointOfInterest[]>}
 */
export default async function querWikidata(req) {

  const latitude = parseFloat(req.query.lat);
  const longitude = parseFloat(req.query.lon);
  const radius = parseFloat(req.query.r);

  if (isNaN(latitude) || isNaN(longitude) || isNaN(radius)) {
    throw new BadRequest('Wikidata query requires lat|lon|r query parameters');
  }

  const wdBuilding = 'Q41176';

  // language=Sparql
  const sparql = `
    SELECT ?item ?itemLabel ?lat ?lon ?sitelink WHERE {
      ?item wdt:P31/wdt:P279* wd:${wdBuilding} .
      ?item p:P625 ?statement .
      ?statement psv:P625 ?coordinate_node .
      ?coordinate_node wikibase:geoLatitude ?lat .
      ?coordinate_node wikibase:geoLongitude ?lon .
      ?sitelink schema:about ?item .
      ?sitelink schema:inLanguage "de" .
      ?sitelink schema:isPartOf <https://de.wikipedia.org/> .
      SERVICE wikibase:around {
        ?item wdt:P625 ?location .
        bd:serviceParam wikibase:center "Point(${longitude} ${latitude})"^^geo:wktLiteral .
        bd:serviceParam wikibase:radius "${radius / 1000}".
      }
      FILTER EXISTS { ?place wdt:P31/wdt:P279* wd:${wdBuilding} }
      SERVICE wikibase:label {
        bd:serviceParam wikibase:language "de" .
      }
    }
    GROUP BY ?item ?itemLabel ?lat ?lon ?sitelink
  `;

  const response = await got('https://query.wikidata.org/sparql?query=' + encodeURIComponent(sparql), {
    headers: {
      Accept: 'application/sparql-results+json'
    }
  });

  const results = JSON.parse(response.body).results.bindings;

  // map to POI format
  return results.map(r => ({
    id: r.item.value,
    title: r.itemLabel.value,
    content: r.sitelink.value,
    location: {
      latitude: parseFloat(r.lat.value),
      longitude: parseFloat(r.lon.value),
      altitude: 0
    }
  }));

}
