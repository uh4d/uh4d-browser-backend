import moment from 'moment';
import * as neo4j from 'neo4j-request';
import errPkg from 'http-errors';

const { InternalServerError } = errPkg;

/**
 * @param req {e.Request<{}, {}, {}, {scene?: string, date?: string, lat?: string, lon?: string, r?: string}>}
 * @return {Promise<IPointOfInterest[]>}
 */
export default async function queryUh4d(req) {

  // check query parameter
  let date = req.query.date ? moment(req.query.date) : null;
  if (date && !date.isValid()) {
    date = null;
  }

  const spatialParams = {
    latitude: parseFloat(req.query.lat),
    longitude: parseFloat(req.query.lon),
    radius: parseFloat(req.query.r)
  };
  const doSpatialQuery = !isNaN(spatialParams.latitude) && !isNaN(spatialParams.longitude) && !isNaN(spatialParams.radius);

  const params = {
    scene: req.query.scene,
    geo: spatialParams
  };

  let q = '';

  if (doSpatialQuery) {
    q += `WITH point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint `;
  }

  q += 'MATCH ' + (req.query.scene ? '(:E53:UH4D {id: $scene})<-[:P89]-' : '');
  q += `(place:E53)<-[:P161]-(e92:E92)<-[:P67]-(poi:E73)-[:P2]->(:E55 {id: "poi"}),
        (place)-[:P168]->(geo:E94) `;

  if (doSpatialQuery) {
    q += `WHERE distance(userPoint, point({latitude: geo.latitude, longitude: geo.longitude})) < $geo.radius`;
  }

  q+= `
    OPTIONAL MATCH (e92)-[:P160]->(:E52)-[:P82]->(poiDate:E61)
    OPTIONAL MATCH (poi)-[:P67]->(e22:E22)
    OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(objFrom:E61)
    OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(objTo:E61)
    WITH poi, place, e22,
         CASE
           WHEN poiDate IS NOT NULL THEN { from: poiDate.from, to: poiDate.to, objectBound: false }
           ELSE { from: objFrom.value, to: objTo.value, objectBound: true }
           END AS date
  `;

  if (date) {
    q += 'WHERE (date.from IS NULL OR date.from < date($date)) AND (date.to IS NULL OR date.to > date($date))';

    params.date = date.format('YYYY-MM-DD');
  }

  // language=Cypher
  q += `
    MATCH (place)-[:P168]->(geo:E94),
          (poi)-[:P102]->(title:E35),
          (poi)-[:P3]->(content:E62)
    RETURN poi.id AS id,
           title.value AS title,
           content.value AS content,
           apoc.map.removeKey(geo, 'id') AS location,
           e22.id AS objectId,
           poi.camera AS camera,
           poi.timestamp As timestamp,
           poi.damageFactors AS damageFactors,
           date
  `;

  try {

    return await neo4j.readTransaction(q, params);

  } catch (e) {

    throw new InternalServerError(e);

  }

}
