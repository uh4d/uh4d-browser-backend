import errPkg from 'http-errors';
import * as neo4j from 'neo4j-request';
import * as shortId from 'shortid';
import moment from 'moment';
import { checkDateFormat, checkRequiredProperties } from '../../modules/utils.js';

const { BadRequest, InternalServerError, NotFound } = errPkg;

/**
 * @param req {e.Request<{id: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export async function get(req, res, next) {

  // language=Cypher
  let q = `
    MATCH (poi:E73 {id: $id})-[:P2]->(:E55 {id: "poi"}),
          (poi)-[:P102]->(title:E35),
          (poi)-[:P3]->(content:E62),
          (poi)-[:P67]->(e92:E92)-[:P161]->(place:E53)-[:P168]->(geo:E94)
    OPTIONAL MATCH (e92)-[:P160]->(:E52)-[:P82]->(poiDate:E61)
    OPTIONAL MATCH (poi)-[:P67]->(e22:E22)
    OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(objFrom:E61)
    OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(objTo:E61)
    RETURN poi.id AS id,
           title.value AS title,
           content.value AS content,
           apoc.map.removeKey(geo, 'id') AS location,
           e22.id AS objectId,
           poi.camera AS camera,
           poi.timestamp As timestamp,
           CASE
             WHEN poiDate IS NOT NULL THEN { from: poiDate.from, to: poiDate.to, objectBound: false }
             ELSE { from: objFrom.value, to: objTo.value, objectBound: true }
           END AS date,
           poi.damageFactors AS damageFactors,
           'uh4d' AS type
  `;

  const params = {
    id: req.params.id
  };

  try {

    const results = await neo4j.readTransaction(q, params);
    if (results[0]) {
      res.json(results[0]);
    } else {
      next(new NotFound(`POI with ID ${req.params.id} not found!`))
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * POI request body.
 * @typedef {Object} POIRequest
 * @property {string} title
 * @property {string} content
 * @property {number[]} position
 * @property {number[]} latitude
 * @property {number[]} longitude
 * @property {number[]} altitude
 * @property {string} objectId
 * @property {{from: string, to: string, objectBound: boolean}} date
 * @property {string[]=} damageFactors
 * @property {string=} scene
 */

/**
 * @param req {e.Request<{}, {}, POIRequest>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export async function save(req, res, next) {

  const requiredProps = ['title', 'content', 'date', 'latitude', 'longitude', 'altitude'];
  if (!checkRequiredProperties(req.body, requiredProps)) {
    return next(new BadRequest('Missing values:  ' + requiredProps.join('|')));
  }

  // check if date has correct format
  if (!checkDateFormat([req.body.date.from, req.body.date.to])) {
    return next(new BadRequest('Wrong date formats!'));
  }

  // check if damageFactors has correct format
  if (req.body.damageFactors && !Array.isArray(req.body.damageFactors)) {
    return next(new BadRequest('Wrong format for damageFactors!'));
  }

  // language=Cypher
  const q = `
    MATCH (tpoi:E55:UH4D {id: "poi"}) ${req.body.scene ? ', (scene:E53:UH4D {id: $scene})' : ''}
    CREATE (poi:E73:UH4D {id: $id, timestamp: datetime()})-[:P67]->(e92:E92:UH4D {id: $e92id})-[:P161]->(place:E53:UH4D {id: $placeId})-[:P168]->(geo:E94:UH4D $geo),
           ${req.body.scene ? '(place)-[:P89]->(scene),' : ''}
           (poi)-[:P2]->(tpoi),
           (poi)-[:P102]->(title:E35:UH4D $title),
           (poi)-[:P3]->(content:E62:UH4D $content)
    ${req.body.damageFactors ? 'SET poi.damageFactors = $damageFactors' : ''}
    
    WITH poi, e92, geo, title, content
    
    CALL apoc.do.when($objectId IS NOT NULL,
      'MATCH (object:E22 {id: objectId}) CREATE (poi)-[:P67]->(object) RETURN object',
      'RETURN NULL AS object',
      { poi: poi, objectId: $objectId }) YIELD value
    
    WITH poi, e92, geo, title, content, value.object AS object
    OPTIONAL MATCH (object)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(objFrom:E61)
    OPTIONAL MATCH (object)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(objTo:E61)
    
    CALL apoc.do.when($objectId IS NOT NULL AND $dateMap.objectBound,
      'RETURN { from: objFrom.value, to: objTo.value } AS date, true AS objectBound',
      'CREATE (e92)-[:P160]->(:E52:UH4D {id: e52id})-[:P82]->(date:E61:UH4D {id: dateMap.id, from: CASE WHEN dateMap.from IS NULL THEN NULL ELSE date(dateMap.from) END, to: CASE WHEN dateMap.to IS NULL THEN NULL ELSE date(dateMap.to) END}) RETURN date, false AS objectBound',
      { e92: e92, e52id: $e52id, dateMap: $dateMap, objFrom: objFrom, objTo: objTo }) YIELD value AS dateValue
    
    RETURN poi.id AS id,
           title.value AS title,
           content.value AS content,
           apoc.map.removeKey(geo, 'id') as location,
           object.id AS objectId,
           poi.timestamp As timestamp,
           { from: dateValue.date.from, to: dateValue.date.to, objectBound: dateValue.objectBound } AS date,
           poi.damageFactors AS damageFactors,
           'uh4d' AS type
  `;

  const id = shortId.generate();

  const params = {
    scene: req.body.scene,
    id: 'e73_poi_' + id,
    placeId: 'e53_poi_' + id,
    e92id: 'e92_poi_' + id,
    geo: {
      id: 'e94_poi_' + id,
      position: req.body.position,
      latitude: req.body.latitude,
      longitude: req.body.longitude,
      altitude: req.body.altitude
    },
    title: {
      id: 'e35_poi_' + id,
      value: req.body.title
    },
    content: {
      id: 'e62_poi_' + id,
      value: req.body.content
    },
    objectId: req.body.objectId || null,
    damageFactors: req.body.damageFactors || null,
    e52id: 'e52_poi_' + id,
    dateMap: {
      id: 'e61_poi_' + id,
      from: req.body.date.from ? moment(req.body.date.from).format('YYYY-MM-DD') : null,
      to: req.body.date.to ? moment(req.body.date.to).format('YYYY-MM-DD') : null,
      objectBound: !!req.body.date.objectBound
    }
  };

  try {

    const results = await neo4j.writeTransaction(q, params);

    if (results[0]) {
      res.json(results[0]);
    } else {
      next(new BadRequest('No POI created!'));
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * @param req {e.Request<{id: string}>}
 * @param res {e.Response}
 * @param next {Function}
 * @return {Promise<void>}
 */
export async function update(req, res, next) {

  const requiredProps = ['title', 'content', 'date'];
  if (!checkRequiredProperties(req.body, requiredProps)) {
    return next(new BadRequest('Missing values: ' + requiredProps.join('|')));
  }

  // check if date has correct format
  if (!checkDateFormat([req.body.date.from, req.body.date.to])) {
    return next(new BadRequest('Wrong date formats!'));
  }

  // check if damageFactors has correct format
  if (req.body.damageFactors && !Array.isArray(req.body.damageFactors)) {
    return next(new BadRequest('Wrong format for damageFactors!'));
  }

  // language=Cypher
  let q = `
    MATCH (poi:E73 {id: $id})-[:P2]->(:E55 {id: "poi"}),
          (poi)-[:P102]->(title:E35),
          (poi)-[:P3]->(content:E62),
          (poi)-[:P67]->(e92:E92)-[:P161]->(place:E53)-[:P168]->(geo:E94)
    OPTIONAL MATCH (poi)-[:P67]->(e22:E22)
    OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(objFrom:E61)
    OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(objTo:E61)
    
    CALL apoc.do.when(e22 IS NOT NULL AND $dateMap.objectBound,
      'OPTIONAL MATCH (e92)-[:P160]->(e52:E52)-[:P82]->(date:E61) DETACH DELETE e52, date RETURN { from: objFrom.value, to: objTo.value } AS date, true AS objectBound',
      'MERGE (e92)-[:P160]->(e52:E52:UH4D)-[:P82]->(date:E61:UH4D) ON CREATE SET e52.id = e52id, date.id = dateMap.id, date.from = CASE WHEN dateMap.from IS NULL THEN NULL ELSE date(dateMap.from) END, date.to = CASE WHEN dateMap.to IS NULL THEN NULL ELSE date(dateMap.to) END RETURN date, false AS objectBound',
      { e92: e92, e52id: $e52id, dateMap: $dateMap, objFrom: objFrom, objTo: objTo }) YIELD value AS dateValue
    
    CALL apoc.do.when($damageFactors IS NULL,
      'REMOVE poi.damageFactors RETURN poi',
      'SET poi.damageFactors = factors RETURN poi',
      { poi: poi, factors: $damageFactors }) YIELD value 

    SET title.value = $title,
    content.value = $content,
    poi.camera = $camera
    
    RETURN poi.id AS id,
           title.value AS title,
           content.value AS content,
           apoc.map.removeKey(geo, 'id') AS location,
           e22.id AS objectId,
           poi.camera AS camera,
           poi.timestamp As timestamp,
           { from: dateValue.date.from, to: dateValue.date.to, objectBound: dateValue.objectBound } AS date,
           poi.damageFactors AS damageFactors,
           'uh4d' AS type
  `;

  const id = shortId.generate();

  const params = {
    id: req.params.id,
    title: req.body.title,
    content: req.body.content,
    e52id: 'e52_poi_' + id,
    dateMap: {
      id: 'e61_poi_' + id,
      from: req.body.date.from ? moment(req.body.date.from).format('YYYY-MM-DD') : null,
      to: req.body.date.to ? moment(req.body.date.to).format('YYYY-MM-DD') : null,
      objectBound: !!req.body.date.objectBound
    },
    camera: req.body.camera,
    damageFactors: req.body.damageFactors || null
  };

  try {

    const results = await neo4j.writeTransaction(q, params);
    if (results[0]) {
      res.json(results[0]);
    } else {
      next(new NotFound(`POI with ID ${req.params.id} not found!`));
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * @param req {e.Request<{id: string}>}
 * @param res {e.Response}
 * @param next {Function}
 * @return {Promise<void>}
 */
export async function remove(req, res, next) {

  // language=Cypher
  let q = `
    MATCH (poi:E73 {id: $id})-[:P2]->(:E55 {id: "poi"}),
          (poi)-[:P102]->(title:E35),
          (poi)-[:P3]->(content:E62),
          (poi)-[:P67]->(e92:E92)-[:P161]->(place:E53)-[:P168]->(geo:E94)
    OPTIONAL MATCH (e92)-[:P160]->(e52:E52)-[:P82]->(date:E61)
    DETACH DELETE poi, place, geo, title, content, e92, e52, date
    RETURN true AS check
  `;

  const params = {
    id: req.params.id
  };

  try {

    const results = await neo4j.writeTransaction(q, params);
    if (results[0]) {
      res.send();
    } else {
      next(new NotFound(`POI with ID ${req.params.id} not found!`))
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}
