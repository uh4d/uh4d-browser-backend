import { Router } from 'express';
import errPkg from 'http-errors';
import * as neo4j from 'neo4j-request';

const { InternalServerError, NotFound } = errPkg;

const router = Router({ mergeParams: true });

router.get('/', query);
router.get('/:scene', get);

export default router;

/**
 * @param req {e.Request<{}, {}, {}, {lat?: string, lon?: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
async function query(req, res, next) {

  // check query parameter
  const spatialParams = {
    latitude: parseFloat(req.query.lat),
    longitude: parseFloat(req.query.lon)
  };
  const doSpatialQuery = !isNaN(spatialParams.latitude) && !isNaN(spatialParams.longitude);

  // compose query
  let q = `
    MATCH (scene:E53:UH4D)-[:P1]->(name:E41),
          (scene)<-[:P121]-(terrain)<-[:P67]-(:D1)-[:P106]->(file:D9),
          (terrain)-[:P168]->(geo:E94)`;

  if (doSpatialQuery) {
    q += `
      WHERE $geo.latitude > geo.south AND $geo.latitude < geo.north
      AND $geo.longitude > geo.west AND $geo.longitude < geo.east`;
  }

  q += `
    RETURN scene.id AS id,
           name.value AS name,
           apoc.map.removeKey(geo, 'id') AS location,
           apoc.map.removeKey(file, 'id') AS file
  `;

  const params = {
    geo: spatialParams
  };

  try {

    const results = await neo4j.readTransaction(q, params);
    res.json(results);

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * @param req {e.Request<{scene: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
async function get(req, res, next) {

  // language=Cypher
  const q = `
    MATCH (scene:E53:UH4D {id: $sceneId})-[:P1]->(name:E41),
          (scene)<-[:P121]-(terrain)<-[:P67]-(:D1)-[:P106]->(file:D9),
          (terrain)-[:P168]->(geo:E94)
    RETURN scene.id AS id,
           name.value AS name,
           apoc.map.removeKey(geo, 'id') AS location,
           apoc.map.removeKey(file, 'id') AS file
  `;

  const params = {
    sceneId: req.params.scene
  };

  try {

    const results = await neo4j.readTransaction(q, params);

    if (results[0]) {
      res.json(results[0]);
    } else {
      next(new NotFound());
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}
