import { URL } from 'url';
import errPkg from 'http-errors';
import got from 'got';
import request from 'request';
import * as config from '../config.js';

const { BadRequest, InternalServerError } = errPkg;

/**
 * Use server as proxy for provided url.
 * @param req {e.Request<{}, any, undefined, {url: string}>}
 * @param res {e.Response}
 * @param next {Function}
 * @return {Promise<void>}
 */
export default async function redirect(req, res, next) {

  if (!req.query.url) {
    return next(new BadRequest('Query parameter `url` hast to be set!'));
  }

  // remove last slash, if any
  const trimmedHost = config.host.replace(/\/$/, '');
  const escapedHost = trimmedHost.replace(/\//g, '\\/');

  // query parameters are automatically decoded
  const url = new URL(req.query.url);

  // only replace contents if requested file is a text file, otherwise pass forward
  if (!/\/$|\/[^./]+$|\.(?:js|css|html|txt)$/.test(url.pathname)) {
    // stream file https://stackoverflow.com/questions/26982996/trying-to-proxy-an-image-in-node-js-express
    request.get(url.href).pipe(res);
    return;
  }

  // fetch file
  let response;
  try {
    response = await got(req.query.url);
  } catch (e) {
      return next(new BadRequest(e));
  }

  try {

    // always replace &amp; and encode uri

    // replace all unsecure http links with redirect url
    let replaced = response.body.replace(
      /(http:\/\/[^"'\s]+)/g,
      (match, p1) => trimmedHost + '/api/redirect?url=' + encodeURIComponent(p1.replace(/&amp;/g, '&'))
    );

    // unsecure links that are escaped
    replaced = replaced.replace(
      /(?<!\/\^?)(http:\\\/\\\/(?:(?!"|'|\s|\\").)+)/g,
      (match, p1) => escapedHost + '\\/api\\/redirect?url=' + encodeURIComponent(p1.replace(/&amp;/g, '&'))
    );

    // prefix relative link
    replaced = replaced.replace(
      /((?:href|src)=["'])(?!(?:https?:\/\/|\/\/)|javascript:)\/?([^"']*)/g,
      (match, p1, p2) => p1 + trimmedHost + '/api/redirect?url=' + encodeURIComponent(url.origin + '/' + p2.replace(/&amp;/g, '&'))
    );

    // absolute links without protocol
    replaced = replaced.replace(
      /((?:href|src)=["'])(\/\/[^"']*)/g,
      (match, p1, p2) => p1 + trimmedHost + '/api/redirect?url=' + encodeURIComponent(url.protocol + p2.replace(/&amp;/g, '&'))
    );

    // if css file, replace relative url links
    if (/\/[^/]+\.css(?:\?|$)/.test(url.href)) {
      replaced = replaced.replace(
        /(url\('?"?)(?!#)([^:'")]+)('?"?\))/g,
        (match, p1, p2, p3) => p1 + trimmedHost + '/api/redirect?url=' + encodeURIComponent(url.href.replace(/\/[^/]+\.css.*$/, '/') + p2.replace(/&amp;/g, '&')) + p3
      );
    }

    res.setHeader('content-type', response.headers['content-type']);
    res.send(replaced);

  } catch (e) {

    next(new InternalServerError(e));

  }

}
