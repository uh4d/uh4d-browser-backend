import multer from 'multer';
import * as shortId from 'shortid';
import * as path from 'path';
import { Subject } from 'rxjs';
import { rateLimit as rateLimitOperator } from '../modules/utils.js';
import logger from '../modules/logger.js';
import * as config from '../config.js';

// file uploads
const storage = multer.diskStorage({
  destination(req, file, callback) {
    callback(null, path.join(config.path.data, 'tmp'));
  },
  filename(req, file, callback) {
    callback(null, file.fieldname + '_' + shortId.generate());
  }
});
export const mUpload = multer({ storage });

/**
 * Middleware to log file uploads.
 * @param req {e.Request}
 * @param res {e.Response}
 * @param next {Function}
 */
export function logFileUpload(req, res, next) {
  const files = req.files || [req.file];
  files.forEach(file => {
    logger.info('File Upload:', file.originalname, file.path, file.size);
  });
  next();
}

/**
 * Middleware to rate limit requests.
 * @param limit {number} Number of requests
 * @param window {number} Rolling window duration (in milliseconds)
 * @return {(function(e.Request, e.Response, function))}
 */
export function rateLimit(limit, window) {

  const subject = new Subject();

  subject
    .pipe(rateLimitOperator(limit, window))
    .subscribe(next => {
      next();
    });

  return function (req, res, next) {
    subject.next(next);
  };

}
