import { Router } from 'express';
import * as neo4j from 'neo4j-request';
import errPkg from 'http-errors';

const { InternalServerError, NotFound } = errPkg;

const router = Router({ mergeParams: true });

router.get('/', query);
router.get('/:id', get);

export default router;

/**
 * @param req {e.Request<null, any, any, {scene?: string, date?: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
async function query(req, res, next) {

  // language=Cypher
  const q = `
    MATCH (e92:E92:UH4D)-[:P161]->(:E53 ${req.query.scene ? '{id: $scene}' : ''})-[:P168]->(geo:E94),
          (e92)-[:P160]->(:E52)-[:P82]->(date:E61 ${req.query.date ? '{value: $date}' : ''}),
          (e92)<-[:P67]-(:E38)-[:P106]->(file:D9)
    RETURN e92.id AS id,
           date.value AS date,
           apoc.map.removeKey(file, 'id') AS file,
           apoc.map.removeKey(geo, 'id') AS location
  `;

  const params = {
    scene: req.query.scene,
    date: req.query.date
  };

  try {

    const results = await neo4j.readTransaction(q, params);
    res.json(results);

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * @param req {e.Request<{id: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
async function get(req, res, next) {

  // language=Cypher
  const q = `
    MATCH (e92:E92:UH4D {id: $id})-[:P161]->(:E53)-[:P168]->(geo:E94),
          (e92)-[:P160]->(:E52)-[:P82]->(date:E61),
          (e92)<-[:P67]-(:E38)-[:P106]->(file:D9)
    RETURN e92.id AS id,
           date.value AS date,
           apoc.map.removeKey(file, 'id') AS file,
           apoc.map.removeKey(geo, 'id') AS location
  `;

  const params = {
    id: req.query.id
  };

  try {

    const results = await neo4j.readTransaction(q, params);

    if (results[0]) {
      res.json(results[0]);
    } else {
      next(new NotFound());
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}
