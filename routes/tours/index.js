import { Router } from 'express';

const router = Router({ mergeParams: true });

import { query, remove, save } from './tours.js';
router.get('/', query);
router.post('/', save);
router.delete('/:id', remove);

export default router;
