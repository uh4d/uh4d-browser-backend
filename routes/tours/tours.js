import errPkg from 'http-errors';
import * as shortId from 'shortid';
import * as neo4j from 'neo4j-request';
import { checkRequiredProperties } from '../../modules/utils.js';

const { BadRequest, InternalServerError, NotFound } = errPkg;

/**
 * @param req {e.Request<null, any, any, {scene?: string, lat?: string, lon?: string, r?: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export async function query(req, res, next) {

  // parse query parameter
  const spatialParams = {
    latitude: parseFloat(req.query.lat),
    longitude: parseFloat(req.query.lon),
    radius: parseFloat(req.query.r)
  };
  const doSpatialQuery = !isNaN(spatialParams.latitude) && !isNaN(spatialParams.longitude) && !isNaN(spatialParams.radius);

  // language=Cypher
  let q = `
    MATCH (tour:Tour)-[r:CONTAINS]->(poi:E73)-[:P2]->(:E55 {id: "poi"}),
          (poi)-[:P67]->(:E92)-[:P161]->(place:E53)-[:P168]->(geo:E94),
          (poi)-[:P102]->(title:E35)
    ${ req.query.scene ? 'WHERE (place)-[:P89]->(:E53:UH4D {id: $scene})' : '' }
    WITH tour, poi, geo, title
    ORDER BY r.order
    WITH tour, collect({id: poi.id, title: title.value, geo: geo}) AS pois`;

  if (doSpatialQuery) {
    q += `, point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint
      WHERE any(p IN pois WHERE distance(userPoint, point({latitude: p.geo.latitude, longitude: p.geo.longitude})) < $geo.radius)`;
  }

  // language=Cypher
  q += `
    RETURN tour.id AS id,
           tour.title AS title,
           [p IN pois | {id: p.id, title: p.title}] as pois
  `;

  const params = {
    scene: req.query.scene,
    geo: spatialParams
  };

  try {

    const results = await neo4j.readTransaction(q, params);
    res.json(results);

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * @param req {e.Request<null, any, {title: string, pois: string[]}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export async function save(req, res, next) {

  // check request body
  const requiredProps = ['title', 'pois'];
  if (!checkRequiredProperties(req.body, requiredProps)) {
    return next(new BadRequest('Missing essential data: ' + requiredProps.join('|')));
  }

  if (!Array.isArray(req.body.pois) || !req.body.pois.length || !req.body.pois.some(p => typeof p === 'string')) {
    return next(new BadRequest('`pois` must be an array of strings!'));
  }

  // language=Cypher
  const q = `
    CREATE (tour:Tour:UH4D $tourMap)
    WITH tour, range(0, size($pois) - 1) as indices
    UNWIND indices as i
    MATCH (poi:E73 {id: $pois[i]})-[:P2]->(:E55 {id: "poi"}),
          (poi)-[:P102]->(pTitle:E35)
    CREATE (tour)-[:CONTAINS {order: i}]->(poi)
    RETURN tour.id AS id,
           tour.title AS title,
           collect({id: poi.id, title: pTitle.value}) as pois
  `;

  const params = {
    tourMap: {
      id: 'tour_' + shortId.generate(),
      title: req.body.title
    },
    pois: req.body.pois
  };

  try {

    const results = await neo4j.writeTransaction(q, params);
    if (results[0]) {
      res.json(results[0]);
    } else {
      next(new BadRequest('Some POIs could not be found!'));
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * @param req {e.Request<{id: string}>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export async function remove(req, res, next) {

  // language=Cypher
  const q = `
    MATCH (poi:E73)<-[:CONTAINS]-(tour:Tour {id: $id})
    DETACH DELETE tour
    RETURN true AS check
  `;

  const params = {
    id: req.params.id
  };

  try {

    const results = await neo4j.writeTransaction(q, params);
    if (results[0]) {
      res.send();
    } else {
      next(new NotFound(`Tour with ID ${req.params.id} not found!`))
    }

  } catch (e) {

    next(new InternalServerError(e));

  }

}
