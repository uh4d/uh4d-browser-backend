import got from 'got';
import request from 'request';
import errPkg from 'http-errors';
import Coordinates from '../modules/coordinates.js';

const { BadRequest, InternalServerError } = errPkg;

/**
 * Elevation query object.
 * @typedef {Object} ElevationQuery
 * @property {string} lat
 * @property {string} lon
 * @property {string} r
 * @property {string} [dataSet]
 * @property {string} [textured]
 * @property {string} [imageryProvider]
 * @property {string} [textureQuality]
 */

/**
 * Proxy request terrain model from Elevation API.
 * Response contains meta information about the generated terrain file.
 *
 * [More details]{@link https://api.elevationapi.com/index.html}
 * @param req {e.Request<{}, any, {}, ElevationQuery>}
 * @param res {e.Response}
 * @param next
 * @return {Promise<void>}
 */
export async function getTerrainMeta(req, res, next) {

  // Rate limit requests. Only one request every 5 seconds (https://elevationapi.com/faq)

  // parse required query parameters
  const latitude = parseFloat(req.query.lat);
  const longitude = parseFloat(req.query.lon);
  const radius = parseFloat(req.query.r);

  if (isNaN(latitude) || isNaN(longitude) || isNaN(radius)) {
    return next(new BadRequest('Missing query parameters: lat|lon|r'));
  }

  // parse optional query parameters
  const dataSet = req.query.dataSet || 'SRTM_GL1';
  const textured = req.query.textured || 'true';
  const imageryProvider = req.query.imageryProvider || 'ThunderForest-Neighbourhood';
  const textureQuality = req.query.textureQuality || '2';

  try {

    const { west, east, south, north } = new Coordinates(latitude, longitude).computeBoundingBox(radius);

    const response = await got(`https://api.elevationapi.com/api/model/3d/bbox/${west},${east},${south},${north}?=dataSet=${dataSet}&textured=${textured}&imageryProvider=${imageryProvider}&textureQuality=${textureQuality}&centerOnOrigin=false`);

    res.json(JSON.parse(response.body));

  } catch (e) {

    next(new InternalServerError(e));

  }

}

/**
 * Proxy request terrain file from Elevation API.
 * @param req {e.Request<{folder: string, file: string}>}
 * @param res {e.Response}
 */
export function getTerrainGltf(req, res) {

  request.get(`https://api.elevationapi.com/gltf/${req.params.folder}/${req.params.file}`).pipe(res);

}
