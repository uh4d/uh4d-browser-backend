import * as path from 'path';
import fs from 'fs-extra';
import errPkg from 'http-errors';
import * as mime from 'mime-types';
import * as config from '../config.js';

const { BadRequest } = errPkg;

/**
 * Take array of file requests, load files, and return as base64 encoded strings.
 * @param req {e.Request<{}, any, {files: string[]}>}
 * @param res {e.Response}
 * @param next {Function}
 * @return {Promise<void>}
 */
export async function batchFiles(req, res, next) {

  if (!Array.isArray(req.body.files)) {
    return next(new BadRequest('Missing array with url of files: files {string[]}!'));
  }

  const promises = req.body.files.map(async (url) => {

    try {

      // strip data from url, if set
      const filePath = url.replace(/^data\//, '');

      // read binary data
      const file = await fs.readFile(path.join(config.path.data, filePath), 'base64');

      // convert binary data to base64 encoded string
      return {
        status: 200,
        url,
        mimeType: mime.lookup(url),
        file
      };

    } catch (e) {

      return {
        status: e.code === 'ENOENT' ? 404 : 500,
        error: e.toString(),
        url
      };

    }

  });

  res.json(await Promise.all(promises));

}
