// swap omega and phi
MATCH (n)
WHERE exists(n.omega) AND exists(n.phi)
WITH n, n.omega AS temp
SET n.omega = n.phi,
    n.phi = temp
RETURN n;

// transform spatial node into camera node
MATCH (img:E38:UH4D)<-[:P94]-(e65:E65)-[:P7]->(:E53)-[:P168]->(geo:E94),
      (img)-[:has_spatial]->(spatial:Spatial)
MERGE (e65)-[:P16]->(camera:E22:UH4D)
ON CREATE SET camera.id = replace(spatial.id, 'spatial_', 'camera_')
SET camera += apoc.map.removeKey(spatial, 'id'),
    camera.omega = geo.omega,
    camera.phi = geo.phi,
    camera.kappa = geo.kappa
REMOVE geo.omega, geo.phi, geo.kappa
DETACH DELETE spatial
RETURN img, e65, camera;
