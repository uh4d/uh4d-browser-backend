// refer poi to spacetime volume
MATCH (poi:E73:UH4D)-[:P2]->(:E55 {id: "poi"}),
      (poi)-[r:P67]->(place:E53)
CREATE (e92:E92:UH4D {id: replace(poi.id, "e73_", "e92_")})
MERGE (poi)-[:P67]->(e92)
MERGE (e92)-[:P161]->(place)
DELETE r
RETURN poi, e92, place
