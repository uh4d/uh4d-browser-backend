CALL apoc.export.cypher.query("MATCH (image:E38:UH4D)-[r1:P106]->(file:D9),
      (image)-[r2:P102]->(title:E35),
      (image)-[r3:P48]->(identifier:E42),
      (image)<-[r4:P94]-(e65:E65)
OPTIONAL MATCH (e65)-[r5:P14]->(e21:E21)-[r6:P131]->(author:E82)
OPTIONAL MATCH (e65)-[r7:P4]->(e52:E52)-[r8:P82]->(date:E61)
OPTIONAL MATCH (image)-[r9:P105]->(e40:E40)-[r10:P131]->(owner:E82)
OPTIONAL MATCH (image)-[r11:P3]->(desc:E62)-[r12:P3_1]->(tDesc:E55 {id: 'image_description'})
OPTIONAL MATCH (image)-[r13:P3]->(misc:E62)-[r14:P3_1]->(tMisc:E55 {id: 'image_miscellaneous'})
OPTIONAL MATCH (image)-[r15:has_spatial]->(spatial:Spatial)
OPTIONAL MATCH (image)-[r16:has_tag]->(tag:TAG)

RETURN *", "images.cypher", {cypherFormat: "updateAll"});
