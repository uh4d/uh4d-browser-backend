// remove all dummy images
MATCH (img:E38)
WHERE img.id =~ ".*_dummy$"
MATCH (img)-[:P102]->(title:E35),
      (img)<-[:P94]-(e65:E65),
      (img)-[:P48]->(identifier:E42),
      (img)-[:P106]->(file:D9),
      (img)-[:has_spatial]->(spatial:Spatial)
OPTIONAL MATCH (e65)-[:P7]->(place:E53)-[:P168]->(geo:E94)
DETACH DELETE img, title, e65, identifier, file, spatial, place, geo;
