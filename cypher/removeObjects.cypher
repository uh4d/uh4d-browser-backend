// remove old object entries
MATCH (e22:E22:UH4D)-[:P1]->(name:E41),
      (e22)-[:P67|P106*1..]-(obj:D1),
      (obj)<-[:L11]-(event:D7),
      (obj)-[:P106]->(file:D9)
WHERE event.id = "d7_HyHLA6jGf"
OPTIONAL MATCH (e22)<-[:P108]-(fc:E12)-[:P4]->(fts:E52)-[:P82]->(from:E61)
OPTIONAL MATCH (e22)<-[:P13]-(td:E6)-[:P4]->(tts:E52)-[:P82]->(to:E61)
OPTIONAL MATCH (obj)-[:P2]->(mat:E57)
DETACH DELETE e22, name, obj, event, file, fc, fts, from, td, tts, to, mat
//RETURN e22, name, obj, event, file, fc, fts, from, td, tts, to, mat

// stadt_lod2: d7_HJrdAjjfG
// residenz: d7_HyHLA6jGf
