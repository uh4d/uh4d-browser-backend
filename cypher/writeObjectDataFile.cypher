CALL apoc.export.cypher.query("MATCH (e22:E22:UH4D)-[r1:P1]->(name:E41),
          (e22)<-[r2:P67]-(object:D1)-[r3:P106]->(file:D9)
    OPTIONAL MATCH (e22)<-[r4:P108]-(e12:E12)-[r5:P4]->(e52from:E52)-[r6:P82]->(from:E61)
    OPTIONAL MATCH (e22)<-[r7:P13]-(e6:E6)-[r8:P4]->(e52to:E52)-[r9:P82]->(to:E61)
    OPTIONAL MATCH (e22)-[r10:P3]->(misc:E62)
    OPTIONAL MATCH (e22)<-[r11:P157]-(e53:E53)-[r12:P87]->(addr:E45)
    OPTIONAL MATCH (addr)-[r13:P139]-(fAddr:E45)
    OPTIONAL MATCH (e22)-[r14:has_tag]->(tag:TAG)

RETURN *", "objects.cypher", {cypherFormat: "updateAll"});
