# Export data

This script extracts all images or objects of a scene.
The extracted data can be used to update the data and database of another UH4D server instance.

## Usage

Execute export script:

```
node index.js [OPTION]

Options:
  -s, --scene=      Scene ID
  -t, --type=       Media type: image | object
  -o, --output=     Output directory                       (default: output)
      --neo4j-home= Path to Neo4j directory (only Windows)
  -h, --help        display this help
```

In the output folder, an `image` or `object` folder containing the respective data and a `image-import.cypher` or `object-import.cypher` file will be created.

Upload the data to the server and/or copy it into the application data folder.

Execute the cypher file to update database:

    cypher-shell -u neo4j -p <password> -d uh4d -f object-import.cypher

## Additional notes

The Neo4j instance, from where the data is exported, has to be configured with

    apoc.export.file.enabled=true
