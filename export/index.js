import nodeGetopt from 'node-getopt';
import * as path from 'path';
import * as os from 'os';
import fs from 'fs-extra';
import { SingleBar, Presets } from 'cli-progress';
import * as neo4j from 'neo4j-request';
import * as config from '../config.js';

const getopt = nodeGetopt.create([
  ['s', 'scene=', 'Scene ID'],
  ['t', 'type=', 'Media type: image | object'],
  ['o', 'output=', 'Output directory', 'output'],
  ['', 'neo4j-home=', 'Path to Neo4j directory (only Windows)']
]).bindHelp();
const opt = getopt.parseSystem();

let importPath = '/var/lib/neo4j/import';

if (!opt.options.scene) {
  console.warn('No scene ID provided!');
  getopt.showHelp();
  process.exit(1);
}

if (opt.options.type !== 'image' && opt.options.type !== 'object') {
  console.warn('No or wrong type provided! Please use `image` or `object`.');
  getopt.showHelp();
  process.exit(1);
}

if (os.platform() === 'win32') {
  if (!opt.options['neo4j-home']) {
    console.warn('No Neo4j home directory provided!');
    getopt.showHelp();
    process.exit(1);
  }
  importPath = path.join(opt.options['neo4j-home'], 'import');
}

main().then(() => {
  console.log('Done!');
  process.exit();
}).catch(reason => {
  console.error('Something went wrong!');
  console.error(reason);
  process.exit(2);
});

async function main() {

  // create output folder
  await fs.ensureDir(opt.options.output);

  // connect to database
  await neo4j.init(config.neo4j.url, config.neo4j.user, config.neo4j.password, config.neo4j.database);

  // prepare cypher statements
  let qFiles;
  let qExport;

  if (opt.options.type === 'image') {

    // language=Cypher
    qFiles = `
      MATCH (:E53:UH4D {id: $scene})<-[:P89]-(:E53)<-[:P7]-(:E65)-[:P94]->(:E38)-[:P106]->(file:D9)
      RETURN file.path AS path
    `;

    // language=Cypher
    qExport = `
      CALL apoc.export.cypher.query("MATCH (:E53:UH4D {id: '${opt.options.scene}'})<-[r1:P89]-(place:E53)<-[r2:P7]-(e65:E65)-[r3:P94]->(image:E38:UH4D),
          (image)-[r4:P106]->(file:D9),
          (image)-[r5:P102]->(title:E35),
          (image)-[r6:P48]->(identifier:E42)
        OPTIONAL MATCH (e65)-[r7:P14]->(e21:E21)-[r8:P131]->(author:E82)
        OPTIONAL MATCH (e65)-[r9:P4]->(e52:E52)-[r10:P82]->(date:E61)
        OPTIONAL MATCH (image)-[r11:P105]->(e40:E40)-[r12:P131]->(owner:E82)
        OPTIONAL MATCH (image)-[r13:P3]->(desc:E62)-[r14:P3_1]->(tDesc:E55 {id: 'image_description'})
        OPTIONAL MATCH (image)-[r15:P3]->(misc:E62)-[r16:P3_1]->(tMisc:E55 {id: 'image_miscellaneous'})
        OPTIONAL MATCH (image)-[r17:has_spatial]->(spatial:Spatial)
        OPTIONAL MATCH (place)-[r18:P168]->(geo:E94)
        OPTIONAL MATCH (image)-[r19:has_tag]->(tag:TAG)
  
        RETURN *", "image-import.cypher", {cypherFormat: 'updateAll'});
    `;

  } else if (opt.options.type === 'object') {

    // language=Cypher
    qFiles = `
      MATCH (:E53:UH4D {id: $scene})<-[:P89]-(:E53)-[:P157]->(:E22)<-[:P67]-(:D1)-[:P106]->(file:D9)
      RETURN file.path AS path
    `;

    // language=Cypher
    qExport = `
      CALL apoc.export.cypher.query("MATCH (city:E53:UH4D {id: '${opt.options.scene}'})<-[r15:P89]-(place:E53)-[r11:P157]->(e22:E22:UH4D)-[r1:P1]->(name:E41),
              (e22)<-[r2:P67]-(object:D1)-[r3:P106]->(file:D9)
        OPTIONAL MATCH (e22)<-[r4:P108]-(e12:E12)-[r5:P4]->(e52from:E52)-[r6:P82]->(from:E61)
        OPTIONAL MATCH (e22)<-[r7:P13]-(e6:E6)-[r8:P4]->(e52to:E52)-[r9:P82]->(to:E61)
        OPTIONAL MATCH (e22)-[r10:P3]->(misc:E62)
        OPTIONAL MATCH (place)-[r12:P87]->(addr:E45)
        OPTIONAL MATCH (addr)-[r13:P139]-(fAddr:E45)
        OPTIONAL MATCH (e22)-[r14:has_tag]->(tag:TAG)
    
        RETURN *", 'object-import.cypher', {cypherFormat: 'updateAll'});
    `;

  }

  // query directory paths
  console.log('Read data...');

  const paths = await neo4j.readTransaction(qFiles, {scene: opt.options.scene});
  if (paths.length < 1) {
    console.warn('No data to export!');
    process.exit();
  }

  // copy each data to output folder
  console.log('Copy files...');

  const progressBar = new SingleBar({}, Presets.shades_classic);
  progressBar.start(paths.length, 0);

  for (const p of paths) {
    const srcPath = path.join(config.path.data, p.path);
    const dstPath = path.join(opt.options.output, p.path);
    await fs.copy(srcPath, dstPath);
    progressBar.increment();
  }

  progressBar.stop();

  console.log('Export cypher...');

  // execute cypher export
  await neo4j.readTransaction(qExport);

  // copy cypher file to output folder
  const cypherFile = `${opt.options.type}-import.cypher`;
  await fs.copy(path.join(importPath, cypherFile), path.join(opt.options.output, cypherFile));

}
