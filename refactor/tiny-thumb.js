import * as path from 'path';
import * as neo4j from 'neo4j-request';
import { execFile } from 'child-process-promise';
import { Presets, SingleBar } from 'cli-progress';
import * as config from '../config.js';

/**
 * Generate tiny thumbnail (64x64) for all images (used for preview in 3D viewport).
 */
main().then(() => {
  console.log('Done!');
  process.exit();
}).catch(err => {
  console.error(err);
  process.exit(1);
});

async function main() {

  await neo4j.init(config.neo4j.url, config.neo4j.user, config.neo4j.password, config.neo4j.database);

  // get images and file information
  // language=Cypher
  const q0 = `MATCH (n:E38:UH4D)-->(file:D9) RETURN n.id AS id, file`;

  const list = await neo4j.readTransaction(q0);

  const paramsList = [];

  // iterate over results and process original image to generate 64x64 tiny thumb
  const progressBar = new SingleBar({}, Presets.shades_classic);
  progressBar.start(list.length, 0);

  for (const item of list) {

    const tinyName = item.file.original.slice(0, item.file.original.lastIndexOf('.')) + '_tiny.jpg';

    await execFile(config.exec.ImageMagick, [path.join(config.path.data, item.file.path, item.file.original), '-resize', '64x64!', path.join(config.path.data, item.file.path, tinyName)]);

    paramsList.push({
      id: item.id,
      tiny: tinyName
    });

    progressBar.increment();

  }

  progressBar.stop();

  // write updated file property to database entry
  // language=Cypher
  const q1 = 'UNWIND $list AS item MATCH (:E38:UH4D {id: item.id})-->(file:D9) SET file.tiny = item.tiny';

  await neo4j.writeTransaction(q1, {list: paramsList});

}
