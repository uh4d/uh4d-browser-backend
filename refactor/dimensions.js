import * as neo4j from 'neo4j-request';
import { execFile } from 'child-process-promise';
import * as config from '../config.js';

/**
 * Determine width and height in pixels of images that have no width/height properties and update database entries.
 */
main().then(() => {
  console.log('Done!');
  process.exit();
});

async function main() {

  await neo4j.init(config.neo4j.url, config.neo4j.user, config.neo4j.password, config.neo4j.database);

  // get images with no width/height property
  // language=Cypher
  const q0 = `MATCH (n:E38:UH4D)-->(file:D9) WHERE NOT exists(file.width) RETURN n.id AS id, file`;

  const list = await neo4j.readTransaction(q0);

  const paramsList = [];

  for (const item of list) {

    // get dimensions of image file
    const result = await execFile(config.exec.ImageIdentify, [config.path.data + '/' + item.file.path + item.file.original]);
    console.log(result.stdout);
    const matches = /\s(\d+)x(\d+)\s/.exec(result.stdout);

    paramsList.push({
      id: item.id,
      width: matches[1],
      height: matches[2]
    });

  }

  // write width/height property to database entry
  // language=Cypher
  const q1 = 'UNWIND $list AS item MATCH (:E38:UH4D {id: item.id})-->(file:D9) SET file.width = item.width, file.height = item.height';

  await neo4j.writeTransaction(q1, {list: paramsList});

}
