import * as path from 'path';
import fs from 'fs-extra';
import * as xmljs from 'xml-js';
import * as shortId from 'shortid';
import { v4 as uuid } from 'uuid';
import * as neo4j from 'neo4j-request';
import getopt from 'node-getopt';
import { execFile } from 'child-process-promise';
import { EdgesGeometry } from 'three';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader.js';
import * as BufferGeometryUtils from 'three/examples/jsm/utils/BufferGeometryUtils.js';
import * as config from '../config.js';

// directories
// const inputDir = "C:\\Users\\Zumbi\\Documents\\3ds Max 2020\\export\\stadt3";
// const inputDir = "residenz";
// const outputDir = "D:/ServerData/UH4D-Data";
// const outputDir = "output";

const opt = getopt.create([
  ['s', 'scene=', 'Scene ID, e.g. "dresden", "jena"'],
  ['i', 'input=', 'Input directory containing OBJ files', 'input'],
  ['o', 'output=', 'Output directory', 'output']
]).bindHelp().parseSystem();

if (!opt.options.scene) {
  console.warn('Unset scene ID! Specify which scene should be affected by refactoring process.');
  process.exit(1);
}

const inputDir = opt.options.input;
const outputDir = opt.options.output;

let sceneConfig;
try {
  sceneConfig = fs.readJSONSync(opt.options.scene + '-config.json');
} catch (e) {
  console.error('Scene config file could not be found!', e);
  process.exit(2);
}

main().then(() => {
  console.log('Done!');
  process.exit();
});

async function main() {

  await neo4j.init(config.neo4j.url, config.neo4j.user, config.neo4j.password);

  fs.ensureDirSync(outputDir);
  fs.ensureDirSync('temp');

  // read input directory
  const files = fs.readdirSync(inputDir);
  console.log(files);

  // process each file
  try {
    for (const filename of files) {

      if (path.extname(filename) !== '.obj') {
        continue;
      }

      console.log('Processing:', filename);

      const name = path.basename(filename, '.obj');
      const fileProps = {
        name,
        original: filename,
        gltf: name + '.gltf',
        path: `objects/${uuid()}/`
      };

      await processFile(fileProps);
      await writeDatabase(fileProps);

    }
  } catch (e) {
    console.error(e);
  }

  fs.removeSync('temp');
}

async function processFile(fileProps) {

  // convert OBJ to COLLADA
  const inputFile = `${inputDir}/${fileProps.original}`;
  await execFile(config.exec.Assimp, ['export', inputFile, 'temp/temp.dae', '-jiv', '-om', '-og']);

  // read COLLADA file
  const daeRaw = await fs.readFile('temp/temp.dae');
  const dae = xmljs.xml2js(daeRaw, {compact: true});

  const daeGeo = dae.COLLADA.library_geometries;
  const daeScene = dae.COLLADA.library_visual_scenes.visual_scene;

  if (!Array.isArray(daeGeo.geometry)) {
    daeGeo.geometry = [daeGeo.geometry]
  }

  if (!Array.isArray(daeScene.node)) {
    daeScene.node = [daeScene.node]
  }

  // load obj
  const objRaw = await fs.readFile(inputFile, { encoding: 'utf8' });
  const obj = await new OBJLoader().parse(objRaw);

  // generate edges
  const edgesGeometries = [];

  obj.children.forEach((child) => {
    const geo = new EdgesGeometry(child.geometry, 24.0);
    edgesGeometries.push(geo);
  });

  const geoCombined = BufferGeometryUtils.mergeBufferGeometries(edgesGeometries);
  geoCombined.name = 'edges';

  // add edges to collada
  daeGeo.geometry.push(geo2dae(geoCombined));

  daeScene.node.push({
    _attributes: {
      id: geoCombined.name,
    },
    matrix: {
      _text: '1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1'
    },
    instance_geometry: {
      _attributes: {
        url: '#' + geoCombined.name + '_geometry'
      },
      bind_material: {
        technique_common: {
          instance_material: {
            _attributes: {
              symbol: 'defaultMaterial',
              target: '#DefaultMaterial'
            }
          }
        }
      }
    }
  });

  // write DAE file
  await fs.writeFile('temp/temp_edges.dae', xmljs.js2xml(dae, {spaces: 2, compact: true}));

  // create folder and copy original file
  console.log('Create folder:', fileProps.path);
  fs.ensureDirSync(`${outputDir}/${fileProps.path}`);
  fs.copySync(inputFile, `${outputDir}/${fileProps.path}${fileProps.original}`)

  // convert DAE to GLTF
  const outputFile = `${outputDir}/${fileProps.path}${fileProps.gltf}`;

  await execFile(config.exec.Collada2Gltf, ['-i', 'temp/temp_edges.dae', '-o', outputFile, '-d', '-t']);

}

/**
 *
 * @param bufferGeo {BufferGeometry}
 */
function geo2dae(bufferGeo) {

  const posAttr = bufferGeo.attributes.position;

  const indexArray = [];
  for (let i = 0, l = posAttr.count; i < l; i++) {
    indexArray.push(i);
  }

  return {
    _attributes: {
      id: bufferGeo.name + '_geometry',
    },
    mesh: {
      source: {
        _attributes: {
          id: bufferGeo.name + '_positions'
        },
        float_array: {
          _attributes: {
            count: posAttr.array.length,
            id: bufferGeo.name + '_positions_array'
          },
          _text: posAttr.array.join(' ')
        },
        technique_common: {
          accessor: {
            _attributes: {
              count: posAttr.count,
              source: '#' + bufferGeo.name + '_positions_array',
              stride: posAttr.itemSize
            }
          },
          param: [
            { _attributes: { name: 'X', type: 'float' }},
            { _attributes: { name: 'Y', type: 'float' }},
            { _attributes: { name: 'Z', type: 'float' }}
          ]
        }
      },
      vertices: {
        _attributes: {
          id: bufferGeo.name + '_vertices'
        },
        input: {
          _attributes: {
            semantic: 'POSITION',
            source: '#' + bufferGeo.name + '_positions'
          }
        }
      },
      lines: {
        _attributes: {
          material: 'defaultMaterial',
          count: posAttr.count / 2
        },
        input: {
          _attributes: {
            offset: 0,
            semantic: 'VERTEX',
            source: '#' + bufferGeo.name + '_vertices'
          }
        },
        p: {
          _text: indexArray.join(' ')
        }
      }
    }
  };

}

async function writeDatabase(fileProps) {

  // language=Cypher
  const q = `
    MATCH (city:E53:UH4D {id: $scene})
    CREATE (e22:E22:UH4D {id: $e22id})-[:P1]->(:E41:UH4D $e41),
           (e22)<-[:P67]-(dobj:D1:UH4D $dobj)-[:P106]->(dfile:D9:UH4D $dfile),
           (e22)<-[:P157]-(place:E53:UH4D {id: $placeId})-[:P89]->(city)
    RETURN e22;
  `;

  const id = shortId.generate() + '_' + fileProps.name;

  const params = {
    scene: sceneConfig.id,
    e22id: 'e22_' + id,
    placeId: 'e53_' + id,
    e41: {
      id: 'e41_' + id,
      value: fileProps.name
    },
    dobj: {
      id: 'd1_' + id,
      matrix: [1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0],
      latitude: sceneConfig.latitude,
      longitude: sceneConfig.longitude,
      altitude: sceneConfig.altitude,
      omega: 0,
      phi: 0,
      kappa: 0
    },
    dfile: {
      id: 'd9_' + id,
      path: fileProps.path,
      file: fileProps.gltf,
      type: 'gltf',
      original: fileProps.original
    }
  };

  const results = await neo4j.writeTransaction(q, params);
  if (!results[0]) {
    throw new Error('No nodes added for ' + fileProps.name);
  }

}
