import { Euler, Matrix4, Vector3 } from 'three';
import * as utm from 'utm';
import * as neo4j from 'neo4j-request';
import * as shortId from 'shortid';
import * as config from '../config.js';

/**
 * Transform matrix to geo-coordinates and update database entries.
 */

if (process.argv[2] !== 'image' && process.argv[2] !== 'object') {
  console.warn('Use `image` or `object` as additional argument to specify which to geo-reference!')
  process.exit(1);
}

main().then(() => {
  console.log('Done!');
  process.exit();
});

async function main() {

  await neo4j.init(config.neo4j.url, config.neo4j.user, config.neo4j.password, config.neo4j.database);

  // query spatialized images

  const q0 = process.argv[2] === 'image' ? /* language=Cypher */ `
    MATCH (img:E38:UH4D)-[:has_spatial]->(spatial)
    RETURN img.id AS id, spatial.matrix AS matrix
  ` : /* language=Cypher */ `
    MATCH (e22:E22:UH4D)<-[:P67]-(obj:D1)
    RETURN e22.id AS id, obj.matrix AS matrix
  `;

  /**
   * @type {{id: string, matrix: number[], longitude?: number, latitude?: number, altitude?: number, omega?: number, phi?: number, kappa?: number}[]}
   */
  const results = await neo4j.readTransaction(q0);

  // transform to longitude/latitude
  const utmOffset = new Vector3(410973.906, 107.392, 5655871.0);
  const utmZone = 33;

  results.forEach(item => {

    const matrix = new Matrix4().fromArray(item.matrix);
    const translation = new Vector3().setFromMatrixPosition(matrix);
    const rotation = new Euler().setFromRotationMatrix(matrix, 'YXZ');
    const utmPosition = translation.clone().multiply(new Vector3(1, 1, -1)).add(utmOffset);
    const latlon = utm.toLatLon(utmPosition.x, utmPosition.z, utmZone, undefined, true);

    Object.assign(item, {
      longitude: latlon.longitude,
      latitude: latlon.latitude,
      altitude: utmPosition.y,
      omega: rotation.x,
      phi: rotation.y,
      kappa: rotation.z
    });

  });

  // write data

  const q1 = process.argv[2] === 'image' ? /* language=Cypher */ `
    UNWIND $items AS item
    MATCH (img:E38:UH4D {id: item.id})<-[:P94]-(e65:E65)
    MERGE (e65)-[:P7]->(place:E53:UH4D)
    ON CREATE SET place.id = item.placeId
    MERGE (place)-[:P168]->(sp:E94:UH4D)
    ON CREATE SET sp.id = item.spId
    SET sp += item.sp
  ` : /* language=Cypher */ `
    UNWIND $items AS item
    MATCH (e22:E22:UH4D {id: item.id})<-[:P67]-(obj:D1)
    SET obj += item.sp
  `;

  const params = {
    items: results.map(item => {
      const id = shortId.generate();
      return {
        id: item.id,
        placeId: 'e53_' + id,
        spId: 'e94_' + id,
        sp: {
          longitude: item.longitude,
          latitude: item.latitude,
          altitude: item.altitude,
          omega: item.omega,
          phi: item.phi,
          kappa: item.kappa
        }
    }})
  };

  await neo4j.writeTransaction(q1, params);

}
