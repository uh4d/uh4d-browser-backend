import * as path from 'path';
import fs from 'fs-extra';
import * as neo4j from 'neo4j-request';
import * as config from '../config.js';

const terrainPath = path.join(config.path.data, 'terrain');

/**
 * Refactor 2021-07
 * For each scene, scan directory, create nodes for terrain and maps.
 */
main().then(() => {
  console.log('Done!');
  process.exit();
}).catch(err => {
  console.error(err);
  process.exit(1);
});

async function main() {

  await neo4j.init(config.neo4j.url, config.neo4j.user, config.neo4j.password, config.neo4j.database);

  // get available scenes
  const files = await fs.readdir(terrainPath);

  for (const file of files) {

    console.log(file);
    await processScene(file);

  }

}

/**
 * @param scene {string} Scene ID
 * @return {Promise<void>}
 */
async function processScene(scene) {

  const scenePath = path.join(terrainPath, scene);

  // get available maps
  const maps = (await fs.readdir(scenePath, { withFileTypes: true }))
    .filter(entry => entry.isDirectory())
    .map(entry => entry.name);

  console.log(maps);

  // language=Cypher
  const q = `
    MATCH (scene:E53:UH4D {id: $sceneId})
    MERGE (scene)<-[:P121]-(place:E53:UH4D {id: $placeId})<-[:P67]-(terrain:D1:UH4D {id: $terrainId})-[:P106]->(tFile:D9:UH4D {id: $tFile.id})
    SET tFile = $tFile
    WITH scene, terrain, tFile
    UNWIND $maps AS map
    MERGE (scene)<-[:P161]-(e92:E92:UH4D {id: map.e92Id})-[:P160]->(e52:E52:UH4D {id: map.e52Id})-[:P82]->(date:E61:UH4D {id: map.dateId})
    MERGE (e92)<-[:P67]-(img:E38:UH4D {id: map.e38Id})-[:P106]->(imgFile:D9:UH4D {id: map.fileId})
    SET date.value = map.date, imgFile.path = map.path
    RETURN scene, terrain, tFile
  `;

  const params = {
    sceneId: scene,
    placeId: 'e53_terrain_' + scene,
    terrainId: 'd1_terrain_' + scene,
    tFile: {
      id: 'd9_terrain_' + scene,
      path: `terrain/${scene}/`,
      file: 'dgm.gltf',
      type: 'gltf'
    },
    maps: maps.map(year => {
      const mapId = 'map_' + scene + '_' + year;
      return {
        e92Id: 'e92_' + mapId,
        e52Id: 'e52_' + mapId,
        dateId: 'e61_e52_' + mapId,
        date: year,
        e38Id: 'e38_' + mapId,
        fileId: 'd9_' + mapId,
        path: `terrain/${scene}/${year}/`
      };
    })
  };

  await neo4j.writeTransaction(q, params);

}
