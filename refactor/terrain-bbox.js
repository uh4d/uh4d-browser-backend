import * as path from 'path';
import { Box3, Mesh } from 'three';
import * as neo4j from 'neo4j-request';
import { loadGltf } from 'node-three-gltf';
import Coordinates from '../modules/coordinates.js';
import * as config from '../config.js';

/**
 * Refactor 2021-07
 * For each terrain model, compute bounding box and store in database.
 */
main().then(() => {
  console.log('Done!');
  process.exit();
}).catch(err => {
  console.log('Exit with error:');
  console.error(err);
  process.exit(1);
});


async function main() {

  await neo4j.init(config.neo4j.url, config.neo4j.user, config.neo4j.password, config.neo4j.database);

  // query objects
  // language=Cypher
  const q0 = `
    MATCH (scene:E53:UH4D)-[:P1]->(name:E41),
          (scene)<-[:P121]-(:E53)<-[:P67]-(:D1)-[:P106]->(file:D9),
          (scene)-[:P168]->(geo:E94)
    RETURN scene.id AS id,
           name.value AS name,
           apoc.map.removeKey(geo, 'id') AS location,
           apoc.map.removeKey(file, 'id') AS file
  `;

  const scenes = await neo4j.readTransaction(q0);

  // iterate over scene terrains and compute bounding box
  for (const scene of scenes) {

    console.log(scene.id);

    const { min, max } = await computeBbox(scene);

    console.log('west', min.longitude);
    console.log('east', max.longitude);
    console.log('north', min.latitude);
    console.log('south', max.latitude);

    // store bounding box, own E53 node for terrain
    // language=Cypher
    const q1 = `
      MATCH (scene:E53:UH4D {id: $sceneId})<-[:P121]-(place:E53),
            (scene)-[:P168]->(geo)
      MERGE (place)-[:P168]->(bbox:E94:UH4D {id: $bbox.id})
      SET bbox = $bbox
      SET bbox += apoc.map.removeKey(geo, 'id')
      RETURN place
    `;

    const params = {
      sceneId: scene.id,
      placeId: 'e53_terrain_' + scene.id,
      bbox: {
        id: 'e94_e53_terrain_' + scene.id,
        west: min.longitude,
        east: max.longitude,
        north: min.latitude,
        south: max.latitude
      }
    };

    await neo4j.writeTransaction(q1, params);

    console.log('---');

  }

}

async function computeBbox(item) {

  const gltf = await loadGltf(path.join(config.path.data, item.file.path, item.file.file));
  const terrain = gltf.scene.children[0];

  const bbox = new Box3();
  bbox.expandByObject(terrain);

  const origin = new Coordinates(item.location.latitude, item.location.longitude, item.location.altitude);
  const min = origin.clone().add(bbox.min);
  const max = origin.clone().add(bbox.max);

  terrain.traverse(child => {
    if (child instanceof Mesh) {
      child.geometry.dispose();
      child.material.dispose();
    }
  });

  return { min, max };

}
