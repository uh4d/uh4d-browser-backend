import * as neo4j from 'neo4j-request';
import * as fs from 'fs-extra';
import getopt from 'node-getopt';
import * as config from '../config.js';

/**
 * Link all images/objects to a global place node. Set id and offset beforehand.
 */

const opt = getopt.create([
  ['t', 'type=', 'Media type: "image" or "object"'],
  ['s', 'scene=', 'Scene ID, e.g. "dresden", "jena"']
]).bindHelp().parseSystem();

if (opt.options.type !== 'image' && opt.options.type !== 'object') {
  console.warn('No or wrong type! Use `image` or `object` to specify which to link!');
  process.exit(1);
}

if (!opt.options.scene) {
  console.warn('Unset scene ID! Specify which scene should be affected by refactoring process.');
  process.exit(1);
}

main().then(() => {
  console.log('Done!');
  process.exit();
});

async function main() {

  let sceneConfig;
  try {
    sceneConfig = await fs.readJSON(opt.options.scene + '-config.json');
  } catch (e) {
    console.error('Scene config file could not be found!', e);
    process.exit(2);
  }

  await neo4j.init(config.neo4j.url, config.neo4j.user, config.neo4j.password, config.neo4j.database);

  // create global place node with E94 node that contains offset to scene origin

  // language=Cypher
  const qOrigin = `
    MERGE (city:E53:UH4D {id: $scene})
    MERGE (name:E53:UH4D {id: $name.id})
      ON CREATE SET name += $name
    MERGE (origin:E94:UH4D {id: $position.id})
      ON CREATE SET origin += $position
    MERGE (city)-[:P1]->(name)
    MERGE (city)-[:P168]->(origin)
    RETURN city, origin
  `;

  const paramsOrigin = {
    scene: sceneConfig.id,
    name: {
      id: 'e41_' + sceneConfig.id,
      value: sceneConfig.label
    },
    position: {
      id: 'e94_' + sceneConfig.id,
      latitude: sceneConfig.latitude,
      longitude: sceneConfig.longitude,
      altitude: sceneConfig.altitude
    }
  };

  // link all images/objects to this node

  // language=Cypher
  const qLinkImages = `
    MATCH (city:E53:UH4D {id: $scene})
    MATCH (img:E38:UH4D)<-[:P94]-(e65:E65)
    MERGE (e65)-[:P7]->(place:E53:UH4D)
      ON CREATE SET place.id = "e53_" + img.id
    MERGE (place)-[:P89]->(city)
    RETURN place;
  `;

  // language=Cypher
  const qLinkObjects = `
    MATCH (city:E53:UH4D {id: $scene})
    MATCH (e22:E22:UH4D)
    MERGE (e22)<-[:P157]-(place:E53:UH4D)
      ON CREATE SET place.id = "e53_" + e22.id
    MERGE (place)-[:P89]->(city)
  `;

  const params = {
    scene: sceneConfig.id
  };

  // execute queries

  const statements = [
    {statement: qOrigin, parameters: paramsOrigin},
    {statement: opt.options.type === 'image' ? qLinkImages : qLinkObjects, parameters: params}
  ];

  try {
    await neo4j.multipleStatements(statements);
  } catch (err) {
    console.error(err);
  }

}
