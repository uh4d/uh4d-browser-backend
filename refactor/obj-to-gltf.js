import * as path from 'path';
import logger from '../modules/logger.js';
import { processModel } from '../modules/process-model.js';

(async () => {

  if (!process.argv[2]) {
    throw new Error('No path to file provided!');
  }

  const file = path.parse(path.resolve(process.argv[2]));

  const response = await processModel({ path: file.dir, name: file.base })

  logger.info(response);

})()
  .then(() => {
    logger.info('Done!');
    process.exit();
  })
  .catch(err => {
    logger.error(err ? err : 'Something failed!');
    process.exit(3);
  });
