import * as path from 'path';
import * as neo4j from 'neo4j-request';
import { Presets, SingleBar } from 'cli-progress';
import { computeModelCenter } from '../modules/model-center.js';
import Coordinates from '../modules/coordinates.js';
import { computeCoordinatesFromMatrix } from '../modules/utils.js';
import * as config from '../config.js';

/**
 * Refactor 2021-07
 * For each 3D object, compute the center (in WGS-84 coordinates) and store it as `E94 Space Primitive` node.
 */
main().then(() => {
  console.log('Done!');
  process.exit();
}).catch(err => {
  console.error(err);
  process.exit(1);
});

async function main() {

  await neo4j.init(config.neo4j.url, config.neo4j.user, config.neo4j.password, config.neo4j.database);

  // query objects

  // language=Cypher
  const q = `
    MATCH (scene:E53:UH4D)<-[:P89]-(:E53)-[:P157]->(e22:E22)<-[:P67]-(object:D1)-[:P106]->(file:D9),
          (scene)-[:P168]->(sceneGeo:E94)
    RETURN e22.id AS id, object AS origin, file, sceneGeo AS scene`;

  const results = await neo4j.readTransaction(q);

  // iterate over all results
  const progressBar = new SingleBar({}, Presets.shades_classic);
  progressBar.start(results.length, 0);

  for (const item of results) {

    progressBar.increment();

    updateOrigin(item);
    const location = await computeCenter(item);
    await updateDatabase(item, location);

  }

  progressBar.stop();

}

/**
 * @typedef ObjectEntry
 * @property {string} id
 * @property {object} origin
 * @property {number} origin.latitude
 * @property {number} origin.longitude
 * @property {number} origin.altitude
 * @property {number[]} origin.matrix
 * @property {object} file
 * @property {string} file.path
 * @property {string} file.file
 * @property {object} scene
 * @property {number} scene.latitude
 * @property {number} scene.longitude
 * @property {number} scene.altitude
 */

/**
 * Update origin coordinates of 3D model from scene origin and object matrix.
 * @param data {ObjectEntry}
 */
function updateOrigin(data) {

  const origin = new Coordinates(data.scene.latitude, data.scene.longitude, data.scene.altitude);

  const coord = computeCoordinatesFromMatrix(origin, data.origin.matrix);

  Object.assign(data.origin, coord);

}

/**
 * Compute center of 3D model in WGS-84 coordinates.
 * @param data {ObjectEntry}
 * @return {Promise<ILocation>}
 */
async function computeCenter(data) {

  // compute center
  const filePath = path.join(config.path.data, data.file.path, data.file.file);

  const center = await computeModelCenter(filePath, data.origin.matrix);

  // compute center coordinates (WGS-84)
  return new Coordinates(data.origin.latitude, data.origin.longitude, data.origin.altitude)
    .add(center)
    .toJSON();

}

/**
 * Store location values in database.
 * @param data {ObjectEntry}
 * @param location {ILocation}
 * @return {Promise<void>}
 */
async function updateDatabase(data, location) {

  // language=Cypher
  const q = `
    MATCH (e22:E22 {id: $id})<-[:P157]-(place:E53),
          (e22)<-[:P67]-(object:D1)
    MERGE (place)-[:P168]->(geo:E94:UH4D)
    ON CREATE SET geo.id = $geoId
    SET geo += $location
    SET object += $origin
  `

  const params = {
    id: data.id,
    geoId: data.id.replace(/^e22_/, 'e94_'),
    location,
    origin: data.origin
  };

  await neo4j.writeTransaction(q, params);

}
