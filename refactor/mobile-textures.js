import * as path from 'path';
import fs from 'fs-extra';
import { execFile } from 'child-process-promise';
import * as config from '../config.js';

/**
 * Resize all textures of objects to max 1024 pixels for better usage on mobile devices.
 * New file with `_mobile` suffix is generated.
 */
main().then(() => {
  console.log('Done!');
  process.exit();
}).catch(err => {
  console.error(err);
  process.exit(1);
});

async function main() {

  // get object folders
  const folders = await fs.readdir(path.join(config.path.data, 'objects'));

  for (const folder of folders) {

    // get file names
    const files = await fs.readdir(path.join(config.path.data, 'objects', folder));

    // iterate over file names
    for (const file of files) {

      // only look at texture files (jpg, png)
      if (['.jpg', '.png'].includes(path.extname(file))) {

        // omit already resized images
        if (/_mobile\.[^.]+$/.test(file)) { continue; }

        console.log(folder, file);

        // append suffix `_mobile`
        const mobileFile = file.replace(/\.[^.]+$/, '_mobile$&');

        // resize to max 1024 pixels
        await execFile(config.exec.ImageMagick, [path.join(config.path.data, 'objects', folder, file), '-resize', '1024x1024>', path.join(config.path.data, 'objects', folder, mobileFile)]);

      }

    }

  }

}
