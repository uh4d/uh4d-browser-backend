import * as neo4j from 'neo4j-request';
import { updateOSMIntersections } from '../modules/osm-intersection.js';
import { cleanOSMPlaces } from '../modules/cleanup.js';
import * as config from '../config.js';

/**
 * Refactor 2021-07
 * Update OSM intersections for all objects in database.
 */

main().then(() => {
  console.log('Done!');
  process.exit();
}).catch(err => {
  console.log('Exit with error:');
  console.error(err);
  process.exit(1);
});

async function main() {

  await neo4j.init(config.neo4j.url, config.neo4j.user, config.neo4j.password, config.neo4j.database);

  // query objects
  // language=Cypher
  const q = `
    MATCH (e22:E22:UH4D)<-[:P67]-(:D1)
    WHERE NOT (e22)<-[:P157]-(:E53)-[:P121]->(:E53)
    RETURN e22.id AS id
  `;

  const objects = await neo4j.readTransaction(q);

  // iterate over objects and update OSM intersections
  for (let i = 0, l = objects.length; i < l; i++) {

    console.log(`${i + 1} / ${l} : ${objects[i].id}`);

    try {

      const osmIds = await updateOSMIntersections(objects[i].id);
      console.log(osmIds);

    } catch (e) {

      console.warn('Update failed! Continue with next one...');

    }

    await new Promise(resolve => {
      setTimeout(() => { resolve(); }, 5000);
    });

  }

  // cleanup
  console.log('Cleanup...');
  await cleanOSMPlaces();

}
