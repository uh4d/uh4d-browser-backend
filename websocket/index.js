import * as path from 'path';
import shortId from 'shortid';
import fs from 'fs-extra';
import moment from 'moment';
import * as config from '../config.js';
import logger from '../modules/logger.js';

/**
 * @type {Map<string, {[key: string]: any}>}
 */
const sessions = new Map();

/**
 * @param ws {WebSocket}
 * @param req {e.Request}
 */
export function openConnection(ws, req) {

  const id = shortId.generate();

  sessions.set(id, {
    origin: req.headers.origin,
    timestamp: moment().format(),
    log: [],
    requests: [],
    files: [],
    warnings: [],
    errors: []
  });

  ws.on('message', (message) => {
    try {
      handleMessage(id, message);
    } catch (e) {
      ws.send(e.toString());
    }
  });

  ws.on('error', (err) => {
    logger.error(err);
    ws.close(1, err.toString());
  });

  ws.on('close', (code) => {
    closeSession(id, code);
  });

  ws.send(`Connected. Session ID: ${id}`);

}

/**
 * Parse message and store data with respect to type.
 * @param id {string}
 * @param message
 */
function handleMessage(id, message) {

  message = JSON.parse(message);

  if (!message.type || !message.value) {
    throw new Error('Message should be of type { type: string, value: string }');
  }

  if (['timestamp', 'exitCode'].includes(message.type)) {
    throw new Error('Restricted message type: ' + message.type);
  }

  const data = sessions.get(id);
  const timestamp = moment().format('YYYY-MM-DDTHH:mm:ss.SSS');

  switch (message.type) {

    case 'userAgent':
      data.userAgent = JSON.parse(message.value);
      break;

    default:
      if (!data[message.type]) {
        data[message.type] = [];
      }
      data[message.type].push(`[${timestamp}] ${message.value}`);

  }

}

/**
 * Write collected data to JSON file.
 * @param id {string}
 * @param code {number}
 */
function closeSession(id, code) {

  const data = sessions.get(id);
  const timestamp = moment();

  // compose output
  let output = `SessionID: ${id}\n`;
  output += `Origin: ${data.origin}\n`;
  output += `Timestamp: ${data.timestamp}\n`;
  output += `Exit with code ${code} at ${timestamp.format()} after ${timestamp.from(data.timestamp, true)}\n`;

  const userAgent = data.userAgent;

  if (userAgent) {
    output += '\nUserAgent:\n----------\n';
    output += JSON.stringify(userAgent, null, 2);
  }

  delete data.origin;
  delete data.timestamp;
  delete data.userAgent;

  for (const [key, value] of Object.entries(data)) {

    const heading = key.replace(/^(.)/, (match, p1) => p1.toUpperCase()) + ':';
    output += `\n\n${heading}\n${heading.replace(/./g, '-')}\n`;

    output += value.join('\n');

  }

  // compose filename
  const filename = [
    moment(data.timestamp).format('YYYYMMDD-HHmmss-S')
  ];

  if (userAgent) {
    if (userAgent.os.name) {
      filename.push(userAgent.os.name + (userAgent.os.version ? '-' + userAgent.os.version : ''));
    }
    if (userAgent.browser.name) {
      filename.push(userAgent.browser.name + (userAgent.browser.major ? '-' + userAgent.browser.major : ''));
    }
  }

  const filePath = path.join(config.path.logs, filename.join('-') + '.log');

  fs.writeFile(filePath, output);

  sessions.delete(id);

}
