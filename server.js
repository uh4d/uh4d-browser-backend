import morgan from 'morgan';
import express from 'express';
import bodyParser from 'body-parser';
import compression from 'compression';
import errPkg from 'http-errors';
import fs from 'fs-extra';
import * as neo4j from 'neo4j-request';
import logger from './modules/logger.js';
import { scheduleCronJobs } from './cron/index.js';
import * as config from './config.js';
const { NotFound } = errPkg;

// only for debug/localhost
// process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

logger.info('uh4d-browser-backend v0.14.1');

// check if data directory exists
if (!fs.pathExistsSync(config.path.data)) {
  throw new Error('Data directory as specified in config.js does not exist!');
}
// check if logs directory exists
if (!fs.pathExistsSync(config.path.logs)) {
  throw new Error('Logs directory as specified in config.js does not exist!');
}

// init neo4j
neo4j.init(config.neo4j.url, config.neo4j.user, config.neo4j.password, config.neo4j.database);

// express
const app = express();

// app.use(morgan('tiny'));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true }));

app.all('/*', (req, res, next) => {
  // CORS headers
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');

  if (req.method === 'OPTIONS') {
    res.status(200).end();
  }
  else {
    next();
  }
});

// configure main routes
import routes from './routes/index.js';
import { batchFiles } from './routes/batch.js';
import redirect from './routes/redirect.js';
app.post('/data/batch', batchFiles);
app.use('/data', express.static(config.path.data));
app.use('/api/draft', express.static('./docs/draft'));
app.use('/api', express.static('./docs/v0.14.1'));
app.get('/api/redirect', redirect);
app.use('/api', morgan('tiny'), compression(), routes);

// if no route is matched by now, it must be 404
app.use((req, res, next) => {
  next(new NotFound());
});

// error handler
// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err: {};

  const log = `${req.method} ${req.originalUrl} ${err.status || 500}`;
  logger.error(log, '\n', err);

  const response = {
    code: err.name,
    message: err.message,
    route: log
  };

  res.status(err.status || 500);
  res.json(response);
});


// start server
app.set('port', process.env.PORT || config.port || 3001);

const server = app.listen(app.get('port'), () => {
  logger.info('Express server listening on port ' + server.address().port);
});


// cron jobs
scheduleCronJobs();


// init websockets
import { WebSocketServer } from 'ws';
import { openConnection } from './websocket/index.js';

const wss = new WebSocketServer({
  server: server,
  path: '/logs'
});
wss.on('connection', openConnection);


// shutdown routine
process.on('exit', () => {
  server.close();
});

// catch ctrl+c event and exit properly
process.on('SIGINT', function () {
  console.log('Shutdown...');

  // close websocket clients async
  wss.clients.forEach(client => client.close());
  setTimeout(() => {
    process.exit();
  }, 200);
});


// catch uncaught exception and exit properly
process.on('uncaughtException', function (err) {
  logger.error('Uncaught Exception', err);
  process.exit();
});
