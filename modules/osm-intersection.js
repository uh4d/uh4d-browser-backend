import * as path from 'path';
import * as neo4j from 'neo4j-request';
import {
  ExtrudeBufferGeometry,
  Mesh,
  MeshBasicMaterial,
  Path, Raycaster,
  Shape,
  Vector2, Vector3
} from 'three';
import * as BufferGeometryUtils from 'three/examples/jsm/utils/BufferGeometryUtils.js';
import { loadGltf } from 'node-three-gltf';
import Coordinates from './coordinates.js';
import { queryOverpass } from './overpass.js';
import * as config from '../config.js';

/**
 * Determine if object overlaps with any OSM buildings and return their IDs.
 * @param objectId {string}
 * @return {Promise<number[]>}
 */
export async function updateOSMIntersections(objectId) {

  // query object
  // language=Cypher
  const q0 = `
    MATCH (e22:E22:UH4D {id: $id})<-[:P67]-(object:D1)-[:P106]->(file:D9),
          (e22)<-[:P157]-(place:E53)-[:P168]->(geo:E94)
    RETURN e22.id AS id, object AS origin, file, geo AS location
  `;

  const item = (await neo4j.readTransaction(q0, { id: objectId }))[0];

  if (!item) {
    throw new Error(`Cannot find object with ID ${objectId}`);
  }

  // load gltf file
  const gltf = await loadGltf(path.join(config.path.data, item.file.path, item.file.file));

  // merge geometries
  const geometries = [];
  gltf.scene.traverse(child => {
    if (child instanceof Mesh) {
      geometries.push(child.geometry);
    }
  });

  const geo = BufferGeometryUtils.mergeBufferGeometries(geometries);
  const mesh = new Mesh(geo, new MeshBasicMaterial());
  mesh.rotation.set(item.origin.omega, item.origin.phi, item.origin.kappa, 'YXZ');
  geo.computeBoundingSphere();

  // query Overpass API
  const result = await queryOverpass(item.location.latitude, item.location.longitude, geo.boundingSphere.radius);
  const origin = new Coordinates(item.origin.latitude, item.origin.longitude, item.origin.altitude);

  // generate OSM buildings
  const buildings = processOSMData(result, origin);

  // test object and OSM buildings agains each other
  const osmIds = [];

  for (const building of buildings) {
    // test intersection
    if (testIntersection(mesh, building) || testIntersection(building, mesh)) {
      if (!osmIds.includes(building.userData.osmId)) {
        osmIds.push(building.userData.osmId);
      }
    }

    // dispose OSM mesh
    building.geometry.dispose();
    building.material.dispose();
  }

  // dispose object mesh
  mesh.geometry.dispose();
  mesh.material.dispose();

  // update database
  // language=Cypher
  const q1 = `
    MATCH (e22:E22:UH4D {id: $id})<-[:P157]-(place:E53)
    OPTIONAL MATCH (place)-[r:P121]->(:E53)
    DELETE r
    WITH place
    UNWIND $osmIds AS osmId
    MERGE (osmPlace:E53:UH4D {id: 'e53_osm_' + toInteger(osmId), osm: toInteger(osmId)})
    MERGE (place)-[:P121]->(osmPlace)
  `;

  await neo4j.writeTransaction(q1, { id: objectId, osmIds });

  return osmIds;

}

/**
 * @param mesh1 {Mesh<BufferGeometry>}
 * @param mesh2 {Mesh<BufferGeometry>}
 * @return {boolean}
 */
function testIntersection(mesh1, mesh2) {

  const posAttr = mesh1.geometry.getAttribute('position');

  for (let i = 0; i < posAttr.count; i++) {

    const pos = mesh1.localToWorld(new Vector3(posAttr.getX(i), 10000, posAttr.getZ(i)));
    const raycaster = new Raycaster(pos, new Vector3(0, -1, 0));
    const intersections = raycaster.intersectObject(mesh2, true);

    if (intersections[0]) {
      return true;
    }

  }

  return false;

}

/**
 * @param result
 * @param origin {Coordinates}
 * @return {Mesh[]}
 */
function processOSMData(result, origin) {

  const nodes = result.elements.filter(e => e.type === 'node');
  const ways = result.elements.filter(e => e.type === 'way');
  const relations = result.elements.filter(e => e.type === 'relation');

  const buildings = [];

  const processWay = (way, role) => {

    const points = [];

    way.nodes.forEach(nodeId => {
      const node = nodes.find(n => n.id === nodeId);
      if (node) {
        const p = new Coordinates(node.lat, node.lon, 0).toCartesian(origin);
        points.push(new Vector2(p.x, -p.z));
      }
    });

    switch (role) {
      case 'outer':
        return new Shape(points);
      case 'inner':
        return new Path(points);
      default:
        return new Shape(points);
    }

  };

  ways.forEach(way => {

    if (!way.tags) { return; }

    const shape = processWay(way, 'part');
    const mesh = generateBuilding(shape);
    mesh.userData.osmId = way.id;
    buildings.push(mesh);

  });

  relations.forEach(rel => {

    rel.members.sort((a) => {
      if (a.role === 'outer') { return -1; }
      if (a.role === 'inner') { return 1; }
      return 0;
    });

    const wayPaths = [];
    rel.members.forEach(m => {
      const way = ways.find(w => w.id === m.ref);
      if (way) {
        wayPaths.push(processWay(way, m.role));
      }
    });

    const shapes = [];
    wayPaths.forEach(p => {
      if (p instanceof Shape) {
        shapes.push(p);
      } else {
        shapes[shapes.length - 1].holes.push(p);
      }
    });

    shapes.forEach(s => {
      const mesh = generateBuilding(s);
      mesh.userData.osmId = rel.id;
      buildings.push(mesh);
    });

  });

  return buildings;

}

/**
 * @param shape {Shape}
 * @return {Mesh<ExtrudeBufferGeometry, MeshBasicMaterial>}
 */
function generateBuilding(shape) {

  const geo = new ExtrudeBufferGeometry(shape, {
    depth: 1,
    bevelEnabled: false
  });

  geo.rotateX(-Math.PI / 2);

  return new Mesh(geo, new MeshBasicMaterial());

}
