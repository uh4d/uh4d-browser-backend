import fs from 'fs-extra';
import { execFile } from 'child-process-promise';
import { v4 as uuid } from 'uuid';
import * as pathUtils from 'path';
import logger from './logger.js';
import * as config from '../config.js';

/**
 * Process image: copy to data folder, create thumbnails, determine dimensions, etc.
 * @param file {string}
 * @param outputDir {string}
 * @return {Promise<{preview: string, path: string, original: string, thumb: string, width: number, type: string, height: number}>}
 */
export async function processImage(file, outputDir) {

  const shortPath = `images/${uuid()}/`;
  const path = pathUtils.join(outputDir, shortPath);
  const filename = pathUtils.basename(file);
  const filenameThumb = filename.slice(0, filename.lastIndexOf('.')) + '_thumb.jpg';
  const filenamePreview = filename.slice(0, filename.lastIndexOf('.')) + '_preview.jpg';
  const filenameTiny = filename.slice(0, filename.lastIndexOf('.')) + '_tiny.jpg';

  logger.debug('Directory:', shortPath, 'File:', filename);

  try {

    // get image size
    const result = await execFile(config.exec.ImageIdentify, [file]);
    const matches = result.stdout.match(/\s(\d+)x(\d+)\s/);
    const width = +matches[1];
    const height = +matches[2];

    // create directory
    await fs.ensureDir(pathUtils.join(outputDir, 'images'));

    // copy image into directory
    await fs.copy(file, path + filename);

    // create thumbnail
    await execFile(config.exec.ImageMagick, [path + filename, '-resize', '200x200>', path + filenameThumb]);

    // down-sample preview image
    await execFile(config.exec.ImageMagick, [path + filename, '-resize', '2048x2048>', path + filenamePreview]);

    // create tiny thumbnail (for 3D preview)
    await execFile(config.exec.ImageMagick, [path + filename, '-resize', '64x64!', path + filenameTiny]);

    return {
      path: shortPath,
      original: filename,
      type: filename.split('.').pop().toLowerCase(),
      preview: filenamePreview,
      thumb: filenameThumb,
      tiny: filenameTiny,
      width,
      height
    };

  } catch (err) {

    logger.warn('Unlink ' + path);
    await fs.remove(path);

    throw err;

  }

}

export default processImage;
