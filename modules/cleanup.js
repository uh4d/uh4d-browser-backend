import * as neo4j from 'neo4j-request';

/**
 * Delete OSM place nodes that are not connected anymore.
 * @return {Promise<void>}
 */
export async function cleanOSMPlaces() {

  // language=Cypher
  const q = `
    MATCH (place:E53:UH4D)
    WHERE exists(place.osm)
    AND NOT (place)<-[:P121]-()
    DELETE place
  `;

  await neo4j.writeTransaction(q);

}
