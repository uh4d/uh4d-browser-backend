import * as utm from 'utm';
import { Vector2, Vector3 } from 'three';

/**
 * @typedef ILocation
 * @property {number} latitude
 * @property {number} longitude
 * @property {number} altitude
 */

/**
 * Class to operate with WGS-84 coordinates.
 */
export default class Coordinates {

  /**
   * @param latitude {number}
   * @param longitude {number}
   * @param altitude {number}
   */
  constructor(latitude = 0, longitude = 0, altitude = 0) {

    this.latitude = latitude;
    this.longitude = longitude;
    this.altitude = altitude;

  }

  /**
   * Return as Object.
   * @return {ILocation}
   */
  toJSON() {

    return {
      latitude: this.latitude,
      longitude: this.longitude,
      altitude: this.altitude
    };

  }

  /**
   * Set from ILocation object.
   * @param json {ILocation}
   */
  fromJSON(json) {

    this.latitude = json.latitude || 0;
    this.longitude = json.longitude || 0;
    this.altitude = json.altitude || 0;

  }

  /**
   * Clone coordinates.
   * @return {Coordinates}
   */
  clone() {

    return new Coordinates(this.latitude, this.longitude, this.altitude);

  }

  /**
   * Copy values from coordinates.
   * @param coords {Coordinates}
   */
  copy(coords) {

    this.latitude = coords.latitude;
    this.longitude = coords.longitude;
    this.altitude = coords.altitude;

  }

  /**
   * Add Cartesian position in meters.
   * @param position {Vector3}
   * @return {this}
   */
  add(position) {

    const { easting, northing, zoneNum, zoneLetter } = utm.fromLatLon(this.latitude, this.longitude);

    const translation = new Vector3(easting, this.altitude, northing)
      .multiply(new Vector3(1, 1, -1))
      .add(position)
      .multiply(new Vector3(1, 1, -1));

    const { latitude, longitude } = utm.toLatLon(translation.x, translation.z, zoneNum, zoneLetter);

    this.latitude = latitude;
    this.longitude = longitude;
    this.altitude = translation.y;

    return this;

  }

  /**
   * Compute WGS-84 bounding box (west, east, north, south) from this point (lat, lon) and radius.
   * @param radius {number}
   * @return {{west: number, east: number, north: number, south: number}}
   */
  computeBoundingBox(radius) {

    const { easting, northing, zoneNum, zoneLetter } = utm.fromLatLon(this.latitude, this.longitude);

    const position = new Vector2(easting, northing);

    const v1 = new Vector2(-radius, -radius).add(position);
    const v2 = new Vector2(radius, radius).add(position);

    const { latitude: south, longitude: west } = utm.toLatLon(v1.x, v1.y, zoneNum, zoneLetter);
    const { latitude: north, longitude: east } = utm.toLatLon(v2.x, v2.y, zoneNum, zoneLetter);

    return { west, east, north, south };

  }

  /**
   * Compute Cartesian coordinates relative to coordinates that are the origin of the local Cartesian space.
   * @param origin {Coordinates}
   * @return {Vector3}
   */
  toCartesian(origin) {

    const originUtm = utm.fromLatLon(origin.latitude, origin.longitude);
    const originPosition = new Vector3(originUtm.easting, origin.altitude, originUtm.northing);

    const { easting, northing } = utm.fromLatLon(this.latitude, this.longitude, originUtm.zoneNum);
    const position = new Vector3(easting, this.altitude, northing);

    return position.sub(originPosition).multiply(new Vector3(1, 1, -1));

  }

}
