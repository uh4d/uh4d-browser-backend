import log4js from 'log4js';

log4js.configure({
  appenders: {
    out: { type: 'console' }
  },
  categories: {
    default: {
      appenders: ['out'],
      level: 'all'
    }
  }
});

process.on('exit', function () {
  log4js.shutdown();
});

export default log4js.getLogger('UH4D');
