import * as path from 'path';
import fs from 'fs-extra';
import * as xmljs from 'xml-js';
import { execFile } from 'child-process-promise';
import { EdgesGeometry } from 'three';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader.js';
import * as BufferGeometryUtils from 'three/examples/jsm/utils/BufferGeometryUtils.js';
import logger from './logger.js';
import * as config from '../config.js';

/**
 * Convert .obj to .gltf
 * @param file {{path: string, name: string}}
 * @return {Promise<{name: string, path: string, original: string, gltf: string, type: string}>}
 */
export async function processModel(file) {

  const objFile = path.join(file.path, file.name);
  const basename = path.basename(file.name, '.obj');
  const daeRawFile = path.join(file.path, 'temp_raw.dae');
  const daeEdgesFile = path.join(file.path, 'temp_edges.dae');
  const gltfFile = path.join(file.path, basename + '.gltf');

  // check if file exists
  const exists = await fs.pathExists(objFile);
  if (!exists) {
    throw new Error(`Path does not exist: ${objFile}`);
  }

  //  check for correct file type
  if (path.extname(file.name) !== '.obj') {
    throw new Error(`File format not supported: ${path.extname(file.name)}`);
  }

  logger.info('Processing:', file.name);

  // convert OBJ to COLLADA
  await execFile(config.exec.Assimp, ['export', objFile, daeRawFile, '-jiv', '-om', '-og']);

  // read COLLADA file
  const daeRaw = await fs.readFile(daeRawFile);
  const dae = xmljs.xml2js(daeRaw, { compact: true });

  const daeGeo = dae.COLLADA.library_geometries;
  const daeScene = dae.COLLADA.library_visual_scenes.visual_scene;

  if (!Array.isArray(daeGeo.geometry)) {
    daeGeo.geometry = [daeGeo.geometry]
  }

  if (!Array.isArray(daeScene.node)) {
    daeScene.node = [daeScene.node]
  }

  // load obj
  const objRaw = await fs.readFile(objFile, { encoding: 'utf8' });
  const obj = new OBJLoader().parse(objRaw);

  // generate edges
  const edgesGeometries = [];

  obj.children.forEach((child) => {
    if (child.isMesh) {
      const geo = new EdgesGeometry(child.geometry, 24.0);
      edgesGeometries.push(geo);
    }
  });

  const geoCombined = BufferGeometryUtils.mergeBufferGeometries(edgesGeometries);
  geoCombined.name = 'edges';

  // add edges to collada
  daeGeo.geometry.push(geo2dae(geoCombined));

  daeScene.node.push({
    _attributes: {
      id: geoCombined.name,
    },
    matrix: {
      _text: '1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1'
    },
    instance_geometry: {
      _attributes: {
        url: '#' + geoCombined.name + '_geometry'
      },
      bind_material: {
        technique_common: {
          instance_material: {
            _attributes: {
              symbol: 'defaultMaterial',
              target: '#DefaultMaterial'
            }
          }
        }
      }
    }
  });

  // write DAE file
  await fs.writeFile(daeEdgesFile, xmljs.js2xml(dae, { spaces: 2, compact: true }));

  // convert DAE to GLTF
  await execFile(config.exec.Collada2Gltf, ['-i', daeEdgesFile, '-o', gltfFile, '-d', '-t']);

  // remove temp files
  await fs.remove(daeRawFile);
  await fs.remove(daeEdgesFile);

  return {
    name: basename,
    path: file.path,
    original: file.name,
    gltf: basename + '.gltf',
    type: 'gltf'
  };

}

/**
 * Generate COLLADA structure for geometry instance.
 * @param bufferGeo {BufferGeometry}
 */
function geo2dae(bufferGeo) {

  const posAttr = bufferGeo.attributes.position;

  const indexArray = [];
  for (let i = 0, l = posAttr.count; i < l; i++) {
    indexArray.push(i);
  }

  return {
    _attributes: {
      id: bufferGeo.name + '_geometry',
    },
    mesh: {
      source: {
        _attributes: {
          id: bufferGeo.name + '_positions'
        },
        float_array: {
          _attributes: {
            count: posAttr.array.length,
            id: bufferGeo.name + '_positions_array'
          },
          _text: posAttr.array.join(' ')
        },
        technique_common: {
          accessor: {
            _attributes: {
              count: posAttr.count,
              source: '#' + bufferGeo.name + '_positions_array',
              stride: posAttr.itemSize
            }
          },
          param: [
            { _attributes: { name: 'X', type: 'float' }},
            { _attributes: { name: 'Y', type: 'float' }},
            { _attributes: { name: 'Z', type: 'float' }}
          ]
        }
      },
      vertices: {
        _attributes: {
          id: bufferGeo.name + '_vertices'
        },
        input: {
          _attributes: {
            semantic: 'POSITION',
            source: '#' + bufferGeo.name + '_positions'
          }
        }
      },
      lines: {
        _attributes: {
          material: 'defaultMaterial',
          count: posAttr.count / 2
        },
        input: {
          _attributes: {
            offset: 0,
            semantic: 'VERTEX',
            source: '#' + bufferGeo.name + '_vertices'
          }
        },
        p: {
          _text: indexArray.join(' ')
        }
      }
    }
  };

}
