import * as utm from 'utm';
import { Euler, Matrix4, Vector3 } from 'three';
import moment from 'moment';
import { concat, map, of, repeat, zip } from 'rxjs';
import { delay } from 'rxjs/operators';

/**
 * Replace unusual characters with `_`.
 * @param str {string}
 * @return {string}
 */
export function replace(str) {

  if (typeof str !== 'string') {
    return str;
  }

  return str.replace(/[^a-zA-Z0-9_\-.]/g, '_');

}

/**
 * Replace special RegExp characters.
 * @param str {string}
 * @return {string}
 */
export function escapeRegex(str) {

  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#escaping
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

}

/**
 * Check if all properties are set in the object/map.
 * @param map {{[key: string]: any}} Object/map to check against.
 * @param propertyList {string[]|string[][]} List of property names. If a 2-dimensional list is provided, it will return `true` if at least one of these list passes the checks.
 * @return {boolean}
 */
export function checkRequiredProperties(map, propertyList) {

  // check if map is object
  if (typeof map.hasOwnProperty !== 'function') { return false; }

  // check if any property names provided
  if (!propertyList[0]) { return true; }

  // check if 2-dimensional array
  const list = Array.isArray(propertyList[0]) ? propertyList : [propertyList];

  return list.some(props => {

    for (let i = 0; i < props.length; i++) {

      // check if property is set
      if (!Object.prototype.hasOwnProperty.call(map, props[i])) {
        return false;
      }

      const prop = map[props[i]];

      // check if undefined or length of string is 0
      if (typeof prop === 'undefined' || typeof prop === 'string' && prop.length === 0) {
        return false;
      }
    }

    return true;

  });

}

/**
 * Check if all passed dates match the provided pattern and are valid dates.
 * `null` is considered as true.
 * @param dates {string|string[]} Date as string or array of strings
 * @param pattern {RegExp=} Regular expression defining date format
 * @return {boolean}
 */
export function checkDateFormat(dates, pattern = /^\d{4}-\d{2}-\d{2}$/) {

  dates = Array.isArray(dates) ? dates : [dates];

  return !dates.some(d => d && !(pattern.test(d) && moment(d).isValid()));

}

/**
 * Compute global WGS-84 coordinates from matrix with respect to scene origin coordinates.
 * @param origin {Coordinates}
 * @param matrix {number[]}
 * @return {{latitude: number, longitude: number, altitude: number, omega: number, phi: number, kappa: number}}
 */
export function computeCoordinatesFromMatrix(origin, matrix) {

  if (!Array.isArray(matrix) || matrix.length !== 16) {
    throw new Error('Wrong matrix format!');
  }

  const { easting, northing, zoneNum } = utm.fromLatLon(origin.latitude, origin.longitude);
  const utmOffset = new Vector3(easting, origin.altitude, northing);
  const mat4 = new Matrix4().fromArray(matrix);
  const translation = new Vector3().setFromMatrixPosition(mat4);
  const rotation = new Euler().setFromRotationMatrix(mat4, 'YXZ');
  const utmPosition = translation.multiply(new Vector3(1, 1, -1)).add(utmOffset);
  const { latitude, longitude } = utm.toLatLon(utmPosition.x, utmPosition.z, zoneNum, undefined, true);

  return {
    latitude,
    longitude,
    altitude: utmPosition.y,
    omega: rotation.x,
    phi: rotation.y,
    kappa: rotation.z
  };

}

/**
 *  Emits up to `limit` items from the source Observable within a rolling window
 *  of `windowMsec`. If the source observable emits items faster than the rate
 *  limit allows, they are delayed until the time window no longer contains
 *  `limit` previously emitted items.
 * @param limit {number} The maximum number of items to emit within the given time window
 * @param window {number} The duration of time to emit no more than `limit` items within
 */
export function rateLimit(limit, window) {

  return (source) => {

    // start with the first `limit` number of items already emittable
    const initialRelease = of({}).pipe(repeat(limit));
    // release another item `windowMsec` milliseconds after one has been consumed
    const delayedRelease = source.pipe(delay((window)));

    const release = concat(initialRelease, delayedRelease);

    return zip(source, release).pipe(map(([a]) => a));

  };

}
