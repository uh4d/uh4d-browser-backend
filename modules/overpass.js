import got from 'got';
import { lastValueFrom, Observable, throwError } from 'rxjs';
import { concatWith, delay, retryWhen, take, tap } from 'rxjs/operators';
import Coordinates from './coordinates.js';

/**
 * Query OSM Overpass API for ways/relations of type `building`.
 * @param latitude {number}
 * @param longitude {number}
 * @param radius {number}
 * @return {Promise<*>}
 */
export async function queryOverpass(latitude, longitude, radius) {

  const { west, east, south, north } = new Coordinates(latitude, longitude).computeBoundingBox(radius);

  // bbox south, west, north, east
  const body = `[out:json][timeout:25];
(
  way["building"](${south},${west},${north},${east});
  relation["building"](${south},${west},${north},${east});
);
out body;
>;
out skel qt;`;

  // Overpass API sometimes errors with Timeout
  // -> retry 5 times with a small delay
  const observable = new Observable((subscriber) => {

    got.post('https://overpass-api.de/api/interpreter', {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      },
      body
    }).then(response => {

      subscriber.next(JSON.parse(response.body))
      subscriber.complete();

    }).catch(error => {

      console.warn(error.toString());
      subscriber.error(error);

    });

  }).pipe(
    retryWhen(errors => errors.pipe(
      tap(() => console.log('Overpass API: Retry in 1 second...')),
      delay(1000),
      take(5),
      concatWith(throwError(() => new Error('Overpass API was not accessible after 5 tries.')))
    ))
  );

  return await lastValueFrom(observable);

}
