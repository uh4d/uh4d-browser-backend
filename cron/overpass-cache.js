import path from 'path';
import { fileURLToPath } from 'url';
import fs from 'fs-extra';
import moment from 'moment';
import logger from '../modules/logger.js';

const dirname = path.dirname(fileURLToPath(import.meta.url));

/**
 * Check cached Overpass files if they are older than one month.
 * If true, discard them, otherwise keep them.
 */
export async function cleanOverpassCache() {

  logger.info('Cron Job: Cleaning Overpass cache from old cache files...');

  const dirPath = path.join(dirname, '..', '.cache/overpass');

  const exists = await fs.pathExists(dirPath);
  if (!exists) {
    logger.warn('Overpass cache directory does not exist. cleanOverpassCache() terminates without')
    return;
  }

  const files = await fs.readdir(dirPath);

  // time one month ago
  const time = moment().subtract(1, 'months');

  for (const file of files) {

    const filePath = path.join(dirPath, file);

    try {

      const stat = await fs.stat(filePath);

      // remove cache file if older than one month
      if (moment(stat.mtime).isBefore(time)) {
        await fs.remove(filePath);
      }

    } catch (e) {

      logger.error(`Error while accessing ${filePath}!`, e);

    }

  }

}
