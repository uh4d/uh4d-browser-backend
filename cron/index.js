import Cron from 'croner';
import { cleanOverpassCache } from './overpass-cache.js';

/**
 * Schedule cron jobs.
 */
export function scheduleCronJobs() {

  // run once a month at 03:00 am of the first day of the month
  Cron('0 3 1 * *', cleanOverpassCache);

}
