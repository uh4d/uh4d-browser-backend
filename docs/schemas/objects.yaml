Object:
  description: Object data
  type: object
  allOf:
    - $ref: '#/BaseData'
    - $ref: '#/AdditionalData'

Objects:
  description: List of objects
  type: array
  items:
    $ref: '#/BaseData'

BaseData:
  type: object
  properties:
    id:
      description: Object ID
      type: string
      format: uuid
    name:
      description: Object appellation
      type: string
      example: Fernmeldeamt
    date:
      description: Time-span of object
      type: object
      properties:
        from:
          description: Date of erection
          type: string
          format: date
          nullable: true
          example: 1981-01-01
        to:
          description: Date of destruction
          type: string
          format: date
          nullable: true
          example: 2018-11-30
    origin:
      description: Origin of 3D object's local coordinate system. Used to place the object in the 3D scene.
      type: object
      allOf:
        - $ref: ./location.yaml#/Position
        - $ref: ./location.yaml#/Rotation
        - type: object
          properties:
            matrix:
              description: Transformation matrix (column-major), refers to scene's local coordinate system (deprecated)
              type: array
              items:
                type: number
                format: format
              minItems: 16
              maxItems: 16
              example: [0.9619390774307525, 0, -0.2732639138418933, 0, 0, 0.9999999776482582, 0, 0, 0.2732639138418933, 0, 0.9619390774307525, 0, 66.78174109184238, 6, -346.1963437562447, 1]
    location:
      description: Geographic position of the center of the object. `altitude` refers to the lowest point of the geometry. Used to query object by `lat`, `lon`, `r`.
      type: object
      allOf:
        - $ref: ./location.yaml#/Position
    file:
      $ref: '#/File'
    vrcity_forceTextures:
      description: Flag that signals that application should also load object's textures on object load (only relevant to VRCity app)
      type: boolean
      nullable: true
      example: false
    osm_overlap:
      description: List of OSM ids of ways/relations that overlap with 3D object
      type: array
      items:
        type: integer

AdditionalData:
  type: object
  properties:
    misc:
      description: Miscellaneous metadata (may be formatted with Markdown syntax)
      type: string
      nullable: true
      example: Link to Wikipedia
    address:
      description: Current address
      type: string
      nullable: true
      example: Theaterstraße 2, 01067 Dresden
    formerAddresses:
      description: Former names of address
      type: array
      items:
        type: string
    tags:
      $ref: ./tags.yaml

File:
  description: File reference information
  type: object
  properties:
    path:
      description: Path to files
      type: string
      example: objects/15870ab9-782b-4ce6-b4b8-bc0f0d48cda3/
    file:
      description: 3D object file
      type: string
      example: fernmeldeamt.gltf
    original:
      description: Original uploaded file
      type: string
      example: fernmeldeamt.obj
    type:
      description: File format
      type: string
      example: gltf
