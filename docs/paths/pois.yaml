get:
  tags: [Points of Interest]
  summary: Query points of interest
  description: |
    Query all points of interest. Query parameters may be set to filter the data.

     `lat`, `lon`, and `r` works only in combination and may be used to look for points of interest within a radius at a position.

    `type` specifies the source API/database the points of interests should be queried from (provided respective retrieval script).
    If omitted, only UH4D points of interest will be queried (equals `type=uh4d`).
    Multiple types are possible, e.g., `?type=uh4d&type=wikidata`.
  parameters:
    - name: date
      description: Filter by date
      in: query
      schema:
        type: string
        format: date
    - $ref: ../parameters.yaml#/QLatitude
    - $ref: ../parameters.yaml#/QLongitude
    - $ref: ../parameters.yaml#/QRadius
    - $ref: ../parameters.yaml#/QScene
    - name: type
      description: |
        Type of sources points of interest should be queried from. Default: `uh4d`
      in: query
      schema:
        anyOf:
          - type: string
          - type: array
            items:
              type: string
  responses:
    '200':
      description: List of points of interest.
      content:
        application/json:
          schema:
            $ref: ../schemas/pois.yaml#/Pois
    '500':
      $ref: ../responses.yaml#/InternalServerError

post:
  tags: [Points of Interest]
  summary: Save point of interest
  description: Save new point of interest at position.
  requestBody:
    content:
      application/json:
        schema:
          type: object
          allOf:
            - $ref: ../schemas/location.yaml#/Position
            - type: object
              properties:
                title:
                  $ref: ../schemas/pois.yaml#/BasePoi/properties/title
                content:
                  $ref: ../schemas/pois.yaml#/BasePoi/properties/content
                date:
                  $ref: ../schemas/pois.yaml#/Date
                objectId:
                  description: ID of object the point of interest is attached to
                  type: string
                  nullable: true
                  example: e22_JPRL_Zvova_st_michael_kirche
                scene:
                  description: ID of scene the POI should be attached to
                  type: string
                damageFactors:
                  description: List of damage factors according to preservation order
                  type: array
                  items:
                    type: string
                  nullable: true
                  example: [ humidity, vandalism, hazardous-substances ]
          required:
            - title
            - content
            - latitude
            - longitude
            - altitude
            - date
  responses:
    '200':
      description: Saved point of interest.
      content:
        application/json:
          schema:
            $ref: ../schemas/pois.yaml#/Uh4dPoi
    '400':
      $ref: ../responses.yaml#/BadRequest
    '500':
      $ref: ../responses.yaml#/InternalServerError
