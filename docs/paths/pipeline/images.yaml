get:
  tags: [Pipeline]
  summary: Query images
  description: |
    Query images with Cartesian coordinates relative to origin.
    The origin is either the average position of all queried images with spatial coordinates or, if `scene` and `forceSceneOrigin=1` are set, the scene's origin.
    
    Query parameters may be set to filter the data.
    `lat`, `lon`, and `r` works only in combination and may be used to look for images within a radius at a position.
  parameters:
    - $ref: ../parameters.yaml#/QScene
    - name: q
      description: |
        Query string to filter results (looks for title, author, owner, tags).
        A simple dash `-` will exclude query from results.
        Special queries are `spatial:set` and `spatial:unset` to look for images that are already spatialized or not yet spatialized, respectively.
      in: query
      schema:
        anyOf:
          - type: string
          - type: array
            items:
              type: string
    - name: from
      description: Time-span minimum value
      in: query
      schema:
        type: string
        format: date
    - name: to
      description: Time-span maximum value
      in: query
      schema:
        type: string
        format: date
    - $ref: ../../parameters.yaml#/QLatitude
    - $ref: ../../parameters.yaml#/QLongitude
    - $ref: ../../parameters.yaml#/QRadius
    - name: forceSceneOrigin
      description: Only works in combination with `scene`. If set to `1`, the origin will be set to the scene's origin, i.e. positions will not be averaged.
      in: query
      scheme:
        type: number
  responses:
    '200':
      description: List of images relativ to origin.
      content:
        application/json:
          schema:
            $ref: ../../schemas/pipeline/images.yaml#/ImagesResponse
    '500':
      $ref: ../../responses.yaml#/InternalServerError

post:
  tags: [Pipeline]
  summary: Save poses
  description: Save computed poses for a set of images.
  requestBody:
    content:
      application/json:
        schema:
          $ref: ../../schemas/pipeline/images.yaml#/ImagesRequest
  responses:
    '200':
      description: Success.
    '400':
      $ref: ../../responses.yaml#/BadRequest
    '404':
      $ref: ../../responses.yaml#/NotFound
    '500':
      $ref: ../../responses.yaml#/InternalServerError
