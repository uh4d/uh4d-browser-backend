# API Documentation

Documentation according to __[OpenAPI Specification](http://spec.openapis.org/oas/v3.0.2)__

Validate OpenAPI files

    npm install -g swagger-cli

    swagger-cli validate openapi.yaml
    
    swagger-cli bundle -o bundle.yaml -t yaml openapi.yaml

Generate API Reference website ([ReDoc](https://github.com/Rebilly/ReDoc))

    npm install -g redoc

    redoc-cli server -w openapi.yaml

    redoc-cli bundle -t template.hbs -o index.html openapi.yaml

Or execute Grunt task (see `Gruntfile.cjs` for details)

    npm run docs

Browse `http://localhost:3001/api` to see generated output.
