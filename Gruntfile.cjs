/**
 * @param grunt {IGrunt}
 */
module.exports = function (grunt) {

  // load plugins
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-exec');
  grunt.loadNpmTasks('grunt-version');

  // configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    clean: {
      docs: ['docs/v<%= pkg.version %>'],
      draft: ['docs/draft']
    },

    copy: {
      docs: {
        expand: true,
        cwd: 'docs/',
        src: ['assets/*', 'descriptions/*.{jpg,png,svg}'],
        flatten: true,
        dest: 'docs/v<%= pkg.version %>'
      },
      draft: {
        expand: true,
        cwd: 'docs/',
        src: ['assets/*', 'descriptions/*.{jpg,png,svg}'],
        flatten: true,
        dest: 'docs/draft'
      }
    },

    exec: {
      docs: {
        cwd: 'docs/',
        cmd: 'npx redoc-cli build -t template.hbs -o v<%= pkg.version %>/index.html openapi.yaml'
      },
      draft: {
        cwd: 'docs/',
        cmd: 'npx redoc-cli build -t template.hbs -o draft/index.html openapi.yaml'
      }
    },

    version: {
      startLog: {
        options: {
          prefix: 'logger\\.info\\(\'uh4d-browser-backend\\sv'
        },
        src: ['server.js']
      },
      docsRoute: {
        options: {
          prefix: 'express\\.static\\(\'\\./docs/v'
        },
        src: ['server.js']
      },
      openapi: {
        options: {
          prefix: 'version:\\s'
        },
        src: ['docs/openapi.yaml']
      },
      packages: {
        src: ['package.json']
      }
    }
  });

  // tasks
  grunt.registerTask('docs', 'Generating docs', (arg1) => {

    let target = 'draft';

    if (arg1 === 'release') {
      target = 'docs';
      grunt.log.writeln('Generating docs to docs/v' + grunt.config.get('pkg').version);
    } else {
      grunt.log.writeln('Generating docs to docs/draft');
    }

    grunt.task.run('clean:' + target, 'copy:' + target, 'exec:' + target);

  });

  grunt.registerTask('update-pkg', 'Refresh pkg from package.json', () => {

    grunt.config.set('pkg', grunt.file.readJSON('package.json'));

  });

  grunt.registerTask('bump-version', 'Bump version, update files, and generate docs', arg1 => {

    if (!arg1) {

      grunt.log.errorlns('No semver release type specified!');
      grunt.log.errorlns('Available release types: major, minor, patch, prerelease or, e.g., 0.1.0');
      return false;

    } else if (['major', 'minor', 'patch', 'prerelease'].indexOf(arg1) === -1 && !/\d+\.\d+\.\d+/.test(arg1)) {

      grunt.log.errorlns('Wrong semver release type!');
      grunt.log.errorlns('Available release types: major, minor, patch, prerelease or, e.g., 0.1.0');
      return false;

    }

    grunt.task.run('clean:docs', 'version::' + arg1, 'update-pkg', 'docs:release');

  });

};
