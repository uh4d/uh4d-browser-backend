import nodeGetopt from 'node-getopt';
import * as path from 'path';
import fs from 'fs-extra';
import FormData from 'form-data';
import got from 'got';
import logger from '../modules/logger.js';

const getopt = nodeGetopt.create([
  ['d', 'dir=', 'Directory with pkl files'],
  ['', 'host=', 'UH4D API host url, e.g., https://4dbrowser.urbanhistory4d.org'],
  ['h', 'help']
])
  .bindHelp(`
Usage: node pkl-files.js [OPTION]

[[OPTIONS]]

`);
const opt = getopt.parseSystem();

if (!opt.options.host) {
  logger.warn('No host set! Specify the url of the API host.');
  getopt.showHelp();
  process.exit(1);
}

if (!opt.options.dir) {
  logger.warn('No directory set! Specify path to directory with pkl files.');
  getopt.showHelp();
  process.exit(2);
}

(async () => {

  // check if dir exists
  const exists = await fs.pathExists(opt.options.dir);
  if (!exists) {
    throw new Error(`Could not resolve directory path ${opt.options.dir}`);
  }

  // resolve host url
  const apiUrl = opt.options.host.replace(/\/(?:api\/?)?$/, '') + '/api/';

  // check if it is UH4D API
  const version = await got(apiUrl + 'version').json();
  if (!/^UH4D\s/.test(version.API)) {
    throw new Error(`Could not detect UH4D API from host ${opt.options.host}`);
  }

  // read dir
  const files = await fs.readdir(opt.options.dir);

  for (const file of files) {

    // only pkl files
    if (!/\.pkl$/.test(file)) {
      continue;
    }

    const imageId = file.replace(/\.pkl$/, '');

    logger.info('Uploading', file);

    const form = new FormData();
    form.append('file', fs.createReadStream(path.join(opt.options.dir, file)));

    try {

      const response = await got.post(`${apiUrl}pipeline/images/${imageId}/pkl`, {
        body: form,
        searchParams: { regexp: '1' }
      }).json();

      logger.info('-> Success!', response.file.pkl);

    } catch (e) {

      logger.warn('Upload failed.', e);

    }

  }

})()
  .then(() => {
    logger.info('Done!');
    process.exit();
  })
  .catch(err => {
    logger.error(err ? err : 'Something failed!');
    process.exit(3);
  });
