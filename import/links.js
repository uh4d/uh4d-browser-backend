import * as path from 'path';
import fs from 'fs-extra';
import nodeGetopt from 'node-getopt';
import LineByLineReader from 'line-by-line';
import * as neo4j from 'neo4j-request';
import logger from '../modules/logger.js';
import { checkDatabaseImageEntry, writeImageToDatabase } from './modules/database.js';
import { extractIdFromLink, requestDetailsPage, requestImage } from './modules/request-dfd.js';
import { parseHTMLFile } from './modules/parser.js';
import processImage from '../modules/process-image.js';
import * as config from '../config.js';

const getopt = nodeGetopt.create([
  ['s', 'scene=', 'Scene ID, e.g. "dresden", "jena"'],
  ['h', 'help']
])
.bindHelp(`
Usage: node links.js [OPTION] <FILE|LINK>...

[[OPTIONS]]

FILE can also use regular expression to select multiple files.
Only txt files with one link on each line are currently supported.
`);
const opt = getopt.parseSystem();

if (!opt.options.scene) {
  logger.warn('Unset scene ID! Specify which scene should be affected by refactoring process.');
  getopt.showHelp();
  process.exit(1);
}

// show help if arguments are missing
if (opt.argv.length === 0) {
  logger.warn('No arguments provided! Specify a file or a link to parse.')
  getopt.showHelp();
  process.exit(2);
}

const tmpDir = './tmp';

// main async
(async () => {

  const ids = [];

  // collect ids
  for (const argv of opt.argv) {

    if (/^https?:\/\/\S+$/.test(argv)) {

      // argv is link
      try {
        ids.push(extractIdFromLink(argv));
      } catch (err) {
        logger.warn(err);
      }

    } else {

      // argv is path to file (or files, regexp supported)
      let sepIndex = argv.lastIndexOf('/');
      if (sepIndex < 0) {
        sepIndex = argv.lastIndexOf('\\');
      }

      const folder = sepIndex < 0 ? './' : argv.substring(0, sepIndex);
      const fileRegex = new RegExp(argv.substring(sepIndex + 1));
      const files = fs.readdirSync(folder);

      for (const file of files) {
        if (fileRegex.test(file) && path.extname(file) === '.txt') {
          ids.push(...(await readLinkFile(file)));
        }
      }

    }

  }

  // connect neo4j database
  await neo4j.init(config.neo4j.url, config.neo4j.user, config.neo4j.password, config.neo4j.database);

  // create tmp dir
  await fs.ensureDir(tmpDir)

  // process ids
  for (let i = 0, l = ids.length; i < l; i++) {
    logger.info(`Process ${i + 1} / ${l}`, ids[i]);
    const result = await processWorkflow(ids[i]);
    await new Promise(resolve => {
      // (wait 1 second and) go to next one
      setTimeout(resolve, result === 'exists' ? 20 : 1000);
    });
  }

})()
  .then(async () => {
    await fs.remove(tmpDir);
    logger.info('Done!');
    process.exit();
  })
  .catch(err => {
    logger.error(err ? err : 'Something failed!');
    process.exit(3);
  });

/**
 * Extract links from file.
 * @param file {string}
 * @return {Promise<string[]>}
 */
function readLinkFile(file) {

  return new Promise((resolve, reject) => {

    const ids = [];
    const lr = new LineByLineReader(file);

    lr.on('error', err => {
      reject(err);
    });

    lr.on('line', line => {
      try {
        ids.push(extractIdFromLink(line));
      } catch (err) {
        logger.warn(err);
      }
    });

    lr.on('end', () => {
      resolve(ids);
    });

  });

}

/**
 * Parse DFD page, download and process image, write to database.
 * @param id {string}
 * @return {Promise<string|undefined>}
 */
async function processWorkflow(id) {

  // check if image with id already exists
  const entryExists = await checkDatabaseImageEntry(id);

  if (entryExists) {
    logger.debug('Already exists. Skip...');
    return 'exists';
  }

  const meta = {
    id,
    permalink: 'http://www.deutschefotothek.de/documents/obj/' + id,
    scene: opt.options.scene
  };

  let htmlFile;
  let imageFile;

  try {

    // pipeline
    htmlFile = await requestDetailsPage(meta, tmpDir);
    Object.assign(meta, await parseHTMLFile(htmlFile));

    // if no download link is available, skip this image
    if (!meta.fileLink) {
      logger.warn('Missing download link. Skip...');
      return;
    }

    imageFile = await requestImage(meta.fileLink, tmpDir);
    meta.file = await processImage(imageFile, config.path.data);

    await writeImageToDatabase(meta);

  } catch (err) {

    // cleanup on error
    if (meta.file && meta.file.path) {
      logger.warn('Unlink ' + meta.file.path);
      await fs.remove(config.path.data + '/' + meta.file.path);
    }

    throw err;

  } finally {

    // cleanup
    if (htmlFile) {
      await fs.remove(htmlFile);
    }
    if (imageFile) {
      await fs.remove(imageFile);
    }

  }

}
