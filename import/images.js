import * as path from 'path';
import fs from 'fs-extra';
import * as neo4j from 'neo4j-request';
import getopt from 'node-getopt';
import * as config from '../config.js';
import processImage from '../modules/process-image.js';
import logger from '../modules/logger.js';
import { extractIdFromLink, getPermalinkByFilename, requestDetailsPage } from './modules/request-dfd.js';
import { checkDatabaseImageEntry, writeImageToDatabase } from './modules/database.js';
import { parseHTMLFile } from './modules/parser.js';

const opt = getopt.create([
  ['s', 'scene=', 'Scene ID, e.g. "dresden", "jena"'],
  ['i', 'input=', 'Input directory containing image files', 'input'],
  ['o', 'output=', 'Output directory (defaults to folder set in config.js)', config.path.data],
  ['', 'dfd', 'Search Deutsche Fotothek for metadata', false]
]).bindHelp().parseSystem();

if (!opt.options.scene) {
  console.warn('Unset scene ID! Specify which scene should be affected by refactoring process.');
  process.exit(2);
}

const tmpDir = './tmp';

// main async
(async () => {

  // read input directory
  const files = await fs.readdir(opt.options.input);

  // connect neo4j database
  await neo4j.init(config.neo4j.url, config.neo4j.user, config.neo4j.password, config.neo4j.database);

  // create tmp dir
  await fs.ensureDir(tmpDir);

  // process files
  for (let i = 0, l = files.length; i < l; i++) {

    logger.info(`Process ${i + 1} / ${l}`, files[i]);

    await processWorkflow(files[i]);

    await new Promise(resolve => {
      setTimeout(resolve, 1000);
    });

  }

})()
  .then(async () => {
    await fs.remove(tmpDir);
    logger.info('Done!');
    process.exit();
  })
  .catch(reason => {
    logger.error(reason ? reason : 'Something went wrong!');
    process.exit(1);
  });

/**
 * Parse DFD page (optional), process image, write to database.
 * @param file {string}
 * @return {Promise<void>}
 */
async function processWorkflow(file) {

  const srcPath = path.join(opt.options.input, file);

  const meta = {
    scene: opt.options.scene,
    title: file
  };

  // get metadata from Deutsche Fotothek (DFD)
  if (opt.options.dfd) {

    try {

      // get permalink
      meta.permalink = await getPermalinkByFilename(file);
      meta.id = extractIdFromLink(meta.permalink);

    } catch (err) {

      // proceed with next image
      logger.warn(err);
      return;

    }

    // check if image with id already exists
    const entryExists = await checkDatabaseImageEntry(meta.id);

    if (entryExists) {
      logger.debug('Already exists. Skip...');
      return;
    }

    // get metadata from details page
    let htmlFile;

    try {

      htmlFile = await requestDetailsPage(meta, tmpDir);
      Object.assign(meta, await parseHTMLFile(htmlFile));

    } finally {

      // cleanup
      if (htmlFile) {
        await fs.remove(htmlFile);
      }

    }
  }

  try {

    meta.file = await processImage(srcPath, opt.options.output);

    await writeImageToDatabase(meta)

  } catch (err) {

    // cleanup on error
    if (meta.file && meta.file.path) {
      logger.warn('Unlink ' + meta.file.path);
      await fs.remove(path.join(opt.options.output, meta.file.path));
    }

    throw err;

  }

}
