# Import data

Two scripts to import image data to UH4D Browser.

## Images files

The `images.js` script reads a folder of image files, processes them, and writes them to the database.
The title of the image will correlate with the file name.

If `--dfd` flag is set, metadata gets automatically retrieved from the _Deutsche Fotothek_.
Otherwise, most of the metadata will remain empty and has to be added manually.

```
node images.js [OPTION]

Options:
  -s, --scene=      Scene ID, e.g. "dresden", "jena"
  -i, --input=      Input directory containing image files (default: input)
  -o, --output=     Output directory (defaults to folder set in config.js)
      --dfd         Search Deutsche Fotothek for metadata
  -h, --help        display this help
```

## Links

The `links.js` script either takes a link or parses a file of links, extracts the IDs, retrieves the image and metadata from _Deutsche Fotothek_, and writes them to the database.
Image data is stored in the directory as specified in `config.js`.

__Note:__ Currently, this works only for links/images of the _Deutsche Fotothek_.

```
node links.js [OPTION] <link|file>

Options:
-s, --scene=      Scene ID, e.g. "dresden", "jena"
-h, --help        display this help
```

The link should look like `http://www.deutschefotothek.de/documents/obj/:id`.

A file must be a simple text file with one link each line, e.g.:

```
http://www.deutschefotothek.de/documents/obj/90075295
http://www.deutschefotothek.de/documents/obj/80653861
http://www.deutschefotothek.de/documents/obj/90075279
http://www.deutschefotothek.de/documents/obj/80102161
...
```

## pkl files

The `pkl-files.js` script reads a folder of pkl files (feature files for computer vision) and tries to upload them to the server.
Hence, `host` must be a running UH4D backend instance, e.g. `http://localhost:3001` or `https://4dbrowser.urbanhistory4d.org`.
The name of pkl file must match the ID of an UH4D image.

```
node pkl-files.js [OPTION]

Options:
-s, --dir=      Directory with pkl files
    --host=     UH4D API host url, e.g., https://4dbrowser.urbanhistory4d.org
-h, --help      display this help
```
