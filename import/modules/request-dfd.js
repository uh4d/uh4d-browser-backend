import * as path from 'path';
import fs from 'fs-extra';
import got from 'got';

/**
 * Download image to file.
 * @param url {string}
 * @param outDir {string} Path to directory where image will be temporally saved.
 * @return {Promise<string>} Path to image file.
 */
export async function requestImage(url, outDir) {

  const tmpFile = path.join(outDir, url.slice(url.lastIndexOf('/') + 1));

  // http request
  const response = await got(url, { encoding: 'binary' });

  // save data to file
  await fs.writeFile(tmpFile, response.body, 'binary');

  return tmpFile;

}

/**
 * Download HTML page to file.
 * @param meta {{id: string, permalink: string}}
 * @param outDir {string} Path to directory where HTML file will be temporally saved.
 * @return {Promise<string>} Path to HTML file.
 */
export async function requestDetailsPage(meta, outDir) {

  const tmpFile = path.join(outDir, `${meta.id}.html`);

  // http request
  const response = await got(meta.permalink, {
    encoding: 'latin1',
    headers: {
      'Accept-Language': 'de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7'
    }
  });

  // save data to file
  await fs.writeFile(tmpFile, response.body, { encoding: 'latin1' });

  return tmpFile;

}

/**
 * Get permalink by searching for file name.
 * @param filename {string}
 * @return {Promise<string>}
 */
export async function getPermalinkByFilename(filename) {

  const baseFilename = filename.substr(0, filename.lastIndexOf('.'));
  const url = 'http://www.deutschefotothek.de/ete?action=query&refine=Suchen&desc=' + baseFilename;

  // request search page
  const response = await got(url).text();

  // parse permalink
  const pattern = /<p class="description">\s*<a\shref="([^"]*)"/;
  const matches = pattern.exec(response);

  if (matches && matches[1]) {
    return matches[1];
  } else {
    throw new Error(`No permalink found from ${filename}`);
  }

}

/**
 * Get ID from DFD link.
 * @param link {string}
 * @return {string}
 */
export function extractIdFromLink(link) {

  const regexp = /https?:\/\/www\.deutschefotothek\.de\/documents\/obj\/([^,.\s/]+)/;
  const matches = regexp.exec(link);

  if (matches && matches[1]) {
    return matches[1];
  } else {
    throw new Error('Link not yet supported: ' + link);
  }

}
