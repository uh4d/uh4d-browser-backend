export const host = 'https://your-domain.com';
export const port = 3001;

export const neo4j = {
  url: 'neo4j://localhost',
  user: 'neo4j',
  password: 'password',
  database: 'neo4j'
};

export const path = {
  data: 'path/to/data',
  logs: 'path/to/logs'
};

export const exec = {
  Assimp: 'assimp', // assimp cli command
  Collada2Gltf: 'COLLADA2GLTF-bin', // collada2gltf cli command
  ImageIdentify: 'identify', // ImageMagick identify
  ImageMagick: 'magick' // ImageMagick magick/convert
};

// only for bug reports
export const gitlab = {
  'uh4d-browser-app': {
    apiEndpoint: 'https://gitlab.com/api/v4',
    projectId: '18567666',
    privateToken: '<your_access_token>'
  },
  'vrcity-app':  {
    apiEndpoint: 'https://git.uni-jena.de/api/v4',
    projectId: '396',
    privateToken: '<your_access_token>'
  }
};
